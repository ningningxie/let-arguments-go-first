# Supplementary Materials

This is the supplementary materials for paper *Let Arguments Go First* in ESOP 2018.

## Road Map

+ [paper_full.pdf](./paper_full.pdf): Full paper with the appendix.
+ [coq/](./coq) contains all of the Coq development.

Require Import LibLN DeclDef HM.
Require Import DeclInfra.
Set Implicit Arguments.

(* ********************************************************************** *)
(** * Properties of well-formedness of a type in an environment *)

(** If a type is well-formed in an environment then it is locally closed. *)

Hint Unfold hokt hokt_empty hokt_typ.
Hint Unfold htyp htyp_nat htyp_bvar htyp_fvar htyp_arrow htyp_all htype .
Hint Unfold htype htype_nat htype_var htype_arrow htype_all.

(* ********************************************************************** *)
(** * Relations between well-formed environment and types well-formed
  in environments *)

(** If an environment is well-formed, then it does not contain duplicated keys. *)

Lemma hok_from_okt : forall E,
  hokt E -> ok E.
Proof.
  induction 1; auto.
Qed.

Hint Extern 1 (ok _) => apply hok_from_okt.

(** Extraction from a typing assumption in a well-formed environments *)

Lemma htype_from_env_has_typ : forall x U E,
  hokt E -> binds x U E -> htype U.
Proof.
  apply* dtype_from_env_has_typ.
Qed.

(** Extraction from a well-formed environment *)

Lemma htype_from_okt_typ : forall x T E,
  hokt (E & x ~ T) -> dtype T.
Proof.
  apply* dtype_from_okt_typ.
Qed.

(** Automation *)

Hint Resolve htype_from_okt_typ.
Hint Immediate htype_from_env_has_typ.

Lemma hinst_regular: forall t1 t2,
    hinst t1 t2 ->
    htype t1 /\ htype t2.
Proof.
  introv ins. inductions ins; simpls~.
  destruct IHins.
  splits~.
  apply_fresh htype_all as y.
  unfold htype in H0.
  apply* dtype_open_inv.
Qed.

Lemma htyping_regular : forall E e T,
  htyping E e T -> hokt E /\ hterm e /\ htype T.
Proof.
  induction 1.
  splits~. apply* hinst_regular.
  splits~.
  splits~.
   pick_fresh y. specializes H2 y. destructs~ H2.
   apply* dokt_concat_left_inv.
   apply_fresh hterm_abs as y.
     specializes H2 y. destructs~ H2.
     pick_fresh y. specializes H2 y. destructs~ H2.

  destruct IHhtyping1 as [? [? ?]].
  destruct IHhtyping2 as [? [? ?]]. inversion~ H6; subst.

  destruct IHhtyping as [? [? ?]].
  splits~.
  apply_fresh* hterm_let as x.
  specializes H2 x. destructs~ H2.
  pick_fresh y. specializes H2 y. destructs~ H2.
Qed.

Hint Extern 1 (hokt ?E) =>
  match goal with
  | H: htyping _ _ _ |- _ => apply (proj31 (htyping_regular H))
  end.

Hint Extern 1 (htype ?E ?T) =>
  match goal with
  | H: htyping E _ T |- _ => apply (proj33 (htyping_regular H))
  end.

Hint Extern 1 (hterm ?e) =>
  match goal with
  | H: htyping _ ?e _ |- _ => apply (proj32 (htyping_regular H))
  end.


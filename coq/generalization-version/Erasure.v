Set Implicit Arguments.
Require Import LibList.
Require Import LibLN.
Require Import FDef.
Require Import FInfra.
Require Import TransGen.
Require Import TransSubtyping.
Require Import TransTyping.

Implicit Types x : var.

Inductive erased_ftrm : Set :=
  | erased_ftrm_bvar   : nat -> erased_ftrm
  | erased_ftrm_fvar   : var -> erased_ftrm
  | erased_ftrm_nat    : nat -> erased_ftrm
  | erased_ftrm_abs    : erased_ftrm -> erased_ftrm
  | erased_ftrm_app    : erased_ftrm -> erased_ftrm -> erased_ftrm
  | erased_ftrm_pair   : erased_ftrm -> erased_ftrm -> erased_ftrm
  | erased_ftrm_fst    : erased_ftrm -> erased_ftrm
  | erased_ftrm_snd    : erased_ftrm -> erased_ftrm
.

Fixpoint erased_fopen_ee_rec (k : nat) (f : erased_ftrm) (e : erased_ftrm) {struct e} : erased_ftrm :=
  match e with
  | erased_ftrm_bvar i       => If k = i then f else (erased_ftrm_bvar i)
  | erased_ftrm_fvar x       => erased_ftrm_fvar x
  | erased_ftrm_nat i        => erased_ftrm_nat i
  | erased_ftrm_abs e1       => erased_ftrm_abs (erased_fopen_ee_rec (S k) f e1)
  | erased_ftrm_app e1 e2    => erased_ftrm_app (erased_fopen_ee_rec k f e1) (erased_fopen_ee_rec k f e2)
  | erased_ftrm_pair e1 e2   => erased_ftrm_pair (erased_fopen_ee_rec k f e1) (erased_fopen_ee_rec k f e2)
  | erased_ftrm_fst e1       => erased_ftrm_fst (erased_fopen_ee_rec k f e1)
  | erased_ftrm_snd e1       => erased_ftrm_snd (erased_fopen_ee_rec k f e1)
  end.

Definition erased_fopen_ee t u := erased_fopen_ee_rec 0 u t.

Notation "t 'erased_fopen_ee_var' x" := (erased_fopen_ee t (erased_ftrm_fvar x)) (at level 67).

Inductive erased_fterm : erased_ftrm -> Prop :=
  | erased_fterm_var : forall x,
      erased_fterm (erased_ftrm_fvar x)
  | erased_fterm_nat : forall i,
      erased_fterm (erased_ftrm_nat i)
  | erased_fterm_abs : forall L e1,
      (forall x, x \notin L -> erased_fterm (e1 erased_fopen_ee_var x)) ->
      erased_fterm (erased_ftrm_abs e1)
  | erased_fterm_app : forall e1 e2,
      erased_fterm e1 ->
      erased_fterm e2 ->
      erased_fterm (erased_ftrm_app e1 e2)
  | erased_fterm_pair : forall e1 e2,
      erased_fterm e1 ->
      erased_fterm e2 ->
      erased_fterm (erased_ftrm_pair e1 e2)
  | erased_fterm_fst : forall e,
      erased_fterm e ->
      erased_fterm (erased_ftrm_fst e)
  | erased_fterm_snd : forall e,
      erased_fterm e ->
      erased_fterm (erased_ftrm_snd e)
.

Fixpoint erased_ffv_ee (e : erased_ftrm) {struct e} : vars :=
  match e with
  | erased_ftrm_bvar i       => \{}
  | erased_ftrm_fvar x       => \{x}
  | erased_ftrm_nat i        => \{}
  | erased_ftrm_abs e1       => (erased_ffv_ee e1)
  | erased_ftrm_app e1 e2    => (erased_ffv_ee e1) \u (erased_ffv_ee e2)
  | erased_ftrm_pair e1 e2   => (erased_ffv_ee e1) \u (erased_ffv_ee e2)
  | erased_ftrm_fst e1       => (erased_ffv_ee e1)
  | erased_ftrm_snd e1       => (erased_ffv_ee e1)
  end.

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : ftrm => ffv_te x) in
  let D := gather_vars_with (fun x : ftrm => ffv_ee x) in
  let E := gather_vars_with (fun x : ftyp => ffv_tt x) in
  let F := gather_vars_with (fun x : fenv => dom x) in
  let G := gather_vars_with (fun x : erased_ftrm => erased_ffv_ee x) in
  constr:(A \u B \u C \u D \u E \u F \u G).

(** "pick_fresh x" tactic create a fresh variable with name x *)

Ltac pick_fresh x :=
  let L := gather_vars in (pick_fresh_gen L x).

(** "apply_fresh T as x" is used to apply inductive rule which
   use an universal quantification over a cofinite set *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

(* ********************************************************************** *)
(** ** Properties of term substitution in terms *)

Lemma erased_fopen_ee_rec_term_core : forall e j v u i, i <> j ->
  erased_fopen_ee_rec j v e = erased_fopen_ee_rec i u (erased_fopen_ee_rec j v e) ->
  e = erased_fopen_ee_rec i u e.
Proof.
  induction e; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma erased_fopen_ee_rec_term : forall u e,
  erased_fterm e -> forall k, e = erased_fopen_ee_rec k u e.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds erased_fopen_ee. pick_fresh x.
   apply* (@erased_fopen_ee_rec_term_core e1 0 (erased_ftrm_fvar x)).
Qed.

(* ********************************************************************** *)
(** ** Erase Type Information in f term *)

Fixpoint erase (e:ftrm) {struct e} : erased_ftrm :=
  match e with
  | ftrm_bvar i       => erased_ftrm_bvar i
  | ftrm_fvar x       => erased_ftrm_fvar x
  | ftrm_nat i        => erased_ftrm_nat i
  | ftrm_absann V e1  => erased_ftrm_abs (erase e1)
  | ftrm_app e1 e2    => erased_ftrm_app (erase e1) (erase e2)
  | ftrm_tabs e1      => erase e1
  | ftrm_tapp e1 e2   => erase e1
  | ftrm_pair e1 e2   => erased_ftrm_pair (erase e1) (erase e2)
  | ftrm_fst e1       => erased_ftrm_fst (erase e1)
  | ftrm_snd e1       => erased_ftrm_snd (erase e1)
  end.

Hint Constructors erased_ftrm erased_fterm.

Lemma erase_fopen_te_rec: forall f X k,
    erase (fopen_te_rec k X f) = erase f.
Proof.
  induction f; auto; introv; simpls~; f_equal ~.
Qed.

Lemma erase_fopen_ee_rec: forall e k y,
    erase (fopen_ee_rec k y e) =
    erased_fopen_ee_rec k (erase y) (erase e).
Proof.
  inductions e; introv; simpls; auto; f_equal ~.
  case_nat~.
Qed.

Lemma erase_fopen_ee_var: forall e x,
    erase (e fopen_ee_var x) =
    (erase e) erased_fopen_ee_var x.
Proof.
  introv. unfold fopen_ee. unfold erased_fopen_ee.
  unsimpl (erase (ftrm_fvar x)).
  apply erase_fopen_ee_rec.
Qed.

Lemma erase_fterm: forall e,
    fterm e ->
    erased_fterm (erase e).
Proof.
  induction 1; simpls; auto.
  apply_fresh erased_fterm_abs as x.
  rewrite~ <- erase_fopen_ee_var.
  pick_fresh y.
  forwards * : H0 y.
  unfolds fopen_te.
  rewrite erase_fopen_te_rec in H1. auto.
Qed.

Lemma erase_notin: forall x e,
    x \notin ffv_ee e ->
    x \notin erased_ffv_ee (erase e).
Proof.
  inductions e; introv notin; simpls; auto.
Qed.

Hint Resolve erase_fterm erase_notin.

Definition id := erased_ftrm_abs (erased_ftrm_bvar 0).
Hint Unfold id.

(* ********************************************************************** *)
(** ** ID ETA equality *)

Inductive id_eta_eq : erased_ftrm -> erased_ftrm -> Prop :=
  | id_eta_eta_reduce : forall L e,
      (forall x, x \notin L -> x \notin erased_ffv_ee (e erased_fopen_ee_var x)) ->
      id_eta_eq (erased_ftrm_abs (erased_ftrm_app e
                                               (erased_ftrm_bvar 0)))
             e
  | id_eta_id_reduce : forall e,
      id_eta_eq (erased_ftrm_app id e) e
  | id_eta_pair_reduce : forall e,
      id_eta_eq e (erased_ftrm_pair (erased_ftrm_fst e) (erased_ftrm_snd e))

  | id_eta_app : forall e1 e1' e2 e2',
      id_eta_eq e1 e1' ->
      id_eta_eq e2 e2' ->
      id_eta_eq (erased_ftrm_app e1 e2) (erased_ftrm_app e1' e2')
  | id_eta_pair : forall e1 e1' e2 e2',
      id_eta_eq e1 e1' ->
      id_eta_eq e2 e2' ->
      id_eta_eq (erased_ftrm_pair e1 e2) (erased_ftrm_pair e1' e2')
  | id_eta_fst : forall e1 e1',
      id_eta_eq e1 e1' ->
      id_eta_eq (erased_ftrm_fst e1) (erased_ftrm_fst e1')
  | id_eta_snd : forall e1 e1',
      id_eta_eq e1 e1' ->
      id_eta_eq (erased_ftrm_snd e1) (erased_ftrm_snd e1')

  | id_eta_abs : forall L e1 e1',
      (forall x, x \notin L -> id_eta_eq (e1 erased_fopen_ee_var x)
                                   (e1' erased_fopen_ee_var x)) ->
      id_eta_eq (erased_ftrm_abs e1) (erased_ftrm_abs e1')
  | id_eta_refl : forall e,
      id_eta_eq e e
  | id_eta_symm : forall e1 e2,
      id_eta_eq e1 e2 ->
      id_eta_eq e2 e1
  | id_eta_trans : forall e1 e2 e3,
      id_eta_eq e1 e2 ->
      id_eta_eq e2 e3 ->
      id_eta_eq e1 e3.

Hint Constructors id_eta_eq.

Lemma id_eta_id_reduce_trans: forall e1 e2,
    id_eta_eq e1 id ->
    id_eta_eq (erased_ftrm_app e1 e2) e2.
Proof.
  introv eq.
  apply id_eta_trans with (erased_ftrm_app id e2).
  apply~ id_eta_app.
  apply id_eta_id_reduce.
Qed.
Hint Resolve id_eta_id_reduce_trans.

Theorem d2fsub_id : forall Q S T f,
    d2fsub Q S T f ->
    id_eta_eq (erase f) id.
Proof.
  induction 1; unfold id; simpls; auto.
  apply_fresh id_eta_abs as x.
  unfold erased_fopen_ee. simpls. do 3 case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  rewrite <- erased_fopen_ee_rec_term.
  apply id_eta_trans with (erased_ftrm_abs (erased_ftrm_app (erased_ftrm_fvar x)
                                                            (erased_ftrm_bvar 0))); auto.
  apply id_eta_trans with (erased_ftrm_abs
                             (erased_ftrm_app (erase f2)
                                              (erased_ftrm_app (erased_ftrm_fvar x)
                                                               (erased_ftrm_bvar 0)))); auto.
  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  rewrite <- erased_fopen_ee_rec_term.
  apply~ id_eta_app.

  apply* erase_fterm.
  apply* erase_fterm.

  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  apply~ id_eta_id_reduce_trans.

  apply* erase_fterm.

  apply_fresh id_eta_eta_reduce as y; auto.
  simpls~.

  apply* erase_fterm.
  apply* erase_fterm.

  (* *)
  apply id_eta_trans with (erase f1); auto.
  apply_fresh id_eta_eta_reduce as x.
  unfold erased_fopen_ee.
  rewrite <- erased_fopen_ee_rec_term; auto.
  apply* erase_fterm.

  (* *)
  apply id_eta_trans with (erase f); auto.
  apply_fresh id_eta_eta_reduce as x.
  unfold erased_fopen_ee.
  rewrite <- erased_fopen_ee_rec_term; auto.
  forwards ~ : H x.
  apply d2fsub_term in H1.
  apply erase_fterm in H1.
  unfolds fopen_te.
  rewrite erase_fopen_te_rec in H1. auto.

  pick_fresh x.
  forwards ~ : H0 x.
  unfolds fopen_te.
  rewrite erase_fopen_te_rec in H1. auto.

  (* *)
  apply id_eta_trans with (erase f1); auto.
  apply_fresh id_eta_eta_reduce as x.
  unfold erased_fopen_ee.
  rewrite <- erased_fopen_ee_rec_term; auto.
  apply* erase_fterm.

  (* *)
  apply_fresh id_eta_abs as x.
  unfold erased_fopen_ee. simpls. do 3 case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  rewrite <- erased_fopen_ee_rec_term.
  apply id_eta_trans with (erased_ftrm_abs (erased_ftrm_app (erased_ftrm_fvar x)
                                                            (erased_ftrm_bvar 0))); auto.
  apply id_eta_trans with (erased_ftrm_abs
                             (erased_ftrm_app (erase f2)
                                              (erased_ftrm_app (erased_ftrm_fvar x)
                                                               (erased_ftrm_bvar 0)))); auto.
  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  rewrite <- erased_fopen_ee_rec_term.
  apply~ id_eta_app.

  apply* erase_fterm.
  apply* erase_fterm.

  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  rewrite <- erased_fopen_ee_rec_term.
  apply~ id_eta_id_reduce_trans.

  apply* erase_fterm.

  apply_fresh id_eta_eta_reduce as y; auto.
  simpls~.

  apply* erase_fterm.
  apply* erase_fterm.

  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  rewrite~ <- erased_fopen_ee_rec_term.
  rewrite~ <- erased_fopen_ee_rec_term.
  apply id_eta_trans with (erased_ftrm_pair
                             (erased_ftrm_app id
                                              (erased_ftrm_fst (erased_ftrm_fvar y)))
                             (erased_ftrm_app (erase f2)
                                              (erased_ftrm_snd (erased_ftrm_fvar y)))).
    apply~ id_eta_pair.
  apply id_eta_trans with (erased_ftrm_pair
                             (erased_ftrm_app id
                                              (erased_ftrm_fst (erased_ftrm_fvar y)))
                             (erased_ftrm_app id
                                              (erased_ftrm_snd (erased_ftrm_fvar y)))).
    apply~ id_eta_pair.
  apply id_eta_trans with (erased_ftrm_pair
                             (erased_ftrm_fst (erased_ftrm_fvar y))
                             (erased_ftrm_app id
                                              (erased_ftrm_snd (erased_ftrm_fvar y)))).
    apply~ id_eta_pair.
  apply id_eta_trans with (erased_ftrm_pair
                             (erased_ftrm_fst (erased_ftrm_fvar y))
                             (erased_ftrm_snd (erased_ftrm_fvar y))).
    apply~ id_eta_pair.
  apply~ id_eta_symm.
  apply* erase_fterm.
  apply* erase_fterm.
Qed.

Theorem d2fgen_id : forall E t1 s1 t2 s2,
    d2fgen E t1 s1 t2 s2 ->
    erase s1 = erase s2.
Proof.
  introv gen. induction gen; simpls~.
  unfold fopen_te.
  rewrite~ erase_fopen_te_rec.
Qed.

Hint Resolve d2fsub_id d2fgen_id.
Theorem d2ftyping_confluence : forall E1 E2 Q1 Q2 S T1 T2 f1 f2,
    d2ftyping E1 Q1 S T1 f1 ->
    d2ftyping E2 Q2 S T2 f2 ->
    id_eta_eq (erase f1) (erase f2).
Proof.
  introv ty. gen f2 T2 Q2 E2.
  induction ty; introv typ2; inversion typ2; subst; simpls~.

  (* *)
  apply~ id_eta_app.
  apply id_eta_trans with id.
  apply* d2fsub_id.
  apply id_eta_symm. apply* d2fsub_id.

  (* *)
  apply_fresh id_eta_abs as x.
  clear typ2 H.
  specializes H9 x. forwards ~: H9. clear H9.
  specializes H1 x (s0 fopen_ee_var x) H.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H1.
  unfolds erased_fopen_ee. auto.

  (* *)
  apply id_eta_trans with (erased_ftrm_abs (erase s0)).
  apply_fresh id_eta_abs as x.
  forwards ~ : H10 x.
  forwards ~ : H1 x H2.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H3.
  unfolds erased_fopen_ee. exact H3.

  apply id_eta_trans with (erased_ftrm_abs
                             (erased_ftrm_app (erased_ftrm_abs (erase s0))
                                              (erased_ftrm_bvar 0))).
  apply id_eta_symm.
  apply_fresh id_eta_eta_reduce as y.
  unfold erased_fopen_ee.
  rewrite <- erased_fopen_ee_rec_term.
  simpls. apply~ erase_notin.
  apply_fresh erased_fterm_abs as x.
  rewrite <- erase_fopen_ee_var.
  apply erase_fterm.
  forwards ~ : H10 x.
  lets : d2ftyping_term H2. auto.

  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  apply~ id_eta_app.
  rewrite <- erased_fopen_ee_rec_term.
  apply id_eta_symm.
  apply~ id_eta_id_reduce_trans.
  apply* d2fsub_id.
  apply erase_fterm.
  apply* d2fsub_term.

  (* *)
  apply_fresh id_eta_abs as x.
  forwards ~ : H8 x. clear H8.
  forwards ~ : H2 x H3. clear H2.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H6.
  unfolds erased_fopen_ee. exact H6.

  (* *)
  apply_fresh id_eta_abs as x.
  forwards ~ : H6 x. clear H6.
  forwards ~ : H2 x H3. clear H2.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H4.
  unfolds erased_fopen_ee. exact H4.

  (* *)
  apply id_eta_symm.
  apply id_eta_trans with (erased_ftrm_abs (erase s)).
  apply_fresh id_eta_abs as x.
  forwards ~ : H10 x.
  forwards ~ : H2 x H3.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H4.
  unfolds erased_fopen_ee.
  apply id_eta_symm. exact H4.

  apply id_eta_trans with (erased_ftrm_abs
                             (erased_ftrm_app (erased_ftrm_abs (erase s))
                                              (erased_ftrm_bvar 0))).
  apply id_eta_symm.
  apply_fresh id_eta_eta_reduce as y.
  unfold erased_fopen_ee.
  rewrite <- erased_fopen_ee_rec_term.
  simpls. apply~ erase_notin.
  apply_fresh erased_fterm_abs as x.
  rewrite <- erase_fopen_ee_var.
  apply erase_fterm.
  forwards ~ : H1 x.
  lets : d2ftyping_term H3. auto.

  apply_fresh id_eta_abs as y.
  unfold erased_fopen_ee. simpls. case_nat~.
  apply~ id_eta_app.
  rewrite <- erased_fopen_ee_rec_term.
  apply id_eta_symm.
  apply~ id_eta_id_reduce_trans.
  apply* d2fsub_id.
  apply erase_fterm.
  apply* d2fsub_term.

  (* *)
  apply_fresh id_eta_abs as y. unfold erased_fopen_ee. simpls.
  case_nat~.
  unsimpl (erased_fopen_ee_rec 0 (erased_ftrm_fvar y) (erased_ftrm_abs (erase s))).
  unsimpl (erased_fopen_ee_rec 0 (erased_ftrm_fvar y) (erased_ftrm_abs (erase s0))).
  rewrite <- erased_fopen_ee_rec_term; auto.
  rewrite <- erased_fopen_ee_rec_term; auto.
  rewrite <- erased_fopen_ee_rec_term; auto.
  rewrite <- erased_fopen_ee_rec_term; auto.
  apply id_eta_app.
  apply_fresh id_eta_abs as x.
  forwards ~ : H11 x.
  forwards ~ : H2 x H3.
  unfolds fopen_ee.
  unfold erased_fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H4. exact H4.

  apply~ id_eta_app.
  apply id_eta_trans with id.
  apply* d2fsub_id.
  apply id_eta_symm. apply* d2fsub_id.
  apply* erase_fterm.
  apply_fresh erased_fterm_abs as x.
  unfold erased_fopen_ee.
  unsimpl (erase (ftrm_fvar x)).
  rewrite <- erase_fopen_ee_rec.
  apply* erase_fterm.
  apply* erase_fterm.
  apply_fresh erased_fterm_abs as x.
  unfold erased_fopen_ee.
  unsimpl (erase (ftrm_fvar x)).
  rewrite <- erase_fopen_ee_rec.
  apply* erase_fterm.

  (* *)
  apply_fresh id_eta_abs as x.
  forwards ~ : H6 x. clear H6.
  forwards ~ : H0 x H1. clear H0.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H4.
  unfolds erased_fopen_ee. exact H4.

  (* *)
  apply_fresh id_eta_abs as x.
  forwards ~ : H4 x. clear H4.
  forwards ~ : H0 x H1. clear H0.
  unfolds fopen_ee.
  do 2 rewrite erase_fopen_ee_rec in H2.
  unfolds erased_fopen_ee. exact H2.

  (* *)
  apply id_eta_app.
  apply* IHty2.
  forwards ~ : IHty1 H2.
  lets: d2fgen_id H.
  lets ~ : d2fgen_id H5.
  rewrite~ <- H1.
  rewrite~ <- H3.

  (* *)
  apply id_eta_pair.
  apply* IHty1.
  apply* IHty2.

  (* *)
  apply id_eta_app.
  apply id_eta_trans with id.
  apply* d2fsub_id.
  apply id_eta_symm. apply* d2fsub_id.
  apply_fresh id_eta_abs as x.
  apply~ id_eta_fst.

  (* *)
  apply id_eta_app.
  apply id_eta_trans with id.
  apply* d2fsub_id.
  apply id_eta_symm. apply* d2fsub_id.
  apply_fresh id_eta_abs as x.
  apply~ id_eta_snd.
Qed.

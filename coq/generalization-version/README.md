# Proofs

This project contains all the Coq proof code for the first application in the paper *let arguments go first*.

All the proofs have been tested under Coq 8.5pl1, which is compiled with OCaml 4.03.0.

## Files

**Declarative System**

- `DeclDef.v`: the definition of declarative system.
- `DeclInfra.v`: the infrastructure of declarative system.
- `DeclSound.v`: includes important properties of the typing system, such as subtyping transitivity, lemmas for application contexts and so on.
- `DTypingSize.v`: definition and lemmas about the size of typing, which is used while doing induction on typing size.
- `DEnvSub.v`: subtyping relationship on typing context.

**System F**

- `FDef.v`: the definition of System F.
- `FInfra.v`: the infrastructure of System F.
- `Erasure.v`: type erasure of System F.

**Translation with proofs**

- `TransGen.v`: translation of generalization.
- `TransSubtyping.v`: translation of subtyping.
- `TransTyping.v`: translation of typing.

**Hindley-Milner**

- `HM.v`: definition of Hindley-Milner system.
- `HMInfra.v`: the infrastructure of Hindley-Milner system.
- `HMPreserve.v`: the proof of conservation over Hindley-Milner system.



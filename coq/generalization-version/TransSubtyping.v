Set Implicit Arguments.
Require Import LibList.
Require Import LibLN.
Require Import DeclDef.
Require Import FDef.
Require Import FInfra.
Require Import DeclInfra.
Require Import TransGen.

Inductive d2fsub :aenv -> dtyp -> dtyp -> ftrm -> Prop :=
  |d2fsub_nat :
     d2fsub plain dtyp_nat dtyp_nat
            (ftrm_absann ftyp_nat (ftrm_bvar 0))
  |d2fsub_tvar : forall X,
     d2fsub plain (dtyp_fvar X) (dtyp_fvar X)
            (ftrm_absann (ftyp_fvar X) (ftrm_bvar 0))
  |d2fsub_arrow : forall S1 S2 T1 T2 f1 f2,
     d2fsub plain T1 S1 f1 ->
     d2fsub plain S2 T2 f2 ->
     d2fsub plain (dtyp_arrow S1 S2) (dtyp_arrow T1 T2)
            (ftrm_absann (ftyp_arrow S1 S2)
                         (ftrm_absann T1
                                      (ftrm_app f2
                                                (ftrm_app (ftrm_bvar 1)
                                                          (ftrm_app f1 (ftrm_bvar 0))))))
  |d2fsub_alll1 : forall S2 T2 U f1,
     dtype U ->
     dmono U ->
     d2fsub plain (dopen_tt S2 U) T2 f1 ->
     d2fsub plain (dtyp_all S2) T2
            (ftrm_absann (dtyp_all S2)
                         (ftrm_app f1
                                   (ftrm_tapp (ftrm_bvar 0) U)))
  |d2fsub_allr : forall L S2 T2 f,
     (forall X, X \notin L ->
           d2fsub plain S2 (T2 dopen_tt_var X) (f fopen_te_var X )) ->
     d2fsub plain S2 (dtyp_all T2)
            (ftrm_absann S2
                         (ftrm_tabs (ftrm_app f
                                              (ftrm_bvar 0))))

  |d2fsub_empty : forall A,
      dtype A ->
      d2fsub (stack nil) A A
           (ftrm_absann A (ftrm_bvar 0))
  |d2fsub_alll2 : forall S2 T2 U c cs f1,
     dtype U ->
     dmono U ->
     d2fsub (stack (cons c cs)) (dopen_tt S2 U) T2 f1 ->
     d2fsub (stack (cons c cs)) (dtyp_all S2) T2
            (ftrm_absann (dtyp_all S2)
                         (ftrm_app f1
                                   (ftrm_tapp (ftrm_bvar 0) U)))
  |d2fsub_arrow_stack : forall S1 S2 T2 c cs f1 f2,
     d2fsub plain c S1 f1 ->
     d2fsub (stack cs) S2 T2 f2 ->
     d2fsub (stack (cons c cs)) (dtyp_arrow S1 S2) (dtyp_arrow c T2)
            (ftrm_absann (ftyp_arrow S1 S2)
                         (ftrm_absann c
                                      (ftrm_app f2
                                                (ftrm_app (ftrm_bvar 1)
                                                          (ftrm_app f1 (ftrm_bvar 0))))))


  |d2fsub_pair : forall T1 T2 T3 T4 f1 f2,
      d2fsub plain T1 T3 f1 ->
      d2fsub plain T2 T4 f2 ->
      d2fsub plain (dtyp_pair T1 T2) (dtyp_pair T3 T4)
           (ftrm_absann (ftyp_pair T1 T2)
                        (ftrm_pair (ftrm_app f1 (ftrm_fst (ftrm_bvar 0)))
                                   (ftrm_app f2 (ftrm_snd (ftrm_bvar 0)))
                        ))
.

Hint Constructors dsub d2fsub fterm.

Lemma d2fsub_dsub : forall Q S T f,
  d2fsub Q S T f -> dsub Q S T.
Proof.
  introv sub. inductions sub; autos*.
Qed.
(* properties about translation subtyping *)

Lemma d2fsub_regular : forall Q S T f,
  d2fsub Q S T f -> dtype S /\ dtype T.
Proof.
  introv sub.
  forwards  : d2fsub_dsub sub.
  lets~ [? [? ?]]: dsub_regular H.
Qed.

Lemma d2fsub_subst_plain_var : forall t T z u f,
    d2fsub plain T t f ->
    dtype (dtyp_fvar u) ->
    d2fsub plain (dsubst_tt z (dtyp_fvar u) T) (dsubst_tt z (dtyp_fvar u) t) (fsubst_te z (dtyp_fvar u) f).
Proof.
  introv Typt Typu. inductions Typt; introv; simpl; autos*.
  case_var*.

  apply d2fsub_alll1.
  apply* dsubst_tt_type.
  apply* dsubst_mono.

  rewrite* <- dsubst_tt_open_tt.

  apply_fresh* d2fsub_allr as x.
  specializes H0 x.
  forwards * : H0.
  unfold fsubst_tt.
  rewrite* dsubst_tt_open_tt_var.
  rewrite* fsubst_te_open_te_var.
Qed.

Lemma d2fsub_subst_stack_typ: forall z c V f Q u,
  d2fsub Q c V f ->
  d2fsub Q c V (fsubst_ee z (ftrm_fvar u) f).
Proof.
  introv Typt. inductions Typt; introv; simpl; autos*.
  apply_fresh d2fsub_allr as x.
  specializes H0 x.
  forwards * : H0.
  rewrite* fsubst_ee_open_te_var.
Qed.

Hint Resolve d2fsub_subst_plain_var.
Hint Resolve d2fsub_subst_stack_typ.
Lemma d2fsub_subst_stack_var : forall t T z u f Q,
    d2fsub Q T t f ->
    dtype (dtyp_fvar u) ->
    d2fsub (dsubst_aenv z (dtyp_fvar u) Q) (dsubst_tt z (dtyp_fvar u) T) (dsubst_tt z (dtyp_fvar u) t) (fsubst_te z (dtyp_fvar u) f).
Proof.
  introv Typt Typu. inductions Typt; introv; simpl; autos*.
  case_var*.

  apply d2fsub_alll1.
  apply* dsubst_tt_type.
  apply* dsubst_mono.

  rewrite* <- dsubst_tt_open_tt.

  apply_fresh* d2fsub_allr as x.
  specializes H0 x.
  forwards * : H0.
  unfold fsubst_tt.
  rewrite* dsubst_tt_open_tt_var.
  rewrite* fsubst_te_open_te_var.

  apply d2fsub_alll2.
  apply* dsubst_tt_type.
  apply* dsubst_mono.
  forwards * : IHTypt.
  unfold fsubst_tt.
  rewrite* <- dsubst_tt_open_tt.
Qed.

Lemma d2fsub_rename : forall x y t S f,
  d2fsub plain S (t dopen_tt_var x) f ->
  x \notin dfv_tt t \u dfv_tt S ->
  y \notin dfv_tt t \u dfv_tt S ->
  d2fsub plain S (t dopen_tt_var y) (fsubst_te x (dtyp_fvar y) f).
Proof.
  introv Typx Frx Fry.
  tests: (x = y). subst*. rewrite* fsubst_te_same.
  rewrite~ (@dsubst_tt_intro x).
  assert  (d2fsub plain (dsubst_tt x (dtyp_fvar y) S) (dsubst_tt x (dtyp_fvar y) (t dopen_tt_var x)) (fsubst_te x (dtyp_fvar y) f)).
  apply* d2fsub_subst_plain_var.
  assert (dsubst_tt x (dtyp_fvar y) S = S).
  apply* dsubst_tt_fresh.
  rewrite H0 in H. auto.
Qed.

Lemma d2fsub_term : forall Q S T f,
  d2fsub Q S T f -> fterm f.
Proof.
  introv sub. induction sub.
  apply_fresh fterm_absann as x; auto.
    unfold fopen_ee. simpls. case_nat*.
  apply_fresh fterm_absann as x; auto.
    unfold fopen_ee. simpls. case_nat*.
  apply_fresh fterm_absann as x; auto.
    apply~ dtype_arrow.
    forwards~ [? ?]: d2fsub_regular sub1.
    forwards~ [? ?]: d2fsub_regular sub2.

    apply_fresh fterm_absann as y; autos.
    forwards~ [? ?] : d2fsub_regular sub1.
    repeat case_nat*. simpls.
    rewrite~ <- fopen_ee_rec_term.
    rewrite~ <- fopen_ee_rec_term.

    apply~ fterm_app.
    rewrite~ <- fopen_ee_rec_term.
    rewrite~ <- fopen_ee_rec_term.
    case_nat*.

  apply_fresh fterm_absann as x; auto.
    apply_fresh dtype_all as y; auto.
    forwards~ [? ?] : d2fsub_regular sub.
    apply dtype_open_inv with U; auto.

    apply fterm_app.
    rewrite~ <- fopen_ee_rec_term.
    case_nat*.

  apply_fresh fterm_absann as x; auto.
    pick_fresh y. specializes H y. forwards ~ : H.
    lets~ [? ?]: d2fsub_regular H1.

    unfold fopen_ee. simpls. case_nat*.
    apply_fresh fterm_tabs as z; auto.
    unfold fopen_te. simpls.
    apply~ fterm_app.
    rewrite <- fopen_te_ee.
    forwards * : H0 z.
    unfolds fopen_te.
    rewrite~ <- fopen_ee_rec_term.

  apply_fresh fterm_absann as x; auto.
    unfold fopen_ee. simpls. case_nat*.

  apply_fresh fterm_absann as x; auto.
    apply_fresh dtype_all as y.
    forwards~ [? ?] : d2fsub_regular sub.
    apply dtype_open_inv with U; auto.

    apply fterm_app.
    rewrite~ <- fopen_ee_rec_term.
    case_nat*.

  apply_fresh fterm_absann as x; auto.
    apply~ dtype_arrow.
    forwards~ [? ?] : d2fsub_regular sub1.
    forwards~ [? ?] : d2fsub_regular sub2.

    apply_fresh fterm_absann as y; autos.
    forwards~ [? ?] : d2fsub_regular sub1.
    repeat case_nat*. simpls.
    rewrite~ <- fopen_ee_rec_term.
    rewrite~ <- fopen_ee_rec_term.

    apply~ fterm_app.
    rewrite~ <- fopen_ee_rec_term.
    rewrite~ <- fopen_ee_rec_term.
    case_nat*.

  apply_fresh fterm_absann as x; auto.
    apply~ dtype_pair.
    forwards~ [? ?] : d2fsub_regular sub1.
    forwards~ [? ?] : d2fsub_regular sub2.

    unfolds fopen_ee. simpls~. case_nat~.
    apply~ fterm_pair;
      rewrite~  <- fopen_ee_rec_term.
Qed.

Inductive d2fsub' : aenv -> dtyp -> dtyp -> ftrm -> Prop :=
  |d2fsub'_nat :
     d2fsub' plain dtyp_nat dtyp_nat
            (ftrm_absann ftyp_nat (ftrm_bvar 0))
  |d2fsub'_tvar : forall X,
     d2fsub' plain (dtyp_fvar X) (dtyp_fvar X)
            (ftrm_absann (ftyp_fvar X) (ftrm_bvar 0))
  |d2fsub'_arrow : forall S1 S2 T1 T2 f1 f2,
     d2fsub' plain T1 S1 f1 ->
     d2fsub' plain S2 T2 f2 ->
     d2fsub' plain (dtyp_arrow S1 S2) (dtyp_arrow T1 T2)
            (ftrm_absann (ftyp_arrow S1 S2)
                         (ftrm_absann T1
                                      (ftrm_app f2
                                                (ftrm_app (ftrm_bvar 1)
                                                          (ftrm_app f1 (ftrm_bvar 0))))))
  |d2fsub'_alll1 : forall S2 T2 U f1,
     dtype U ->
     dmono U ->
     d2fsub' plain (dopen_tt S2 U) T2 f1 ->
     d2fsub' plain (dtyp_all S2) T2
            (ftrm_absann (dtyp_all S2)
                         (ftrm_app f1
                                   (ftrm_tapp (ftrm_bvar 0) U)))
  |d2fsub'_allr : forall S2 T2 f X,
     X \notin (ffv_tt S2 \u ffv_tt T2 \u ffv_te f) ->
     d2fsub' plain S2 (T2 dopen_tt_var X) (f fopen_te_var X) ->
     d2fsub' plain S2 (dtyp_all T2)
            (ftrm_absann S2
                         (ftrm_tabs (ftrm_app f
                                              (ftrm_bvar 0))))

  |d2fsub'_empty : forall A,
      dtype A ->
      d2fsub' (stack nil) A A
           (ftrm_absann A (ftrm_bvar 0))
  |d2fsub'_alll2 : forall S2 T2 U c cs f1,
     dtype U ->
     dmono U ->
     d2fsub' (stack (cons c cs)) (dopen_tt S2 U) T2 f1 ->
     d2fsub' (stack (cons c cs)) (dtyp_all S2) T2
            (ftrm_absann (dtyp_all S2)
                         (ftrm_app f1
                                   (ftrm_tapp (ftrm_bvar 0) U)))
  |d2fsub'_arrow_stack : forall S1 S2 T2 c cs f1 f2,
     d2fsub' plain c S1 f1 ->
     d2fsub' (stack cs) S2 T2 f2 ->
     d2fsub' (stack (cons c cs)) (dtyp_arrow S1 S2) (dtyp_arrow c T2)
            (ftrm_absann (ftyp_arrow S1 S2)
                         (ftrm_absann c
                                      (ftrm_app f2
                                                (ftrm_app (ftrm_bvar 1)
                                                          (ftrm_app f1 (ftrm_bvar 0))))))

  |d2fsub'_pair : forall T1 T2 T3 T4 f1 f2,
      d2fsub' plain T1 T3 f1 ->
      d2fsub' plain T2 T4 f2 ->
      d2fsub' plain (dtyp_pair T1 T2) (dtyp_pair T3 T4)
           (ftrm_absann (ftyp_pair T1 T2)
                        (ftrm_pair (ftrm_app f1 (ftrm_fst (ftrm_bvar 0)))
                                   (ftrm_app f2 (ftrm_snd (ftrm_bvar 0)))
                        ))
.

Hint Constructors d2fsub' d2fsub.
Hint Unfold ffv_tt.
Lemma d2fsub_d2fsub' : forall Q S T f,
    d2fsub Q S T f -> d2fsub' Q S T f.
Proof.
  introv sub. inductions sub; autos*.
  pick_fresh y.
  apply d2fsub'_allr with y; auto.
Qed.

Lemma d2fsub'_d2fsub : forall Q S T f,
    d2fsub' Q S T f -> d2fsub Q S T f.
Proof.
  introv sub. inductions sub; autos*.
  apply_fresh d2fsub_allr as x; auto.
  forwards * : d2fsub_rename x IHsub .
  unfold ffv_tt in H. auto.
  rewrite <- fsubst_te_intro in H0; auto.
Qed.

Lemma dsub_d2fsub' : forall Q S T,
  dsub Q S T ->
  exists f, d2fsub' Q S T f.
Proof.
  introv sub. inductions sub; autos*.
  destruct IHsub1. destruct IHsub2. autos*.
  destruct IHsub. autos*.

  pick_fresh y.
  forwards * : H0 y.
  destruct H1.
  exists* (ftrm_absann S2
                  (ftrm_tabs (ftrm_app (fclose_te y x)
                                       (ftrm_bvar 0)))).
  apply d2fsub'_allr with y; auto.
  assert (y \notin ffv_te (fclose_te y x)). apply fclose_te_fresh.
  autos*.
  unfold fopen_te. rewrite* <- fclose_te_open.
  lets : d2fsub'_d2fsub H1.
  lets : d2fsub_term H2. auto.

  destruct IHsub. autos*.
  destruct IHsub1. destruct IHsub2. autos*.

  destruct IHsub1. destruct IHsub2. autos*.
Qed.

Lemma dsub_d2fsub : forall Q S T,
  dsub Q S T ->
  exists f, d2fsub Q S T f.
Proof.
  introv h.
  lets: dsub_d2fsub' h.
  destruct H.
  exists* x. apply* d2fsub'_d2fsub.
Qed.

Hint Constructors ftyping.
Hint Resolve d2fsub_regular d2fsub_term.

Lemma d2fsub_type : forall Q S T f,
    d2fsub Q S T f ->
    ftyping empty f (ftyp_arrow S T).
Proof.
  introv sub.
  induction sub.
  apply_fresh ftyping_absann as x.
    unfold fopen_ee. simpls. case_nat*.

  apply_fresh ftyping_absann as x.
    unfold fopen_ee. simpls. case_nat*.

  lets [? ?]: d2fsub_regular sub1.
  lets [? ?]: d2fsub_regular sub2.
  apply_fresh ftyping_absann as x.
    apply_fresh ftyping_absann as y.
    unfold fopen_ee. simpls. case_nat*.
    unfold fopen_ee. simpls. case_nat*.
    unfold fopen_ee. simpls. case_nat*.
    repeat rewrite~ <- fopen_ee_rec_term.
    apply ftyping_app with S2; auto.
    apply ftyping_app with S1; auto.
    apply ftyping_app with T1; auto.
    apply ftyping_var; auto.
    rewrite <- concat_assoc.
    apply_empty* ftyping_weaken.
    rewrite~ concat_assoc. 
    apply ftyping_var; auto.
    rewrite <- concat_assoc.
    apply_empty* ftyping_weaken.
    rewrite~ concat_assoc.

  apply_fresh ftyping_absann as x.
    apply ftyping_app with (dopen_tt S2 U); auto.
    case_nat*.
    apply~ ftyping_tapp.
    apply~ ftyping_var.
    apply~ fokt_typ.
    lets [? [? ?]]: ftyping_regular IHsub; auto.
    lets [? [? ?]]: ftyping_regular IHsub; auto.
    inversion H3; subst.
    apply_fresh dtype_all as y.
    apply dtype_open_inv with U; auto.
    rewrite~ <- fopen_ee_rec_term.
    apply_empty* ftyping_weaken.
    apply~ fokt_typ.
    lets [? [? ?]]: ftyping_regular IHsub; auto.
    lets [? [? ?]]: ftyping_regular IHsub; auto.
    inversion H3; subst.
    apply_fresh dtype_all as y.
    apply dtype_open_inv with U; auto.

  apply_fresh ftyping_absann as y.
    unfold fopen_ee. simpls.
    case_nat*.
    apply_fresh ftyping_tabs as x.
    unfold fopen_te. simpls.
    rewrite <- fopen_te_ee.
    rewrite~ <- fopen_ee_rec_term.
    apply ftyping_app with S2; auto.
    apply~ ftyping_var.
    apply~ fokt_typ.
    specializes H y. forwards ~ : H.
    lets~ [? ?]: d2fsub_regular H1.

    apply_empty * ftyping_weaken.
    apply~ fokt_typ.
    specializes H y. forwards ~ : H.
    lets~ [? ?]: d2fsub_regular H1.
    forwards ~ : H x.
    lets~ : d2fsub_term H1.

  apply_fresh ftyping_absann as x.
    unfold fopen_ee. simpls. case_nat*.

  lets [? ?]: d2fsub_regular sub.
  lets [? [? ?]]: ftyping_regular IHsub.
  apply_fresh ftyping_absann as x.
    apply ftyping_app with (dopen_tt S2 U).
    case_nat*.
    apply~ ftyping_tapp. unfold ftyp_all.
    apply~ ftyping_var.
    apply~ fokt_typ.
    apply_fresh dtype_all as y.
    apply dtype_open_inv with U; auto.
    rewrite~ <- fopen_ee_rec_term.
    apply_empty* ftyping_weaken.
    apply~ fokt_typ.
    apply_fresh dtype_all as y.
    apply dtype_open_inv with U; auto.

  lets [? ?]: d2fsub_regular sub1.
  lets [? ?]: d2fsub_regular sub2.
  apply_fresh ftyping_absann as x.
    apply_fresh ftyping_absann as y.
    unfold fopen_ee. simpls. case_nat*.
    unfold fopen_ee. simpls. case_nat*.
    unfold fopen_ee. simpls. case_nat*.
    repeat rewrite~ <- fopen_ee_rec_term.
    apply ftyping_app with S2; auto.
    apply ftyping_app with S1; auto.
    apply ftyping_app with c; auto.
    apply ftyping_var; auto.
    rewrite <- concat_assoc.
    apply_empty* ftyping_weaken.
    rewrite~ concat_assoc.
    apply ftyping_var; auto.
    rewrite <- concat_assoc.
    apply_empty* ftyping_weaken.
    rewrite~ concat_assoc.

  lets [? ?]: d2fsub_regular sub1.
  lets [? ?]: d2fsub_regular sub2.
  apply_fresh ftyping_absann as x.
    unfolds fopen_ee. simpls. case_nat~.
    repeat rewrite~ <- fopen_ee_rec_term.
    apply~ ftyping_pair.
    apply ftyping_app with (T1 := T1); auto.
      apply ftyping_fst with (T2 := T2); auto.
      apply_empty* ftyping_weaken.
    apply ftyping_app with (T1 := T2); auto.
      apply ftyping_snd with (T1 := T1); auto.
      apply_empty* ftyping_weaken.
Qed.
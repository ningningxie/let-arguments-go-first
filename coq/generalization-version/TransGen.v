Set Implicit Arguments.
Require Import LibList.
Require Import LibLN.
Require Import DeclDef.
Require Import FDef.
Require Import FInfra.
Require Import DeclInfra.

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : dtrm => dfv_ee x) in
  let D := gather_vars_with (fun x : dtyp => dfv_tt x) in
  let E := gather_vars_with (fun x : denv => dom x) in
  let F := gather_vars_with (fun x : aenv => dfv_aenv x) in
  let G := gather_vars_with (fun x : dstack => dfv_stack x) in
  let H := gather_vars_with (fun x : ftrm => ffv_te x) in
  let I := gather_vars_with (fun x : ftrm => ffv_ee x) in
  let J := gather_vars_with (fun x : ftyp => ffv_tt x) in
  let K := gather_vars_with (fun x : fenv => dom x) in
  let L := gather_vars_with (fun x : denv => dfv_tt_env x) in
  let M := gather_vars_with (fun x : fenv => dfv_tt_env x) in
  constr:(A \u B \u C \u D \u E \u F \u G \u H \u I \u J \u K \u L \u M).

(** "pick_fresh x" tactic create a fresh variable with name x *)

Ltac pick_fresh x :=
  let L := gather_vars in (pick_fresh_gen L x).

(** "apply_fresh T as x" is used to apply inductive rule which
   use an universal quantification over a cofinite set *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

(** Generalization *)
Inductive d2fgen : denv -> dtyp -> ftrm -> dtyp -> ftrm -> Prop :=
  | d2fgen_empty : forall E t s,
      dokt E ->
      dtype t ->
      fterm s ->
      (dfv_tt t) \- (dfv_tt_env E) = \{} ->
      d2fgen E t s t s
  | d2fgen_step : forall x E t t2 s s2,
      x \in (dfv_tt (t fopen_tt_var x) \- dfv_tt_env E) ->
      x \notin dfv_tt t ->
      x \notin ffv_te s ->
      d2fgen E (dtyp_all t)
                 (ftrm_tabs s)
                 t2 s2 ->
      d2fgen E (t fopen_tt_var x) (s fopen_te_var x) t2 s2.

Hint Constructors dgen d2fgen fterm.

Lemma d2fgen_dgen : forall E T1 S1 T2 S2,
  d2fgen E T1 S1 T2 S2 -> dgen E T1 T2.
Proof.
  introv sub. inductions sub; autos*.
Qed.

Lemma d2fgen_regular : forall E T1 T2 S1 S2,
  d2fgen E T1 S1 T2 S2 -> dokt E /\ dtype T1 /\ dtype T2.
Proof.
  introv gen.
  lets: d2fgen_dgen gen.
  apply* dgen_regular.
Qed.

Lemma d2fgen_term : forall E T1 T2 S1 S2,
  d2fgen E T1 S1 T2 S2 -> fterm S1 /\ fterm S2.
Proof.
  induction 1.
  auto.
  destruct IHd2fgen as [? ?].
  split; auto.
  apply* fterm_open_tabs.
Qed.

Lemma d2fgen_type : forall E S1 T1 S2 T2,
  ftyping E S1 T1 ->
  d2fgen E T1 S1 T2 S2 ->
  ftyping E S2 T2.
Proof.
  introv ty gen.
  induction gen; simpls; auto.
  apply* IHgen.
  apply_fresh ftyping_tabs as y.

  apply ftyping_subst with (x:=x) (y:=y) in ty.
  rewrite <- fsubst_tt_intro in ty; auto.
  rewrite <- fsubst_te_intro in ty; auto.
  rewrite dsubst_tt_env_fresh in ty; auto.
  rewrite in_remove in H.
  destruct~ H.
Qed.

Lemma dgen_d2fgen: forall E S1 T1 T2,
    dgen E T1 T2 ->
    ftyping E S1 T1 ->
    exists S2,  d2fgen E T1 S1 T2 S2.
Proof.
  introv gen ty. gen S1. induction gen; introv ty.
  exists~ S1.

  assert (ftyping E (ftrm_tabs (fclose_te x S1))
                    (dtyp_all t)).
  apply_fresh ftyping_tabs as y.

  unfold fopen_te.
  rewrite~ fclose_te_open_subst.
  rewrite fsubst_tt_intro with (X:=x); auto.
  rewrite in_remove in H.
  destruct H as [int notine].
  assert (E = map (fsubst_tt x (ftyp_fvar y)) E).
  apply* dmap_subst_tt_id.
  rewrite H.
  apply~ ftyping_subst.

  assert (S1 = (fclose_te x S1) fopen_te_var x).
  rewrite~ <- fclose_te_open_var.
  rewrite H2.
  lets: IHgen H1.
  destruct H3 as (S2 & dfgen).
  assert (S2 = (fclose_te x S2) fopen_te_var x).
  rewrite~ <- fclose_te_open_var.
  lets [? ?] : d2fgen_term dfgen. auto.
  rewrite H3 in dfgen.
  exists (fclose_te x S2 fopen_te_var x).
  apply~ d2fgen_step.
  apply~ fclose_te_fresh.
Qed.

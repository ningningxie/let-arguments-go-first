Set Implicit Arguments.
Require Import LibList.
Require Import LibLN.
Require Import DeclDef.
Require Import FDef.
Require Import FInfra.
Require Import DeclInfra.
Require Import TransGen.
Require Import TransSubtyping.

Inductive d2ftyping : denv -> dstack -> dtrm -> dtyp -> ftrm -> Prop :=
  | d2ftyping_var : forall E x T f T2 cs,
      dokt E ->
      binds x T E ->
      d2fsub (stack cs) T T2 f ->
      d2ftyping E cs (dtrm_fvar x) T2
                (ftrm_app f (ftrm_fvar x))
  | d2ftyping_nat : forall E i,
      dokt E ->
      d2ftyping E nil (dtrm_nat i) (dtyp_nat)
                (ftrm_nat i)
  | d2ftyping_absann : forall L E V e1 T1 s,
      dwft \{} V ->
      (forall x, x \notin L ->
            d2ftyping (E & x ~ V) nil (e1 dopen_ee_var x) T1 (s fopen_ee_var x)) ->
      d2ftyping E nil (dtrm_absann V e1) (dtyp_arrow V T1)
                (ftrm_absann V s)
  | d2ftyping_abs : forall L E t e1 T1 s,
      dtype t ->
      dmono t ->
      (forall x, x \notin L ->
            d2ftyping (E & x ~ t) nil (e1 dopen_ee_var x) T1 (s fopen_ee_var x)) ->
      d2ftyping E nil (dtrm_abs e1) (dtyp_arrow t T1)
                (ftrm_absann t s)

  | d2ftyping_absann_stack : forall L E V e1 T1 c cs f s,
      dwft \{} V ->
      d2fsub plain c V f ->
      (forall x, x \notin L ->
            d2ftyping (E & x ~ V) cs (e1 dopen_ee_var x) T1 (s fopen_ee_var x)) ->
      d2ftyping E (cons c cs) (dtrm_absann V e1) (dtyp_arrow c T1)
              (ftrm_absann c
                           (ftrm_app (ftrm_absann V s) (ftrm_app f (ftrm_bvar 0))))
  | d2ftyping_abs_stack : forall L E e1 T1 c cs s,
      (forall x, x \notin L ->
            d2ftyping (E & x ~ c) cs (e1 dopen_ee_var x) T1 (s fopen_ee_var x)) ->
      d2ftyping E (cons c cs) (dtrm_abs e1) (dtyp_arrow c T1) (ftrm_absann c s)
  | d2ftyping_app : forall T1 T3 E e1 e2 T2 cs s1 s2 s3,
      d2ftyping E nil e2 T2 s2 ->
      d2fgen E T2 s2 T3 s3 ->
      d2ftyping E (cons T3 cs) e1 (dtyp_arrow T3 T1) s1 ->
      d2ftyping E cs (dtrm_app e1 e2) T1 (ftrm_app s1 s3)

  | d2ftyping_pair : forall E e1 e2 T1 T2 s1 s2,
      d2ftyping E nil e1 T1 s1 ->
      d2ftyping E nil e2 T2 s2 ->
      d2ftyping E nil (dtrm_pair e1 e2) (dtyp_pair T1 T2) (ftrm_pair s1 s2)
  | d2ftyping_fst : forall E T cs f,
      dokt E ->
      d2fsub (stack cs) fst_typ T f ->
      d2ftyping E cs dtrm_fst T (ftrm_app f
                                          (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                                                             (ftrm_fst (ftrm_bvar 0))
                                          ))))
  | d2ftyping_snd : forall E T cs f,
      dokt E ->
      d2fsub (stack cs) snd_typ T f ->
      d2ftyping E cs dtrm_snd T (ftrm_app f
                                          (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                                                             (ftrm_snd (ftrm_bvar 0))
                                          ))))
.

Hint Constructors d2ftyping.
Hint Constructors dtyping.
Hint Resolve d2fsub_dsub.
Hint Resolve d2fgen_dgen.

Lemma d2ftyping_dtyping : forall E Q S T f,
  d2ftyping E Q S T f -> dtyping E Q S T.
Proof.
  introv ty. inductions ty; autos*.
Qed.

Lemma d2ftyping_regular : forall E Q e T f,
  d2ftyping E Q e T f -> dokt E /\ dterm e /\ dtype T.
Proof.
  introv ty. lets: d2ftyping_dtyping ty.
  lets ~ [? [? [? ?]]]: dtyping_regular H.
Qed.

Hint Resolve d2fsub_regular d2ftyping_regular.
Lemma d2ftyping_term : forall E Q S T f,
  d2ftyping E Q S T f -> fterm f.
Proof.
  introv ty. inductions ty; autos*.
  apply_fresh fterm_absann as y; auto.
  pick_fresh y.
  specializes H0 y. forwards ~ : H0.
  lets [? [? ?]]: d2ftyping_regular H2.
  lets~ [? [? ?]]: dokt_push_typ_inv H3.

  apply_fresh fterm_absann as y; auto.
  lets~ [? ?]: d2fsub_regular H0.
  unfold fopen_ee. simpl. case_nat*.
  unsimpl (fopen_ee_rec 0 (ftrm_fvar y) (ftrm_absann V s)).
  rewrite~ <- fopen_ee_rec_term.
  apply~ fterm_app.
  apply_fresh fterm_absann as x.
  lets~ [? ?]: d2fsub_regular H0.
  apply* H2.
  rewrite* <- fopen_ee_rec_term.
  apply_fresh fterm_absann as x.
  lets~ [? ?]: d2fsub_regular H0.
  apply* H2.

  apply_fresh fterm_absann as y; auto.
  pick_fresh y.
  specializes H y. forwards ~ : H.
  lets [? ?]: d2ftyping_regular H1.
  lets~ [? [? ?]]: dokt_push_typ_inv H2.

  apply~ fterm_app.
  lets~ [? ?]: d2fgen_term H.

  lets : d2fsub_term H0.
  apply~ fterm_app.
  apply_fresh fterm_tabs as a; auto.
  apply_fresh fterm_tabs as b; auto. simpls~. repeat case_nat~.
  apply_fresh fterm_absann as c; auto.
  simpls~. case_nat~.
  unfolds fopen_ee.
  simpls~. case_nat~.

  lets : d2fsub_term H0.
  apply~ fterm_app.
  apply_fresh fterm_tabs as a; auto.
  apply_fresh fterm_tabs as b; auto. simpls~. repeat case_nat~.
  apply_fresh fterm_absann as c; auto.
  simpls~. case_nat~.
  unfolds fopen_ee.
  simpls~. case_nat~.
Qed.

Hint Resolve d2ftyping_term d2fsub_term.

Lemma d2ftyping_type : forall E Q e T f,
  d2ftyping E Q e T f -> ftyping E f T.
Proof.
  introv ty. inductions ty; autos*.

  apply ftyping_app with T; auto.
  lets: d2fsub_type H1.
  assert (E = empty & E) by rewrite~ concat_empty_l.
  rewrite H3.
  apply_empty* ftyping_weaken.
  rewrite~ concat_empty_l.

  apply_fresh ftyping_absann as y.
  unfold fopen_ee. simpl. case_nat*.
  unsimpl (fopen_ee_rec 0 (ftrm_fvar y) (ftrm_absann V s)).
  rewrite~ <- fopen_ee_rec_term.
  rewrite~ <- fopen_ee_rec_term.
  apply ftyping_app with V.
  apply ftyping_app with c.
  apply~ ftyping_var. apply~ fokt_typ.
  specializes H2 y. forwards ~ : H2.
  lets [? [? ?]]: ftyping_regular H3.
  lets [? ?]: fokt_push_typ_inv H4.
  unfolds~ fokt.
  lets~ [? ?]: d2fsub_regular H0.
  lets : d2fsub_type H0.
  assert (E & y ~ c= empty & (E & y ~c)) by rewrite~ concat_empty_l.
  rewrite H4.
  apply_empty* ftyping_weaken.
  rewrite~ concat_empty_l.
  apply~ fokt_typ.
  specializes H2 y. forwards ~ : H2.
  lets [? [? ?]]: ftyping_regular H5.
  lets [? ?]: fokt_push_typ_inv H6.
  unfolds~ fokt.
  lets~ [? ?]: d2fsub_regular H0.

  apply_fresh ftyping_absann as x.
  forwards * : H2 x. apply* ftyping_weaken.
  apply~ fokt_typ.
  apply~ fokt_typ.
  lets~ [? [? ?]]: ftyping_regular H3.
  lets [? ?]: fokt_push_typ_inv H4.
  unfolds~ fokt.
  lets~ [? ?]: d2fsub_regular H0.
  lets~ [? ?]: d2fsub_regular H0.
  lets ~ : d2fsub_term H0.

  apply_fresh fterm_absann as x.
  lets~ [? ?]: d2fsub_regular H0.
  forwards ~ : H2 x.

  apply ftyping_app with T3; auto.
  lets~ : d2fgen_type IHty1 H.

  lets: d2fsub_type H0.
  apply ftyping_app with fst_typ; autos.
  apply_fresh ftyping_tabs as a; autos.
  apply_fresh ftyping_tabs as b; autos.
  unfold fopen_te. unfolds fopen_tt.
  simpls~. repeat case_nat~.
  simpls~. repeat case_nat~.
  apply_fresh ftyping_absann as c; autos*.
  unfolds fopen_ee.
  simpls~. repeat case_nat~.
  apply* ftyping_fst.
  assert (E = empty & E) by rewrite~ concat_empty_l.
  rewrite H2.
  apply_empty* ftyping_weaken.
  rewrite~ concat_empty_l.

  lets: d2fsub_type H0.
  apply ftyping_app with snd_typ; autos.
  apply_fresh ftyping_tabs as a; autos.
  apply_fresh ftyping_tabs as b; autos.
  unfold fopen_te. unfolds fopen_tt.
  simpls~. repeat case_nat~.
  simpls~. repeat case_nat~.
  apply_fresh ftyping_absann as c; autos*.
  unfolds fopen_ee.
  simpls~. repeat case_nat~.
  apply* ftyping_snd.
  assert (E = empty & E) by rewrite~ concat_empty_l.
  rewrite H2.
  apply_empty* ftyping_weaken.
  rewrite~ concat_empty_l.
Qed.

Inductive d2ftyping' : denv -> dstack -> dtrm -> dtyp -> ftrm -> Prop :=
  | d2ftyping'_var : forall E x T f T2 cs,
      dokt E ->
      binds x T E ->
      d2fsub (stack cs) T T2 f ->
      d2ftyping' E cs (dtrm_fvar x) T2
                (ftrm_app f (ftrm_fvar x))
  | d2ftyping'_nat : forall E i,
      dokt E ->
      d2ftyping' E nil (dtrm_nat i) (dtyp_nat)
                (ftrm_nat i)
  | d2ftyping'_absann : forall x E V e1 T1 s,
      dwft \{} V ->
      x \notin (dom E \u dfv_ee e1 \u dfv_tt V \u dfv_tt T1 \u ffv_ee s) ->
      d2ftyping' (E & x ~ V) nil (e1 dopen_ee_var x) T1 (s fopen_ee_var x) ->
      d2ftyping' E nil (dtrm_absann V e1) (dtyp_arrow V T1)
                (ftrm_absann V s)
  | d2ftyping'_abs : forall x E t e1 T1 s,
      dtype t ->
      dmono t ->
      x \notin (dom E \u dfv_ee e1 \u dfv_tt t \u dfv_tt T1 \u ffv_ee s) ->
      d2ftyping' (E & x ~ t) nil (e1 dopen_ee_var x) T1 (s fopen_ee_var x) ->
      d2ftyping' E nil (dtrm_abs e1) (dtyp_arrow t T1)
                (ftrm_absann t s)

  | d2ftyping'_absann_stack : forall x E V e1 T1 c cs f s,
      dwft \{} V ->
      d2fsub plain c V f ->
      x \notin (dom E \u dfv_tt V \u dfv_ee e1 \u dfv_tt T1 \u ffv_ee s \u dfv_stack cs \u dfv_tt c) ->
      d2ftyping' (E & x ~ V) cs (e1 dopen_ee_var x) T1 (s fopen_ee_var x) ->
      d2ftyping' E (cons c cs) (dtrm_absann V e1) (dtyp_arrow c T1)
              (ftrm_absann c
                           (ftrm_app (ftrm_absann V s) (ftrm_app f (ftrm_bvar 0))))
  | d2ftyping'_abs_stack : forall x E e1 T1 c cs s,
      x \notin dom E \u dfv_tt c \u dfv_ee e1 \u dfv_tt T1 \u ffv_ee s \u dfv_stack cs->
      d2ftyping' (E & x ~ c) cs (e1 dopen_ee_var x) T1 (s fopen_ee_var x) ->
      d2ftyping' E (cons c cs) (dtrm_abs e1) (dtyp_arrow c T1) (ftrm_absann c s)
  | d2ftyping'_app : forall T1 E e1 e2 T2 T3 cs s1 s2 s3,
      d2ftyping' E nil e2 T2 s2 ->
      d2fgen E T2 s2 T3 s3 ->
      d2ftyping' E (cons T3 cs) e1 (dtyp_arrow T3 T1) s1 ->
      d2ftyping' E cs (dtrm_app e1 e2) T1 (ftrm_app s1 s3)

  | d2ftyping'_pair : forall E e1 e2 T1 T2 s1 s2,
      d2ftyping' E nil e1 T1 s1 ->
      d2ftyping' E nil e2 T2 s2 ->
      d2ftyping' E nil (dtrm_pair e1 e2) (dtyp_pair T1 T2) (ftrm_pair s1 s2)
  | d2ftyping'_fst : forall E T cs f,
      dokt E ->
      d2fsub (stack cs) fst_typ T f ->
      d2ftyping' E cs dtrm_fst T (ftrm_app f
                                          (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                                                             (ftrm_fst (ftrm_bvar 0))
                                          ))))
  | d2ftyping'_snd : forall E T cs f,
      dokt E ->
      d2fsub (stack cs) snd_typ T f ->
      d2ftyping' E cs dtrm_snd T (ftrm_app f
                                          (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                                                             (ftrm_snd (ftrm_bvar 0))
                                          ))))
.

Hint Constructors d2ftyping'.
Lemma d2ftyping_d2ftyping': forall E Q e t f,
    d2ftyping E Q e t f -> d2ftyping' E Q e t f.
Proof.
  induction 1; autos*.
  pick_fresh x.
  apply d2ftyping'_absann with x; auto.
  pick_fresh x.
  apply d2ftyping'_abs with x; auto.
  pick_fresh x.
  apply d2ftyping'_absann_stack with x; auto.
  pick_fresh x.
  apply d2ftyping'_abs_stack with x; auto.
Qed.

Lemma d2fgen_subst: forall E F z u U s1 s2 T1 T2,
    d2fgen (E & z ~ U & F) T1 s1 T2 s2 ->
    dokt (E & u ~ U & F) ->
    d2fgen (E & u ~ U & F) T1 (fsubst_ee z (ftrm_fvar u) s1)
           T2 (fsubst_ee z (ftrm_fvar u) s2).
Proof.
  introv gen. inductions gen; introv oke; simpls~.
  apply~ d2fgen_empty.
  rewrite dfv_tt_env_middle_inv in H2.
  rewrite~ dfv_tt_env_middle_inv.

  specializes IHgen E F z U.
  forwards ~  : IHgen.
  rewrite~ <- fsubst_ee_open_te_var.
  apply~ d2fgen_step.
  rewrite in_remove in H.
  destruct H as [int notin e].
  rewrite in_remove. split~.
  rewrite dfv_tt_env_middle_inv in notin.
  rewrite~ dfv_tt_env_middle_inv.
  apply* fnotin_fv_te_subst_ee_inv.
  simpls~.
Qed.

Lemma d2ftyping_subst : forall E F Q t T z u U f,
    d2ftyping (E & z ~ U & F) Q t T f ->
    dokt (E & u ~ U & F) ->
    d2ftyping (E & u ~ U & F) Q (dsubst_ee z (dtrm_fvar u) t) T
              (fsubst_ee z (ftrm_fvar u) f) .
Proof.
  introv typt. inductions typt; introv okt; simpl; f_equal~.
  case_var*.
    binds_mid~.
    apply d2ftyping_var with U; autos*.
    apply~ binds_middle_eq.
    lets: dok_from_okt okt.
    lets ~ : ok_middle_inv_r H0.

    apply* d2ftyping_var.
    forwards ~ : binds_remove H0.
    apply~ binds_weaken.

  apply_fresh d2ftyping_absann as x; auto.
  specializes H1 x E (F & x ~ V) z U.
  forwards * : H1.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H0 x. forwards ~ : H0.
  lets [? _]: d2ftyping_regular H2. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H2.
  rewrite* dsubst_ee_open_ee_var.
  rewrite* fsubst_ee_open_ee_var.

  apply_fresh d2ftyping_abs as x; auto.
  specializes H2 x E (F & x ~ t) z U.
  forwards ~ : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.
  rewrite* fsubst_ee_open_ee_var.

  apply_fresh d2ftyping_absann_stack as x; auto.
  specializes H2 x E (F & x ~ V) z U.
  forwards * : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H1 x. forwards ~ : H1.
  lets [? _]: d2ftyping_regular H3. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.
  rewrite* fsubst_ee_open_ee_var.

  apply_fresh d2ftyping_abs_stack as x.
  specializes H0 x E (F & x ~ c) z U.
  forwards ~ : H0.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H x. forwards ~ : H.
  lets [? _]: d2ftyping_regular H1. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H1.
  rewrite* dsubst_ee_open_ee_var.
  rewrite* fsubst_ee_open_ee_var.

  apply d2ftyping_app with (T2:=T2) (T3:=T3) (s2:=fsubst_ee z (ftrm_fvar u) s2).
  specializes IHtypt1 E F z U.
  apply* d2fgen_subst.
  specializes IHtypt2 E F z U.
Qed.

Lemma d2ftyping_rename : forall x Q y E t S f V,
  d2ftyping (E & x ~ V) Q (S dopen_ee_var x) t f ->
  x \notin dom E \u dfv_ee S ->
  y \notin dom E \u dfv_ee S ->
  d2ftyping (E & y ~ V) Q (S dopen_ee_var y) t (fsubst_ee x (ftrm_fvar y) f).
Proof.
  introv Typx Frx Fry.
  tests: (x = y). subst*. rewrite* fsubst_ee_same.
  rewrite~ (@dsubst_ee_intro x).
  assert  (d2ftyping (E & y ~ V & empty) Q (dsubst_ee x (dtrm_fvar y) (S dopen_ee_var x)) t (fsubst_ee x (ftrm_fvar y) f)).
  apply d2ftyping_subst.
  rewrite~ concat_empty_r.
  rewrite~ concat_empty_r.
  apply~ dokt_typ.
  lets [? ?]: d2ftyping_regular Typx.
  lets~ : dokt_concat_left_inv H.
  lets [? ?]: d2ftyping_regular Typx.
  apply* dtype_from_okt_typ.

  rewrite~ concat_empty_r in H.
Qed.

Lemma d2ftyping'_d2ftyping: forall E Q e t f,
    d2ftyping' E Q e t f -> d2ftyping E Q e t f.
Proof.
  introv ty. inductions ty; autos~.
  apply* d2ftyping_var.
  apply_fresh d2ftyping_absann as x; auto.
  forwards~ : d2ftyping_rename y IHty; simpls~.
  rewrite <- fsubst_ee_intro in H1; auto.
  apply_fresh d2ftyping_abs as x; auto.
  forwards* : d2ftyping_rename y IHty; simpls~.
  rewrite <- fsubst_ee_intro in H2; auto.
  apply_fresh d2ftyping_absann_stack as x; auto.
  forwards* : d2ftyping_rename y IHty; simpls*.
  rewrite <- fsubst_ee_intro in H2; auto.
  apply_fresh d2ftyping_abs_stack as x; auto.
  forwards * : d2ftyping_rename y IHty.
  rewrite <- fsubst_ee_intro in H0; auto.
  apply* d2ftyping_app.
Qed.

Hint Constructors d2ftyping'.
Lemma dtyping_d2ftyping': forall E Q S T,
  dtyping E Q S T -> exists f, d2ftyping' E Q S T f.
Proof.
  introv ty. inductions ty; autos*.
  lets: dsub_d2fsub H1. inversion H2.
  exists* (ftrm_app x0 (ftrm_fvar x)).

  pick_fresh y.
  specializes H1 y.
  destruct H1; auto.
  exists* (ftrm_absann V (fclose_ee y x)).
  apply d2ftyping'_absann with y; auto.
  assert (y \notin ffv_ee (fclose_ee y x)). apply fclose_ee_fresh.
  autos*. rewrite* <- fclose_ee_open.
  lets : d2ftyping'_d2ftyping H1.
  lets : d2ftyping_term H2. auto.

  pick_fresh y.
  specializes H2 y.
  destruct H2; auto.
  exists (ftrm_absann t (fclose_ee y x)).
  apply d2ftyping'_abs with y; auto.
  assert (y \notin ffv_ee (fclose_ee y x)). apply fclose_ee_fresh.
  autos*. rewrite* <- fclose_ee_open.
  lets : d2ftyping'_d2ftyping H2.
  lets : d2ftyping_term H3. auto.

  pick_fresh y.
  specializes H2 y.
  destruct H2; auto.
  lets: dsub_d2fsub H0.
  destruct H3.
  exists* (ftrm_absann c
                  (ftrm_app (ftrm_absann V (fclose_ee y x))
                            (ftrm_app x0
                                      (ftrm_bvar 0)))).
  apply d2ftyping'_absann_stack with y; auto.
  assert (y \notin ffv_ee (fclose_ee y x)). apply fclose_ee_fresh.
  autos*. rewrite* <- fclose_ee_open.
  lets : d2ftyping'_d2ftyping H2.
  lets : d2ftyping_term H4. auto.

  pick_fresh y.
  specializes H0 y.
  destruct H0; auto.
  exists* (ftrm_absann c (fclose_ee y x)).
  apply d2ftyping'_abs_stack with y; auto.
  assert (y \notin ffv_ee (fclose_ee y x)). apply fclose_ee_fresh.
  autos*. rewrite* <- fclose_ee_open.
  lets : d2ftyping'_d2ftyping H0.
  lets : d2ftyping_term H1. auto.

  destruct IHty1.
  destruct IHty2.
  lets: d2ftyping'_d2ftyping H0.
  lets: d2ftyping_type H2.
  lets: dgen_d2fgen H H3.
  destruct H4.
  exists* (ftrm_app x0 x1).

  destruct IHty1.
  destruct IHty2.
  exists* (ftrm_pair x x0).

  lets: dsub_d2fsub H0. inversion H1.
  exists*
  (ftrm_app x
            (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                               (ftrm_fst (ftrm_bvar 0))
  )))).

  lets: dsub_d2fsub H0. inversion H1.
  exists*
  (ftrm_app x
            (ftrm_tabs (ftrm_tabs (ftrm_absann (dtyp_pair (dtyp_bvar 1) (dtyp_bvar 0))
                                               (ftrm_snd (ftrm_bvar 0))
  )))).
Qed.

Lemma dtyping_d2ftyping: forall E Q S T,
  dtyping E Q S T -> exists f, d2ftyping E Q S T f.
Proof.
  introv ty.
  lets: dtyping_d2ftyping' ty.
  destruct H.
  lets: d2ftyping'_d2ftyping H.
  exists* x.
Qed.

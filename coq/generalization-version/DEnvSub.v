Require Import LibLN DeclDef.
Require Import DeclInfra.
Require Import DeclSound.

Set Implicit Arguments.

Inductive denv_sub : denv -> denv -> Prop :=
| denv_sub_relf: denv_sub empty empty
| denv_sub_add : forall a x y e1 e2,
    denv_sub e1 e2 ->
    dsub plain x y ->
    a # e1 \/ a # e2 ->
    denv_sub (e1 & a ~ x) (e2 & a ~ y).

Hint Constructors denv_sub.

Lemma denv_sub_var_preservation: forall E F a,
    denv_sub E F ->
    a # F -> a # E.
Proof.
  introv sub. induction sub; simpls~.
Qed.

Lemma denv_sub_var_preservation_inv: forall E F a,
    denv_sub E F ->
    a # E -> a # F.
Proof.
  introv sub. induction sub; simpls~.
Qed.

Lemma denv_sub_regular: forall E F,
    denv_sub E F ->
    dokt E /\ dokt F.
Proof.
  introv sub. induction sub; simpls~.
  destruct IHsub.
  split~.
  apply~ dokt_typ. apply (proj21 (dsub_regular H)).
  destruct~ H0. apply* denv_sub_var_preservation.
  apply~ dokt_typ. apply (proj22 (dsub_regular H)).
  destruct~ H0. apply* denv_sub_var_preservation_inv.
Qed.

Lemma denv_sub_self: forall E,
    dokt E ->
    denv_sub E E.
Proof.
  introv oke; induction oke; simpls~.
Qed.

Lemma denv_sub_binds: forall x t E F,
    binds x t F ->
    denv_sub E F ->
    exists t2, binds x t2 E /\ dsub plain t2 t.
Proof.
  introv bd sub. induction sub.
  false* binds_empty_inv.
  tests: (x = a).
  apply binds_push_eq_inv in bd. subst~.
  exists x0. split~.

  apply binds_push_neq_inv in bd; auto.
  apply IHsub in bd.
  destruct bd as (t2 & [bd2 sub2]).
  exists~ t2.
Qed.
Set Implicit Arguments.
Require Import LibList LibLN DeclDef.

(** Computing free term variables in a term *)

Fixpoint dfv_ee (e : dtrm) {struct e} : vars :=
  match e with
  | dtrm_bvar i       => \{}
  | dtrm_fvar x       => \{x}
  | dtrm_nat i           => \{}
  | dtrm_abs e1       => (dfv_ee e1)
  | dtrm_absann V e1  => (dfv_ee e1)
  | dtrm_app e1 e2    => (dfv_ee e1) \u (dfv_ee e2)
  | dtrm_pair e1 e2   => (dfv_ee e1) \u (dfv_ee e2)
  | dtrm_fst          => \{}
  | dtrm_snd          => \{}
  end.

(** Computing free typ variables in dstack *)

Fixpoint dfv_stack (G : dstack) {struct G} : vars :=
  match G with
  | nil => \{}
  | cons c cs => dfv_tt c \u (dfv_stack cs)
  end.

Fixpoint dfv_aenv (G : aenv) {struct G} : vars :=
  match G with
  | plain => \{}
  | stack s => dfv_stack s
  end.

(** Substitution for free type variables in types. *)

Fixpoint dsubst_tt (Z : var) (U : dtyp) (T : dtyp) {struct T} : dtyp :=
  match T with
  | dtyp_nat         => dtyp_nat
  | dtyp_bvar J      => dtyp_bvar J
  | dtyp_fvar X      => If X = Z then U else (dtyp_fvar X)
  | dtyp_arrow T1 T2 => dtyp_arrow (dsubst_tt Z U T1) (dsubst_tt Z U T2)
  | dtyp_all T1      => dtyp_all (dsubst_tt Z U T1)
  | dtyp_pair T1 T2  => dtyp_pair (dsubst_tt Z U T1) (dsubst_tt Z U T2)
  end.

(** Substitution for free term variables in terms. *)

Fixpoint dsubst_ee (z : var) (u : dtrm) (e : dtrm) {struct e} : dtrm :=
  match e with
  | dtrm_bvar i       => dtrm_bvar i
  | dtrm_fvar x       => If x = z then u else (dtrm_fvar x)
  | dtrm_nat i        => dtrm_nat i
  | dtrm_abs e1       => dtrm_abs (dsubst_ee z u e1)
  | dtrm_absann V e1  => dtrm_absann V (dsubst_ee z u e1)
  | dtrm_app e1 e2    => dtrm_app (dsubst_ee z u e1) (dsubst_ee z u e2)
  | dtrm_pair e1 e2   => dtrm_pair (dsubst_ee z u e1) (dsubst_ee z u e2)
  | dtrm_fst          => dtrm_fst
  | dtrm_snd          => dtrm_snd
  end.

(** Substitution for free type variables in stack. *)

Definition dsubst_stack (Z: var) (P: dtyp) (b:dstack) : dstack := List.map (dsubst_tt Z P) b.

(** Substitution for free type variables in application context. *)

Definition dsubst_aenv (Z: var) (P: dtyp) (a:aenv) : aenv :=
  match a with
    | plain => plain
    | stack s => stack (dsubst_stack Z P s)
  end.

Inductive dstack_type : dstack -> Prop :=
| dstack_type_empty: dstack_type nil
| dstack_type_cons : forall t s,
    dtype t ->
    dstack_type s ->
    dstack_type (t :: s).

Inductive daenv_type : aenv -> Prop :=
| daenv_type_plain: daenv_type plain
| daenv_type_stack : forall s,
    dstack_type s ->
    daenv_type (stack s).
Hint Constructors dstack_type daenv_type.

(* ********************************************************************** *)
(** * Tactics *)

(** Constructors as hints. *)

Hint Constructors dtype dterm dokt dokt dmono.

Hint Resolve
  dsub_nat dsub_tvar dsub_arrow
  dtyping_var dtyping_app.

(** Gathering free names already used in the proofs *)

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : dtrm => dfv_ee x) in
  let D := gather_vars_with (fun x : dtyp => dfv_tt x) in
  let E := gather_vars_with (fun x : denv => dom x) in
  let F := gather_vars_with (fun x : aenv => dfv_aenv x) in
  let G := gather_vars_with (fun x : dstack => dfv_stack x) in
  let H := gather_vars_with (fun x : denv => dfv_tt_env x) in
  constr:(A \u B \u C \u D \u E \u F \u G \u H).

(** "pick_fresh x" tactic create a fresh variable with name x *)

Ltac pick_fresh X :=
  let L := gather_vars in (pick_fresh_gen L X).

(** "apply_fresh T as x" is used to apply inductive rule which
   use an universal quantification over a cofinite set *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

(* ********************************************************************** *)
(** * Properties of Substitutions *)

(* ********************************************************************** *)
(** ** Properties of type substitution in type *)

(** Substitution on indices is identity on well-formed terms. *)

Lemma dopen_tt_rec_type_core : forall T j V U i, i <> j ->
  (dopen_tt_rec j V T) = dopen_tt_rec i U (dopen_tt_rec j V T) ->
  T = dopen_tt_rec i U T.
Proof.
  induction T; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma dopen_tt_rec_type : forall T U,
  dtype T -> forall k, T = dopen_tt_rec k U T.
Proof.
  induction 1; intros; simpl; f_equal*.
  pick_fresh X. apply* (@dopen_tt_rec_type_core T2 0 (dtyp_fvar X)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma dsubst_tt_fresh : forall Z U T,
  Z \notin dfv_tt T -> dsubst_tt Z U T = T.
Proof.
  induction T; simpl; intros; f_equal*.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma dsubst_tt_open_tt_rec : forall T1 T2 X P n, dtype P ->
  dsubst_tt X P (dopen_tt_rec n T2 T1) =
  dopen_tt_rec n (dsubst_tt X P T2) (dsubst_tt X P T1).
Proof.
  introv WP. generalize n.
  induction T1; intros k; simpls; f_equal*.
  case_nat*.
  case_var*. rewrite* <- dopen_tt_rec_type.
Qed.

Lemma dsubst_tt_open_tt : forall T1 T2 X P, dtype P ->
  dsubst_tt X P (dopen_tt T1 T2) =
  dopen_tt (dsubst_tt X P T1) (dsubst_tt X P T2).
Proof.
  unfold dopen_tt. autos* dsubst_tt_open_tt_rec.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma dsubst_tt_open_tt_var : forall X Y U T, Y <> X -> dtype U ->
  (dsubst_tt X U T) dopen_tt_var Y = dsubst_tt X U (T dopen_tt_var Y).
Proof.
  introv Neq Wu. rewrite* dsubst_tt_open_tt.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma dsubst_tt_intro : forall X T2 U,
  X \notin dfv_tt T2 -> dtype U ->
  dopen_tt T2 U = dsubst_tt X U (T2 dopen_tt_var X).
Proof.
  introv Fr Wu. rewrite* dsubst_tt_open_tt.
  rewrite* dsubst_tt_fresh. simpl. case_var*.
Qed.

(** substitute a mono type with a mono type gives back mono type **)

Lemma dsubst_mono: forall U u z,
    dmono U ->
    dmono u ->
    dmono (dsubst_tt z u U).
Proof.
  introv mu me. inductions mu; simpls; auto.
  case_var*.
Qed.

(* ********************************************************************** *)
(** ** Properties of type substitution in env *)

Lemma  dfv_tt_env_push_inv: forall G x v,
  dfv_tt_env (G & x ~ v) = dfv_tt v \u dfv_tt_env G.
Proof.
  introv.
  rewrite <- cons_to_push.
  simpls~.
Qed.

Lemma dsubst_tt_env_fresh : forall E x U,
    x \notin dfv_tt_env E ->
    map (dsubst_tt x U) E = E.
Proof.
  induction E using env_ind; introv notin; simpls~.
  rewrite~ map_empty.
  rewrite~ map_push.
  rewrite dfv_tt_env_push_inv in notin.
  apply notin_union in notin.
  rewrite* dsubst_tt_fresh.
  rewrite* IHE.
Qed.

Lemma  dfv_tt_env_empty:
  dfv_tt_env empty = \{}.
Proof.
  rewrite empty_def. simpls~.
Qed.

Lemma  dfv_tt_env_concat_inv: forall G F,
  dfv_tt_env (G & F) = dfv_tt_env G \u dfv_tt_env F.
Proof.
  introv. induction F using env_ind.
  repeat rewrite concat_empty_r.
  rewrite~ dfv_tt_env_empty.
  rewrite~ union_empty_r.

  rewrite concat_assoc.
  rewrite dfv_tt_env_push_inv.
  rewrite dfv_tt_env_push_inv.
  rewrite IHF.
  rewrite~ union_comm_assoc.
Qed.

Lemma  dfv_tt_env_middle_inv: forall G x v F,
  dfv_tt_env (G & x ~ v & F) = dfv_tt v \u dfv_tt_env G \u dfv_tt_env F.
Proof.
  introv. induction F using env_ind.
  repeat rewrite concat_empty_r.
  rewrite~ dfv_tt_env_empty.
  rewrite~ union_empty_r.
  apply* dfv_tt_env_push_inv.

  rewrite concat_assoc.
  rewrite dfv_tt_env_push_inv.
  rewrite dfv_tt_env_push_inv.
  rewrite IHF.
  rewrite union_comm_assoc. f_equal.
  rewrite~ union_comm_assoc.
Qed.

(* ********************************************************************** *)
(** ** Properties of term substitution in terms *)

Lemma dopen_ee_rec_term_core : forall e j v u i, i <> j ->
  dopen_ee_rec j v e = dopen_ee_rec i u (dopen_ee_rec j v e) ->
  e = dopen_ee_rec i u e.
Proof.
  induction e; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma dopen_ee_rec_term : forall u e,
  dterm e -> forall k, e = dopen_ee_rec k u e.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds dopen_ee. pick_fresh x.
   apply* (@dopen_ee_rec_term_core e1 0 (dtrm_fvar x)).
  unfolds dopen_ee. pick_fresh x.
   apply* (@dopen_ee_rec_term_core e1 0 (dtrm_fvar x)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma dsubst_ee_fresh : forall x u e,
  x \notin dfv_ee e -> dsubst_ee x u e = e.
Proof.
  induction e; simpl; intros; f_equal*.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma dsubst_ee_open_ee : forall t1 t2 u x, dterm u ->
  dsubst_ee x u (dopen_ee t1 t2) =
  dopen_ee (dsubst_ee x u t1) (dsubst_ee x u t2).
Proof.
  intros. unfold dopen_ee. generalize 0.
  induction t1; intros; simpls; f_equal*.
  case_nat*.
  case_var*. rewrite* <- dopen_ee_rec_term.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma dsubst_ee_open_ee_var : forall x y u e, y <> x -> dterm u ->
  (dsubst_ee x u e) dopen_ee_var y = dsubst_ee x u (e dopen_ee_var y).
Proof.
  introv Neq Wu. rewrite* dsubst_ee_open_ee.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma dsubst_ee_intro : forall x u e,
  x \notin dfv_ee e -> dterm u ->
  dopen_ee e u = dsubst_ee x u (e dopen_ee_var x).
Proof.
  introv Fr Wu. rewrite* dsubst_ee_open_ee.
  rewrite* dsubst_ee_fresh. simpl. case_var*.
Qed.

(** Substitutions preserve local closure. *)

Lemma dsubst_tt_type : forall T Z P,
  dtype T -> dtype P -> dtype (dsubst_tt Z P T).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* dtype_all as X. rewrite* dsubst_tt_open_tt_var.
Qed.

Lemma dsubst_ee_term : forall e1 Z e2,
  dterm e1 -> dterm e2 -> dterm (dsubst_ee Z e2 e1).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* dterm_abs as y. rewrite* dsubst_ee_open_ee_var.
  apply_fresh* dterm_absann as y. rewrite* dsubst_ee_open_ee_var.
Qed.

Hint Resolve dsubst_tt_type dsubst_ee_term.

(* ********************************************************************** *)
(** ** Properties of term substitution in stack *)

Lemma dsubst_stack_fresh : forall Z U T,
  Z \notin dfv_stack T -> dsubst_stack Z U T = T.
Proof.
  induction T; simpl; intros; f_equal*.
  apply* dsubst_tt_fresh.
Qed.

(* ********************************************************************** *)
(** * Properties of well-formedness of a type in an environment *)

(** Through type reduction *)

Lemma dtype_open : forall U T2,
  dtype (dtyp_all T2) ->
  dtype U ->
  dtype (dopen_tt T2 U).
Proof.
  introv WA WU. inversions WA. pick_fresh X.
  rewrite* (@dsubst_tt_intro X).
Qed.

(* ********************************************************************** *)
(** * Relations between well-formed environment and types well-formed
  in environments *)

(** If an environment is well-formed, then it does not contain duplicated keys. *)

Lemma dok_from_okt : forall E,
  dokt E -> ok E.
Proof.
  induction 1; auto.
Qed.

Hint Extern 1 (ok _) => apply dok_from_okt.

(** Extraction from a typing assumption in a well-formed environments *)

Lemma dtype_from_env_has_typ : forall x U E,
  dokt E -> binds x U E -> dtype U.
Proof.
  induction E using env_ind; intros Ok B.
  false* binds_empty_inv.
  inversions Ok.
    false (empty_push_inv H0).
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       auto.
       apply* IHE.
Qed.

(** Extraction from a well-formed environment *)

Lemma dtype_from_okt_typ : forall x T E,
  dokt (E & x ~ T) -> dtype T.
Proof.
  intros. inversions* H.
  false (empty_push_inv H1).
  destruct (eq_push_inv H0) as [? [? ?]]. inversion~ H4. subst~.
Qed.

(** Automation *)

Hint Resolve dtype_from_okt_typ.
Hint Immediate dtype_from_env_has_typ.

(* ********************************************************************** *)
(** ** Properties of well-formedness of type (dwft) *)

Lemma dwft_dtype: forall E t,
    dwft E t ->
    dtype t.
Proof.
  introv wf. inductions wf; simpls~.
  apply_fresh dtype_all as x.
  apply* H0.
Qed.

Lemma din_open_tt_rec: forall x T k U,
    x \in dfv_tt T ->
    x \in dfv_tt (dopen_tt_rec k U T).
Proof.
  intros. gen x k U. induction T; simpls~; intros.
  rewrite in_empty in H. false~.
  rewrite in_union in *.
  destruct H.
  left. apply* IHT1.
  right. apply* IHT2.
  rewrite in_union in *.
  destruct H.
  left. apply* IHT1.
  right. apply* IHT2.
Qed.

Lemma din_open_tt_var: forall x T y,
    x \in dfv_tt T ->
    x \in dfv_tt (T dopen_tt_var y).
Proof.
  intros. unfolds dopen_tt. apply* din_open_tt_rec.
Qed.

Lemma dwft_subset_ftv: forall E t,
    dwft E t ->
    dfv_tt t \c E.
Proof.
  introv wf. inductions wf; simpls~.
  apply subset_empty_l.
  unfold subset. introv inx. rewrite in_singleton in inx. subst~.
  lets: subset_union_2 IHwf1 IHwf2.
  rewrite union_same in H; auto.
  unfold subset. introv inx. pick_fresh y.
  forwards ~ : H0 y. clear H0.
  forwards ~ : din_open_tt_var y inx.
  unfolds subset. apply H1 in H0.
  rewrite in_union in H0. destruct H0; auto.
  rewrite in_singleton in H0. assert( x<> y). auto.
  false (H2 H0).
  lets: subset_union_2 IHwf1 IHwf2.
  rewrite union_same in H; auto.
Qed.

Lemma dwft_no_ftv: forall t x,
    dwft \{} t ->
    x \notin dfv_tt t.
Proof.
  introv wf.
  apply dwft_subset_ftv in wf.
  unfolds subset. unfold notin.
  introv nin.
  apply wf in nin.
  rewrite in_empty in nin. false~.
Qed.

Lemma dwft_subst: forall t x y,
    dwft \{} t ->
    dsubst_tt x y t = t.
Proof.
  introv wf.
  apply dsubst_tt_fresh.
  apply* dwft_no_ftv.
Qed.

(* ********************************************************************** *)
(** ** Properties of well-formedness of an environment *)

(** Inversion lemma *)

Lemma dokt_push_typ_inv : forall E x T,
  dokt (E & x ~ T) -> dokt E /\ dtype T /\ x # E.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&M&?): (eq_push_inv H). subst. auto.
Qed.

Lemma dokt_push_typ_type : forall E X T,
  dokt (E & X ~ T) -> dtype T.
Proof. intros. applys dokt_push_typ_inv. exact H.
Qed.

Hint Immediate dokt_push_typ_type.

(** If an environment is well-formed, then its left part is also well-formed. *)

Lemma dokt_concat_left_inv: forall E F,
    dokt (E & F) -> dokt E.
Proof.
  induction F using env_ind; introv okt; auto.
  clean_empty okt; auto.
  rewrite concat_assoc in okt.
  lets [? ?]: dokt_push_typ_inv okt. apply* IHF.
Qed.

(** Through strengthening *)

Lemma dokt_strengthen : forall x T (E F:denv),
  dokt (E & x ~ T & F) ->
  dokt (E & F).
Proof.
 introv O. induction F using env_ind.
  rewrite concat_empty_r in *. lets*: (dokt_push_typ_inv O).
  rewrite concat_assoc in *.
     lets (?&?&?): (dokt_push_typ_inv O).
      applys~ dokt_typ.
Qed.

(** Through type substitution *)

Lemma dokt_subst_tt : forall Z P (E F:denv),
  dokt (E & F) ->
  dtype P ->
  dokt (E & map (dsubst_tt Z P) F).
Proof.
 introv O W. induction F using env_ind.
  rewrite map_empty. rewrite concat_empty_r in *.
    auto.
  rewrite map_push. rewrite concat_assoc in *.
     lets (?&?&?): (dokt_push_typ_inv O).
      applys~ dokt_typ.
Qed.

Lemma dokt_subst_tt_empty : forall Z P (F:denv),
  dokt F ->
  dtype P ->
  dokt (map (dsubst_tt Z P) F).
Proof.
 introv O W. assert (dokt (empty & F)) by rewrite~ concat_empty_l.
 forwards ~ : dokt_subst_tt Z P H.
 rewrite concat_empty_l in H0. auto.
Qed.

(** Automation *)

Hint Resolve dokt_subst_tt.
Hint Immediate dokt_strengthen.

(* ********************************************************************** *)
(** ** Environment is unchanged by substitution from a fresh name *)

Lemma dnotin_fv_tt_open : forall Y X T,
  X \notin dfv_tt (T dopen_tt_var Y) ->
  X \notin dfv_tt T.
Proof.
 introv. unfold dopen_tt. generalize 0.
 induction T; simpl; intros k Fr; auto.
 specializes IHT1 k. specializes IHT2 k. auto.
 specializes IHT (S k). auto.
 specializes IHT1 k. specializes IHT2 k. auto.
Qed.

Lemma dnotin_fv_tt_dopen : forall Y X T,
  X \notin dfv_tt (dopen_tt T Y) ->
  X \notin dfv_tt T.
Proof.
 introv. unfold dopen_tt. generalize 0.
 induction T; simpl; intros k Fr; auto.
 specializes IHT1 k. specializes IHT2 k. auto.
 specializes IHT (S k). auto.
 specializes IHT1 k. specializes IHT2 k. auto.
Qed.

Lemma dnotin_fv_tt_open_inv : forall X T U k,
  X \notin dfv_tt T ->
  X \notin dfv_tt U ->
  X \notin dfv_tt (dopen_tt_rec k U T).
Proof.
  introv notint notinu. gen k.
  inductions T; introv; simpls~.
  case_nat*.
Qed.

Lemma dnotin_fv_tt_env_bind_inv: forall E x T y,
    binds y T E ->
    x \notin dfv_tt_env E ->
    x \notin dfv_tt T.
Proof.
  induction E using env_ind; introv bd notin.
  false* binds_empty_inv.
  apply binds_push_inv in bd.
  destruct bd as [[v1 v2]| [v3 v4]].
  subst. rewrite dfv_tt_env_push_inv in notin. autos*.
  rewrite dfv_tt_env_push_inv in notin.
  apply notin_union in notin.
  destruct notin.
  apply* IHE.
Qed.

Lemma dmap_subst_tt_id : forall G Z P,
  Z \notin dfv_tt_env G -> G = map (dsubst_tt Z P) G.
Proof.
  induction G using env_ind; introv notin.
  rewrite* map_empty.
  rewrite map_push.
  rewrite* dsubst_tt_fresh.
  f_equal~.
  apply* IHG.
  rewrite dfv_tt_env_push_inv in notin. auto.
  rewrite dfv_tt_env_push_inv in notin. auto.
Qed.

(* ********************************************************************** *)
(** ** Regularity of relations *)

(** The subtyping relation is restricted to well-formed objects. *)

Lemma dopen_ee_rec_term_commu : forall e j v u i, i <> j ->
  dterm v -> dterm u ->
  dopen_ee_rec j v (dopen_ee_rec i u e) = dopen_ee_rec i u (dopen_ee_rec j v e).
Proof.
  induction e; introv Neq tv tu; simpl in *; try solve [f_equal*].
  case_nat*. case_nat*. simpls. case_nat*.
  symmetry. apply~ dopen_ee_rec_term.
  case_nat*.
  simpls. case_nat*.
  apply~ dopen_ee_rec_term.
  simpls. case_nat*. case_nat*.
Qed.

Lemma dopen_tt_rec_type_commu : forall e j v u i, i <> j ->
  dtype v -> dtype u ->
  dopen_tt_rec j v (dopen_tt_rec i u e) = dopen_tt_rec i u (dopen_tt_rec j v e).
Proof.
  induction e; introv Neq tv tu; simpl in *; auto; try solve [f_equal*].
  case_nat*. case_nat*. simpls. case_nat*.
  symmetry. apply~ dopen_tt_rec_type.
  case_nat*.
  simpls. case_nat*.
  apply~ dopen_tt_rec_type.
  simpls. case_nat*. case_nat*.
Qed.

Lemma dtype_open_inv: forall T U P k,
   dtype U ->
   dtype P ->
   dtype (dopen_tt_rec k U T) ->
   dtype (dopen_tt_rec k P T).
Proof.
  introv wtu wtp wtsu.
  gen_eq M:(dopen_tt_rec k U T) .
  gen U P k T.
  induction wtsu; introv wtu wtp minfo.

  destruct T; simpls~; try(solve[inversion minfo]).
  case_nat*.

  destruct T; simpls~; try(solve[inversion minfo]).
  case_nat*.

  destruct T; simpls~; try(solve[inversion minfo]).
  case_nat*.
  inversion minfo; subst. clear minfo. constructor.
  apply IHwtsu1 with U; auto.
  apply IHwtsu2 with U; auto.

  destruct T; simpls~; try(solve[inversion minfo]).
  case_nat *.

  inversion minfo; subst.
  apply_fresh dtype_all as Y.
  specializes H0 Y.
  unfold dopen_tt.
  rewrite~ dopen_tt_rec_type_commu.
  apply H0 with U; auto.
  rewrite~ dopen_tt_rec_type_commu.

  destruct T; simpls~; try(solve[inversion minfo]).
  case_nat*.
  inversion minfo; subst. clear minfo. constructor.
  apply IHwtsu1 with U; auto.
  apply IHwtsu2 with U; auto.

Qed.

Lemma dsub_regular : forall Q S T,
  dsub Q S T -> dtype S /\ dtype T /\ daenv_type Q.
Proof.
  induction 1. autos*. autos*. jauto_set; auto.
  split~.
   apply_fresh dtype_all as Y.
   destruct IHdsub as [? ?].
   unfold dopen_tt.
   apply dtype_open_inv with U; auto.
   destruct IHdsub as [? ?]. auto.

  split.
    pick_fresh y. specializes H0 y. destructs~ H0.
    split~.
    apply_fresh dtype_all as Y.  specializes H0 Y. destructs~ H0.

  autos*.

  split.
   apply_fresh dtype_all as Y.
   destruct IHdsub as [? ?].
   unfold dopen_tt.
   apply dtype_open_inv with U; auto.
   split~. destruct IHdsub as [? [? ?]] . auto.
   destruct IHdsub as [? [? ?]] . auto.

  jauto_set; auto.
  apply~ daenv_type_stack.
  apply~ dstack_type_cons.
  inversions~ H6.

  autos*.
Qed.

(** The generalization relation is restricted to well-formed objects. *)

Lemma dgen_regular : forall E T S,
  dgen E S T -> dokt E /\ dtype S /\ dtype T.
Proof.
  induction 1.
  auto.
  destruct IHdgen as [? [? ?]].
  split; auto.
  split; auto.
  apply* dtype_open.
Qed.

(** The typing relation is restricted to well-formed objects. *)

Lemma dtyping_regular : forall E e T Q,
  dtyping E Q e T -> dokt E /\ dterm e /\ dtype T /\ dstack_type Q.
Proof.
  induction 1.
  splits~. lets[? [? ?]]: dsub_regular H1. auto.
           lets[? [? ?]]: dsub_regular H1. inversion~ H4.
  splits~.
  splits~.
   pick_fresh y. specializes H1 y. destructs~ H1.
    forwards*: dokt_push_typ_inv.
   apply_fresh dterm_absann as y.
     pick_fresh y. specializes H1 y. destructs~ H1.
      forwards*: dokt_push_typ_inv.
     specializes H1 y. destructs~ H1.
   pick_fresh y. specializes H1 y. destructs~ H1.
    apply* dtype_arrow.
  splits.
   pick_fresh y. specializes H2 y. destructs~ H2.
    forwards*: dokt_push_typ_inv.
   apply_fresh* dterm_abs as y.
     forwards~ K: (H2 y). destructs K. auto.
   pick_fresh y. specializes H2 y. destructs~ H2.
   apply~ dstack_type_empty.

  destruct (dsub_regular H0) as [? ?].
  splits~.
   pick_fresh y. specializes H2 y. destructs~ H2. apply* dokt_concat_left_inv.
   apply_fresh* dterm_absann as y.
     specializes H2 y. destructs~ H2.
   pick_fresh y. specializes H2 y. destructs~ H2.
   apply~ dstack_type_cons.
     pick_fresh y. specializes H2 y. destructs~ H2.

  splits~.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: dokt_push_typ_inv.
   apply_fresh dterm_abs as y.
     forwards~ K: (H0 y). destructs K. auto.
   pick_fresh y. specializes H0 y. destructs~ H0.
    apply* dtype_arrow.
   pick_fresh y. specializes H0 y. destructs~ H0.
    apply~ dstack_type_cons.
    forwards*: dokt_push_typ_inv.

   destruct IHdtyping1 as [? [? [? ?]]].
   destruct IHdtyping2 as [? [? [? ?]]]. splits~.
   inversion~ H8.
   inversion~ H9.

   destruct IHdtyping1 as [? [? [? ?]]].
   destruct IHdtyping2 as [? [? [? ?]]]. splits~.

   lets [? [? ?]]: dsub_regular H0.
   inversions~ H3.

   lets [? [? ?]]: dsub_regular H0.
   inversions~ H3.
Qed.

Hint Constructors dwterm.
Lemma dtyping_regular_dwterm : forall E e T Q,
  dtyping E Q e T -> dwterm e.
Proof.
  induction 1; simpls~.
  apply_fresh dwterm_absann as x; simpls~.
  apply_fresh dwterm_abs as x; simpls~.
  apply_fresh dwterm_absann as x; simpls~.
  apply_fresh dwterm_abs as x; simpls~.
Qed.

(** Automation *)

Hint Extern 1 (dokt ?E) =>
  match goal with
  | H: dtyping _ _ _ |- _ => apply (proj41 (dtyping_regular H))
  | H: dgen _ _ _ |- _ => apply (proj31 (dgen_regular H))
  end.

Hint Extern 1 (dtype ?E ?T) =>
  match goal with
  | H: dtyping E _ T |- _ => apply (proj43 (dtyping_regular H))
  | H: dsub E T _ |- _ => apply (proj31 (dsub_regular H))
  | H: dsub E _ T |- _ => apply (proj32 (dsub_regular H))
  | H : dgen _ T _ |- _ => apply (proj32 (dgen_regular H))
  | H : dgen _ _ T |- _ => apply (proj33 (dgen_regular H))
  end.

Hint Extern 1 (dterm ?e) =>
  match goal with
  | H: dtyping _ ?e _ |- _ => apply (proj42 (dtyping_regular H))
  end.

(* *)

(** Open_var with fresh names is an injective operation *)

Lemma dopen_ee_rec_inj : forall x t1 t2 k,
  x \notin (dfv_ee t1) -> x \notin (dfv_ee t2) ->
  (dopen_ee_rec k (dtrm_fvar x) t1 = dopen_ee_rec k (dtrm_fvar x) t2) -> (t1 = t2).
Proof.
  intros x t1.
  induction t1; intros t2 k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal*
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma dopen_tt_rec_inj : forall x t1 t2 k,
  x \notin (dfv_tt t1) -> x \notin (dfv_tt t2) ->
  (dopen_tt_rec k (dtyp_fvar x) t1 = dopen_tt_rec k (dtyp_fvar x) t2) -> (t1 = t2).
Proof.
  intros x t1.
  induction t1; intros t2 k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal*
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Hint Resolve dopen_ee_rec_inj dopen_tt_rec_inj.

(** More properties about free variables *)

Lemma dnotin_fv_tt_subst_inv: forall V z x U,
    z \notin dfv_tt U ->
    z \notin dfv_tt V ->
    z <> x ->
    z \notin dfv_tt (dsubst_tt x U V).
Proof.
  induction V; introv notinu notinv neq; simpls~.
  case_var~.
Qed.

Hint Resolve dnotin_fv_tt_subst_inv.
Hint Resolve dnotin_fv_tt_open_inv.

Lemma dnotin_fv_ee_open_ee_inv: forall V z U k,
    z \notin dfv_ee V ->
    z \notin dfv_ee U ->
    z \notin dfv_ee (dopen_ee_rec k U V).
Proof.
  induction V; introv notinv notinu; simpls~.
  case_nat~.
Qed.

Lemma din_open_tt_inv: forall y t,
    y \in dfv_tt (t dopen_tt_var y) ->
          y \in dfv_tt t \/ (forall x, x \in dfv_tt (t dopen_tt_var x)).
Proof.
  unfolds dopen_tt. generalize 0. introv. gen n.
  induction t; intros; simpls~.
  case_nat~. right. intros. simpls~. apply in_singleton_self.

  rewrite in_union in H. destruct H.
  forwards * : IHt1.
  destruct H0. left. rewrite in_union. left~.
  right. intros. rewrite in_union. left~.
  forwards * : IHt2.
  destruct H0. left. rewrite in_union. right~.
  right. intros. rewrite in_union. right~.

  rewrite in_union in H. destruct H.
  forwards * : IHt1.
  destruct H0. left. rewrite in_union. left~.
  right. intros. rewrite in_union. left~.
  forwards * : IHt2.
  destruct H0. left. rewrite in_union. right~.
  right. intros. rewrite in_union. right~.
Qed.

Lemma din_subst_tt_inv: forall x y U t,
    x \in dfv_tt (dsubst_tt y U t) ->
          (x \in dfv_tt t /\ x <> y) \/
          (x \in dfv_tt U /\ y \in dfv_tt t).
Proof.
  introv xin. induction t; simpls~.
  rewrite in_empty in xin. false~.
  rewrite in_empty in xin. false~.
  case_var~. right. splits~. apply in_singleton_self.
  simpls. left. splits~. rewrite in_singleton in xin. subst~.

  rewrite in_union in xin. destruct xin.
  forwards ~ : IHt1. destruct H0 as [[? ?] | [? ?]].
  left. splits~. rewrite in_union. left~.
  right. splits~. rewrite in_union. left~.
  forwards ~ : IHt2. destruct H0 as [[? ?] | [? ?]].
  left. splits~. rewrite in_union. right~.
  right. splits~. rewrite in_union. right~.
  rewrite in_union in xin. destruct xin.
  forwards ~ : IHt1. destruct H0 as [[? ?] | [? ?]].
  left. splits~. rewrite in_union. left~.
  right. splits~. rewrite in_union. left~.
  forwards ~ : IHt2. destruct H0 as [[? ?] | [? ?]].
  left. splits~. rewrite in_union. right~.
  right. splits~. rewrite in_union. right~.
Qed.

Lemma dnotin_dfv_tt_inv: forall x y U t,
    x \notin dfv_tt (dsubst_tt y U t) ->
    (x \notin dfv_tt t /\ (x \notin dfv_tt U \/ y \notin dfv_tt t)) \/
    (x \in dfv_tt t /\ x = y /\ x \notin dfv_tt U).
Proof.
  introv xin. induction t; simpls~.
  case_var~.
  tests: (x = y).
  right. splits~. apply in_singleton_self.
  left. splits~.

  rewrite notin_union in xin.
  destruct xin.
  forwards ~ : IHt1. clear IHt1.
  forwards ~ : IHt2. clear IHt2.
  destruct H1 as [ [? [? | ?]] | [? [? ?]]].
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  subst~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.

  rewrite notin_union in xin.
  destruct xin.
  forwards ~ : IHt1. clear IHt1.
  forwards ~ : IHt2. clear IHt2.
  destruct H1 as [ [? [? | ?]] | [? [? ?]]].
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  subst~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.
Qed.

Lemma dnotin_dfv_tt_env_inv: forall x y U E,
    x \notin dfv_tt_env (map (dsubst_tt y U) E) ->
    (x \notin dfv_tt_env E /\ (x \notin dfv_tt U \/ y \notin dfv_tt_env E)) \/
    (x \in dfv_tt_env E /\ x = y /\ x \notin dfv_tt U).
Proof.
  introv xin.
  induction E using env_ind; simpls~.

  rewrite dfv_tt_env_empty.
  left. splits~.

  rewrite map_push in xin.
  rewrite dfv_tt_env_push_inv in xin.
  rewrite notin_union in xin. destruct xin.
  forwards ~ : IHE. clear IHE.
  rewrite dfv_tt_env_push_inv.
  apply dnotin_dfv_tt_inv in H.
  destruct H1 as [[? [? | ?]] | [? [? ?]]].
  destruct H as [[? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~. right. splits~. rewrite in_union. left~.
  destruct H as [[? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~. right. splits~. rewrite in_union. left~.
  destruct H as [[? [? | ?]] | [? [? ?]]]; subst~.
  right. splits~. rewrite in_union. right~.
  right. splits~. rewrite in_union. right~.
  right. splits~. rewrite in_union. right~.
Qed.

Lemma dnotin_fv_subst_tt_env_inv: forall z x U E,
    z \notin dfv_tt_env E ->
    z \notin dfv_tt U ->
    z <> x ->
    z \notin dfv_tt_env (map (dsubst_tt x U) E).
Proof.
  induction E using env_ind; introv notinu notinv neq; simpls~.
  rewrite map_empty. rewrite dfv_tt_env_empty. apply notin_empty.
  rewrite map_push. rewrite dfv_tt_env_push_inv.
  rewrite dfv_tt_env_push_inv in notinu.
  rewrite notin_union in notinu.
  destruct notinu.
  rewrite notin_union. split~.
Qed.


(* ********************************************************************** *)
(** * Properties of Closeness *)

(** Abstracting a term name out of a term *)

Fixpoint dclose_ee_rec (k : nat) (z : var) (t : dtrm) {struct t} : dtrm :=
  match t with
  | dtrm_bvar i        => dtrm_bvar i
  | dtrm_fvar x        => If x = z then (dtrm_bvar k) else (dtrm_fvar x)
  | dtrm_nat  i        => dtrm_nat i
  | dtrm_abs  t2       => dtrm_abs (dclose_ee_rec (S k) z t2)
  | dtrm_absann t1 t2  => dtrm_absann t1 (dclose_ee_rec (S k) z t2)
  | dtrm_app t1 t2     => dtrm_app (dclose_ee_rec k z t1) (dclose_ee_rec k z t2)
  | dtrm_pair t1 t2    => dtrm_pair (dclose_ee_rec k z t1) (dclose_ee_rec k z t2)
  | dtrm_fst           => dtrm_fst
  | dtrm_snd           => dtrm_snd
  end.

Definition dclose_ee z t := dclose_ee_rec 0 z t.

(** Close var commutes with open with some freshness conditions,
  this is used in the proofs of [close_ee_open] *)

Lemma dclose_ee_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (dfv_ee t1) ->
    (dopen_ee_rec i (dtrm_fvar y) (dopen_ee_rec j (dtrm_fvar z) (dclose_ee_rec j x t1)))
  = (dopen_ee_rec j (dtrm_fvar z) (dclose_ee_rec j x (dopen_ee_rec i  (dtrm_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ].
  case_var*. simpl. case_nat*.
Qed.

(** Close var removes fresh var *)

Lemma dclose_ee_fresh : forall x t,
  x \notin dfv_ee (dclose_ee x t).
Proof.
  introv. unfold dclose_ee. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

(** Close var is the right inverse of open_var *)

Lemma dclose_ee_open : forall x t,
  dterm t -> t = (dclose_ee x t) dopen_ee_var x.
Proof.
  introv W. unfold dclose_ee, dopen_ee. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u dfv_ee t)) as [y Fr] end.
  apply* (@dopen_ee_rec_inj y).
  unfolds dopen_ee. rewrite* dclose_ee_rec_open.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u dfv_ee t)) as [y Fr] end.
  apply* (@dopen_ee_rec_inj y).
  unfolds dopen_ee. rewrite* dclose_ee_rec_open.
Qed.

(** Abstracting a type name out of a type *)

Fixpoint dclose_tt_rec (k : nat) (z : var) (t : dtyp) {struct t} : dtyp :=
  match t with
  | dtyp_bvar i        => dtyp_bvar i
  | dtyp_fvar x        => If x = z then (dtyp_bvar k) else (dtyp_fvar x)
  | dtyp_nat           => dtyp_nat
  | dtyp_arrow t1 t2   => dtyp_arrow (dclose_tt_rec k z t1) (dclose_tt_rec k z t2)
  | dtyp_all t1        => dtyp_all (dclose_tt_rec (S k) z t1)
  | dtyp_pair t1 t2    => dtyp_pair (dclose_tt_rec k z t1) (dclose_tt_rec k z t2)
  end.

(** Abstracting a type name out of a type *)

Fixpoint dclose_te_rec (k : nat) (z : var) (t : dtrm) {struct t} : dtrm :=
  match t with
  | dtrm_bvar i        => dtrm_bvar i
  | dtrm_fvar x        => dtrm_fvar x
  | dtrm_nat  i        => dtrm_nat i
  | dtrm_abs  t2       => dtrm_abs (dclose_te_rec k z t2)
  | dtrm_absann t1 t2  => dtrm_absann (dclose_tt_rec k z t1) (dclose_te_rec k z t2)
  | dtrm_app t1 t2     => dtrm_app (dclose_te_rec k z t1) (dclose_te_rec k z t2)
  | dtrm_pair t1 t2    => dtrm_pair (dclose_te_rec k z t1) (dclose_te_rec k z t2)
  | dtrm_fst           => dtrm_fst
  | dtrm_snd           => dtrm_snd
  end.

Definition dclose_tt z t := dclose_tt_rec 0 z t.
Definition dclose_te z t := dclose_te_rec 0 z t.

(** Close var commutes with open with some freshness conditions *)

Lemma dclose_tt_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (dfv_tt t1) ->
    (dopen_tt_rec i (dtyp_fvar y) (dopen_tt_rec j (dtyp_fvar z) (dclose_tt_rec j x t1)))
  = (dopen_tt_rec j (dtyp_fvar z) (dclose_tt_rec j x (dopen_tt_rec i  (dtyp_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ].
  case_var*. simpl. case_nat*.
Qed.

(** Close var removes fresh var *)

Lemma dclose_tt_fresh : forall x t k,
  x \notin dfv_tt (dclose_tt_rec k x t).
Proof.
  introv. unfold dclose_tt. gen k.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Hint Resolve dclose_tt_fresh.

(** Close var is the right inverse of open_var *)

Lemma dclose_tt_open : forall x t k,
  dtype t -> t = dopen_tt_rec k (dtyp_fvar x) (dclose_tt_rec k x t).
Proof.
  introv W. unfold dclose_tt, dopen_tt. gen k.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u dfv_tt t)) as [y Fr] end.
  apply* (@dopen_tt_rec_inj y).
  unfolds dopen_tt. rewrite* dclose_tt_rec_open.
Qed.

Lemma dclose_tt_open_var : forall x t,
  dtype t -> t = dopen_tt (dclose_tt x t) (dtyp_fvar x).
Proof.
  intros. unfolds dopen_tt.
  apply* dclose_tt_open.
Qed.

Hint Resolve dclose_tt_rec_open.
Hint Resolve dclose_tt_open.

(* close does not introduce new free vairables *)

Lemma dnotin_fv_tt_close_inv: forall S x y k,
    x \notin dfv_tt S ->
    x \notin dfv_tt (dclose_tt_rec k y S).
Proof.
  induction S; introv notin; simpls~.
  case_var~. simpls~.
Qed.

Lemma dnotin_fv_ee_close_te_inv: forall S x y k,
    x \notin dfv_ee S ->
    x \notin dfv_ee (dclose_te_rec k y S).
Proof.
  induction S; introv notin; simpls~.
Qed.

Lemma dclose_tt_in_inv : forall x t k y,
        x <> y ->
        x \in dfv_tt (dclose_tt_rec k y t) ->
        x \in dfv_tt t.
Proof.
  introv. unfold dclose_tt. gen k.
  induction t; intros; simpls; notin_simpl; auto.
  case_var; simple*.
  simpls. rewrite in_empty in H0. false~.
  rewrite in_union in *. destruct H0. left. apply* IHt1. right. apply* IHt2.
  apply* IHt.
  rewrite in_union in *. destruct H0. left. apply* IHt1. right. apply* IHt2.
Qed.

Hint Resolve dnotin_fv_tt_close_inv
     dnotin_fv_ee_close_te_inv
.

(** open a closed one is substitution *)

Lemma dclose_tt_open_subst: forall S k x y,
    dtype S ->
    dopen_tt_rec k (dtyp_fvar y) (dclose_tt_rec k x S) = dsubst_tt x (dtyp_fvar y) S.
Proof.
  introv ty. gen k. induction ty; introv; simpls; auto.
  case_var~. simpls. case_nat~.
  rewrite* IHty1. rewrite* IHty2.

  pick_fresh z.
  specializes H0 z (S k).

  rewrite dsubst_tt_open_tt in H0; auto.
  simpls. case_var.

  unfolds dopen_tt.
  unfolds dopen_tt.
  rewrite <- dclose_tt_rec_open in H0; auto.
  apply dopen_tt_rec_inj in H0; auto.
  rewrite~ H0.
  apply dnotin_fv_tt_open_inv; simpls~.
  apply dnotin_fv_tt_subst_inv; simpls~.

  rewrite* IHty1. rewrite* IHty2.
Qed.

Hint Resolve dclose_tt_open_subst.

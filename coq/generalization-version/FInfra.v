Set Implicit Arguments.
Require Import LibLN DeclDef FDef.
Require Import DeclInfra.

(* ********************************************************************** *)
(** * Additional Definitions Used in the Proofs *)

(** Computing free type variables in a type *)

Definition ffv_tt := dfv_tt.

(** Computing free type variables in a term *)

Fixpoint ffv_te (e : ftrm) {struct e} : vars :=
  match e with
  | ftrm_bvar i       => \{}
  | ftrm_fvar x       => \{}
  | ftrm_nat  i       => \{}
  | ftrm_absann V e1  => (ffv_tt V) \u (ffv_te e1)
  | ftrm_app e1 e2    => (ffv_te e1) \u (ffv_te e2)
  | ftrm_tabs e1      => (ffv_te e1)
  | ftrm_tapp e1 V    => (ffv_tt V) \u (ffv_te e1)
  | ftrm_pair e1 e2   => (ffv_te e1) \u (ffv_te e2)
  | ftrm_fst e1       => (ffv_te e1)
  | ftrm_snd e1       => (ffv_te e1)
  end.

(** Computing free term variables in a term *)

Fixpoint ffv_ee (e : ftrm) {struct e} : vars :=
  match e with
  | ftrm_bvar i       => \{}
  | ftrm_fvar x       => \{x}
  | ftrm_nat i        => \{}
  | ftrm_absann V e1  => (ffv_ee e1)
  | ftrm_app e1 e2    => (ffv_ee e1) \u (ffv_ee e2)
  | ftrm_tabs e1      => (ffv_ee e1)
  | ftrm_tapp e1 V    => (ffv_ee e1)
  | ftrm_pair e1 e2   => (ffv_ee e1) \u (ffv_ee e2)
  | ftrm_fst e1       => (ffv_ee e1)
  | ftrm_snd e1       => (ffv_ee e1)
  end.

(** Substitution for free type variables in types. *)

Definition fsubst_tt := dsubst_tt.

(** Substitution for free type variables in terms. *)

Fixpoint fsubst_te (Z : var) (U : ftyp) (e : ftrm) {struct e} : ftrm :=
  match e with
  | ftrm_bvar i       => ftrm_bvar i
  | ftrm_fvar x       => ftrm_fvar x
  | ftrm_nat i        => ftrm_nat i
  | ftrm_absann V e1  => ftrm_absann  (fsubst_tt Z U V)  (fsubst_te Z U e1)
  | ftrm_app e1 e2    => ftrm_app  (fsubst_te Z U e1) (fsubst_te Z U e2)
  | ftrm_tabs e1      => ftrm_tabs (fsubst_te Z U e1)
  | ftrm_tapp e1 V    => ftrm_tapp (fsubst_te Z U e1) (fsubst_tt Z U V)
  | ftrm_pair e1 e2   => ftrm_pair  (fsubst_te Z U e1) (fsubst_te Z U e2)
  | ftrm_fst e1       => ftrm_fst  (fsubst_te Z U e1)
  | ftrm_snd e1       => ftrm_snd  (fsubst_te Z U e1)
  end.

(** Substitution for free term variables in terms. *)

Fixpoint fsubst_ee (z : var) (u : ftrm) (e : ftrm) {struct e} : ftrm :=
  match e with
  | ftrm_bvar i       => ftrm_bvar i
  | ftrm_fvar x       => If x = z then u else (ftrm_fvar x)
  | ftrm_nat i        => ftrm_nat i
  | ftrm_absann V e1  => ftrm_absann V (fsubst_ee z u e1)
  | ftrm_app e1 e2    => ftrm_app  (fsubst_ee z u e1) (fsubst_ee z u e2)
  | ftrm_tabs e1      => ftrm_tabs (fsubst_ee z u e1)
  | ftrm_tapp e1 V    => ftrm_tapp (fsubst_ee z u e1) V
  | ftrm_pair e1 e2   => ftrm_pair (fsubst_ee z u e1) (fsubst_ee z u e2)
  | ftrm_fst e1       => ftrm_fst (fsubst_ee z u e1)
  | ftrm_snd e1       => ftrm_snd (fsubst_ee z u e1)
  end.

(* ********************************************************************** *)
(** * Tactics *)

(** Constructors as hints. *)

Hint Constructors dtype fterm ok dokt.
Hint Unfold ftyp ftyp_nat ftyp_bvar ftyp_fvar ftyp_arrow ftyp_all ftyp_arrow ftype .
Hint Unfold ftype ftype_nat ftype_var ftype_arrow ftype_all ftype_arrow.

Hint Resolve ftyping_var ftyping_nat ftyping_absann ftyping_tabs ftyping_app ftyping_tapp
     ftyping_pair ftyping_fst ftyping_snd
.

(** Gathering free names already used in the proofs *)

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : ftrm => ffv_te x) in
  let D := gather_vars_with (fun x : ftrm => ffv_ee x) in
  let E := gather_vars_with (fun x : ftyp => ffv_tt x) in
  let F := gather_vars_with (fun x : fenv => dom x) in
  let G := gather_vars_with (fun x : fenv => dfv_tt_env x) in
  let H := gather_vars_with (fun x : denv => dom x) in
  let I := gather_vars_with (fun x : denv => dfv_tt_env x) in
  constr:(A \u B \u C \u D \u E \u F \u G \u H \u I).

(** "pick_fresh x" tactic create a fresh variable with name x *)

Ltac pick_fresh x :=
  let L := gather_vars in (pick_fresh_gen L x).

(** "apply_fresh T as x" is used to apply inductive rule which
   use an universal quantification over a cofinite set *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

(** These tactics help applying a lemma which conclusion mentions
  an environment (E & F) in the particular case when F is empty *)

Ltac fget_env :=
  match goal with
  | |- ftyping ?E _ _ => E
  end.

Tactic Notation "apply_empty_bis" tactic(get_env) constr(lemma) :=
  let E := fget_env in rewrite <- (concat_empty_r E);
  eapply lemma; try rewrite concat_empty_r.

Tactic Notation "apply_empty" constr(F) :=
  apply_empty_bis (fget_env) F.

Tactic Notation "apply_empty" "*" constr(F) :=
  apply_empty F; autos*.

(* ********************************************************************** *)
(** * Properties of Substitutions *)

(* ********************************************************************** *)
(** ** Properties of type substitution in type *)

(** Substitution on indices is identity on well-formed terms. *)

Lemma fopen_tt_rec_type_core : forall T j V U i, i <> j ->
  (fopen_tt_rec j V T) = fopen_tt_rec i U (fopen_tt_rec j V T) ->
  T = fopen_tt_rec i U T.
Proof.
  apply* dopen_tt_rec_type_core.
Qed.

Lemma fopen_tt_rec_type : forall T U,
  ftype T -> forall k, T = fopen_tt_rec k U T.
Proof.
  apply* dopen_tt_rec_type.
Qed.

(** Substitution for a fresh name is identity. *)

Lemma fsubst_tt_fresh : forall Z U T,
  Z \notin ffv_tt T -> fsubst_tt Z U T = T.
Proof.
  apply* dsubst_tt_fresh.
Qed.

(** Substitution for itself if identity. *)

Lemma fsubst_tt_same: forall y f,
    (fsubst_tt y (dtyp_fvar y) f) = f.
Proof.
  inductions f; simpls; autos*; f_equal *.
  case_var*.
Qed.

Hint Resolve fsubst_tt_same.
Lemma fsubst_te_same: forall y f,
    (fsubst_te y (dtyp_fvar y) f) = f.
Proof.
  inductions f; simpls; autos*; f_equal *.
Qed.

Lemma fsubst_ee_same: forall y f,
    (fsubst_ee y (ftrm_fvar y) f) = f.
Proof.
  inductions f; simpls; autos*; f_equal *.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma fsubst_tt_open_tt_rec : forall T1 T2 X P n, ftype P ->
  fsubst_tt X P (fopen_tt_rec n T2 T1) =
  fopen_tt_rec n (fsubst_tt X P T2) (fsubst_tt X P T1).
Proof.
  apply* dsubst_tt_open_tt_rec.
Qed.

Lemma fsubst_tt_open_tt : forall T1 T2 X P, ftype P ->
  fsubst_tt X P (fopen_tt T1 T2) =
  fopen_tt (fsubst_tt X P T1) (fsubst_tt X P T2).
Proof.
  apply* dsubst_tt_open_tt.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma fsubst_tt_open_tt_var : forall X Y U T, Y <> X -> ftype U ->
  (fsubst_tt X U T) fopen_tt_var Y = fsubst_tt X U (T fopen_tt_var Y).
Proof.
  apply* dsubst_tt_open_tt_var.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma fsubst_tt_intro : forall X T2 U,
  X \notin ffv_tt T2 -> ftype U ->
  fopen_tt T2 U = fsubst_tt X U (T2 fopen_tt_var X).
Proof.
  apply* dsubst_tt_intro.
Qed.

(** Open_var with fresh names is an injective operation *)

Lemma fopen_ee_rec_inj : forall x t1 t2 k,
  x \notin (ffv_ee t1) -> x \notin (ffv_ee t2) ->
  (fopen_ee_rec k (ftrm_fvar x) t1 = fopen_ee_rec k (ftrm_fvar x) t2) -> (t1 = t2).
Proof.
  intros x t1.
  induction t1; intros t2 k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal*
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma fopen_tt_rec_inj : forall x t1 t2 k,
  x \notin (ffv_tt t1) -> x \notin (ffv_tt t2) ->
  (fopen_tt_rec k (ftyp_fvar x) t1 = fopen_tt_rec k (ftyp_fvar x) t2) -> (t1 = t2).
Proof.
  intros x t1.
  induction t1; intros t2 k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal*
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Hint Resolve fopen_ee_rec_inj fopen_tt_rec_inj.

Lemma fopen_te_rec_inj : forall x t1 t2 k,
  x \notin (ffv_te t1) -> x \notin (ffv_te t2) ->
  (fopen_te_rec k (ftyp_fvar x) t1 = fopen_te_rec k (ftyp_fvar x) t2) -> (t1 = t2).
Proof.
  intros x t1. unfold fopen_te.
  induction t1; intros t2 k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal*
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

(** open_ee and open_te commute. *)

Lemma fopen_te_ee: forall z u s k1 k2,
    fopen_ee_rec k1 (ftrm_fvar z) (fopen_te_rec k2 (ftyp_fvar u) s) =
    fopen_te_rec k2 (ftyp_fvar u) (fopen_ee_rec k1 (ftrm_fvar z) s).
Proof.
  introv. generalize k1 k2.
  inductions s; introv; simpls; f_equal *; autos*.
  case_nat*.
Qed.


(* ********************************************************************** *)
(** ** Properties of type substitution in terms *)

Lemma fopen_te_rec_term_core : forall e j u i P ,
  fopen_ee_rec j u e = fopen_te_rec i P (fopen_ee_rec j u e) ->
  e = fopen_te_rec i P e.
Proof.
  induction e; intros; simpl in *; inversion H; f_equal*; f_equal*.
Qed.

Lemma fopen_te_rec_type_core : forall e j Q i P, i <> j ->
  fopen_te_rec j Q e = fopen_te_rec i P (fopen_te_rec j Q e) ->
   e = fopen_te_rec i P e.
Proof.
  induction e; intros; simpl in *; inversion H0; f_equal*;
  match goal with H: ?i <> ?j |- ?t = fopen_tt_rec ?i _ ?t =>
   apply* (@fopen_tt_rec_type_core t j) end.
Qed.

Lemma fopen_te_rec_term : forall e U,
  fterm e -> forall k, e = fopen_te_rec k U e.
Proof.
  intros e U WF. induction WF; intros; simpl;
    f_equal*; try solve [ apply* fopen_tt_rec_type ].
  unfolds fopen_ee. pick_fresh x.
   apply* (@fopen_te_rec_term_core e1 0 (ftrm_fvar x)).
  unfolds fopen_te. pick_fresh X.
   apply* (@fopen_te_rec_type_core e1 0 (ftyp_fvar X)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma fsubst_te_fresh : forall X U e,
  X \notin ffv_te e -> fsubst_te X U e = e.
Proof.
  induction e; simpl; intros; f_equal*; autos* fsubst_tt_fresh.
Qed.

(** Substitution distributes on the open operation. *)

Lemma fsubst_te_open_te : forall e T X U, ftype U ->
  fsubst_te X U (fopen_te e T) =
  fopen_te (fsubst_te X U e) (fsubst_tt X U T).
Proof.
  intros. unfold fopen_te. generalize 0.
  induction e; intros; simpls; f_equal*;
  autos* fsubst_tt_open_tt_rec.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma fsubst_te_open_te_var : forall X Y U e, Y <> X -> ftype U ->
  (fsubst_te X U e) fopen_te_var Y = fsubst_te X U (e fopen_te_var Y).
Proof.
  introv Neq Wu. rewrite* fsubst_te_open_te.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma fsubst_te_intro : forall X U e,
  X \notin ffv_te e -> ftype U ->
  fopen_te e U = fsubst_te X U (e fopen_te_var X).
Proof.
  introv Fr Wu. rewrite* fsubst_te_open_te.
  rewrite* fsubst_te_fresh. simpl. case_var*.
Qed.

(* ********************************************************************** *)
(** ** Properties of term substitution in terms *)

Lemma fopen_ee_rec_term_core : forall e j v u i, i <> j ->
  fopen_ee_rec j v e = fopen_ee_rec i u (fopen_ee_rec j v e) ->
  e = fopen_ee_rec i u e.
Proof.
  induction e; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma fopen_ee_rec_type_core : forall e j V u i,
  fopen_te_rec j V e = fopen_ee_rec i u (fopen_te_rec j V e) ->
  e = fopen_ee_rec i u e.
Proof.
  induction e; introv H; simpls; inversion H; f_equal*.
Qed.

Lemma fopen_ee_rec_term : forall u e,
  fterm e -> forall k, e = fopen_ee_rec k u e.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds fopen_ee. pick_fresh x.
   apply* (@fopen_ee_rec_term_core e1 0 (ftrm_fvar x)).
  unfolds fopen_te. pick_fresh X.
   apply* (@fopen_ee_rec_type_core e1 0 (ftyp_fvar X)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma fsubst_ee_fresh : forall x u e,
  x \notin ffv_ee e -> fsubst_ee x u e = e.
Proof.
  induction e; simpl; intros; f_equal*.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma fsubst_ee_open_ee : forall t1 t2 u x, fterm u ->
  fsubst_ee x u (fopen_ee t1 t2) =
  fopen_ee (fsubst_ee x u t1) (fsubst_ee x u t2).
Proof.
  intros. unfold fopen_ee. generalize 0.
  induction t1; intros; simpls; f_equal*.
  case_nat*.
  case_var*. rewrite* <- fopen_ee_rec_term.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma fsubst_ee_open_ee_var : forall x y u e, y <> x -> fterm u ->
  (fsubst_ee x u e) fopen_ee_var y = fsubst_ee x u (e fopen_ee_var y).
Proof.
  introv Neq Wu. rewrite* fsubst_ee_open_ee.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma fsubst_ee_intro : forall x u e,
  x \notin ffv_ee e -> fterm u ->
  fopen_ee e u = fsubst_ee x u (e fopen_ee_var x).
Proof.
  introv Fr Wu. rewrite* fsubst_ee_open_ee.
  rewrite* fsubst_ee_fresh. simpl. case_var*.
Qed.

(** Interactions between type substitutions in terms and opening
  with term variables in terms. *)

Lemma fsubst_te_open_ee_var : forall Z P x e,
  (fsubst_te Z P e) fopen_ee_var x = fsubst_te Z P (e fopen_ee_var x).
Proof.
  introv. unfold fopen_ee. generalize 0.
  induction e; intros; simpl; f_equal*. case_nat*.
Qed.

(** Interactions between term substitutions in terms and opening
  with type variables in terms. *)

Lemma fsubst_ee_open_te_var : forall z u e X, fterm u ->
  (fsubst_ee z u e) fopen_te_var X = fsubst_ee z u (e fopen_te_var X).
Proof.
  introv. unfold fopen_te. generalize 0.
  induction e; intros; simpl; f_equal*.
  case_var*. symmetry. autos* fopen_te_rec_term.
Qed.

(** Substitutions preserve local closure. *)

Lemma fsubst_tt_type : forall T Z P,
  ftype T -> ftype P -> ftype (fsubst_tt Z P T).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh ftype_all as X. rewrite* fsubst_tt_open_tt_var.
Qed.

Lemma fsubst_te_term : forall e Z P,
  fterm e -> ftype P -> fterm (fsubst_te Z P e).
Proof.
  lets: fsubst_tt_type. induction 1; intros; simpl; auto.
  apply_fresh* fterm_absann as x. rewrite* fsubst_te_open_ee_var.
  apply_fresh* fterm_tabs as x. rewrite* fsubst_te_open_te_var.
Qed.

Lemma fsubst_ee_term : forall e1 Z e2,
  fterm e1 -> fterm e2 -> fterm (fsubst_ee Z e2 e1).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* fterm_absann as y. rewrite* fsubst_ee_open_ee_var.
  apply_fresh* fterm_tabs as Y. rewrite* fsubst_ee_open_te_var.
Qed.

Hint Resolve fsubst_tt_type fsubst_te_term fsubst_ee_term.

(* ********************************************************************** *)
(** * Properties of fterm *)

Lemma fterm_open_tabs : forall U T2,
  fterm (ftrm_tabs T2) ->
  ftype U ->
  fterm (fopen_te T2 U).
Proof.
  introv WA WU. inversions WA. pick_fresh X.
  rewrite* (@fsubst_te_intro X).
Qed.

(* ********************************************************************** *)
(** * Properties of well-formedness of a type in an environment *)

(** If a type is well-formed in an environment then it is locally closed. *)

Hint Unfold fokt fokt_empty fokt_typ.

(** Through type reduction *)

Lemma ftype_open_inv: forall T U P k,
   ftype U ->
   ftype P ->
   ftype (fopen_tt_rec k U T) ->
   ftype (fopen_tt_rec k P T).
Proof.
  apply* dtype_open_inv.
Qed.

(* ********************************************************************** *)
(** * Relations between well-formed environment and types well-formed
  in environments *)

(** If an environment is well-formed, then it does not contain duplicated keys. *)

Lemma fok_from_okt : forall E,
  fokt E -> ok E.
Proof.
  induction 1; auto.
Qed.

Hint Extern 1 (ok _) => apply fok_from_okt.

(** Extraction from a typing assumption in a well-formed environments *)

Lemma ftype_from_env_has_typ : forall x U E,
  fokt E -> binds x U E -> dtype U.
Proof.
  apply* dtype_from_env_has_typ.
Qed.

(** Extraction from a well-formed environment *)

Lemma ftype_from_okt_typ : forall x T E,
  fokt (E & x ~ T) -> dtype T.
Proof.
  apply* dtype_from_okt_typ.
Qed.

(** Automation *)

Hint Resolve ftype_from_okt_typ.
Hint Immediate ftype_from_env_has_typ.


(* ********************************************************************** *)
(** ** Properties of well-formedness of an environment *)

(** Inversion lemma *)

Lemma fokt_push_typ_inv : forall E x T,
  fokt (E & x ~ T) -> fokt E /\ ftype T /\ x # E.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&M&?): (eq_push_inv H). subst. auto.
Qed.

Lemma fokt_push_typ_type : forall E X T,
  fokt (E & X ~ T) -> ftype T.
Proof. intros. applys fokt_push_typ_inv. exact H.
Qed.

Hint Immediate fokt_push_typ_type.

(** Through strengthening *)

Lemma fokt_strengthen : forall x T (E F:fenv),
  fokt (E & x ~ T & F) ->
  fokt (E & F).
Proof.
  apply* dokt_strengthen.
Qed.

(** Through type substitution *)

Lemma fokt_subst_tt : forall Z P (E F:fenv),
  fokt (E & F) ->
  dtype P ->
  fokt (E & map (fsubst_tt Z P) F).
Proof.
  apply* dokt_subst_tt.
Qed.

(** Automation *)

Hint Resolve fokt_subst_tt.
Hint Immediate fokt_strengthen.


(* ********************************************************************** *)
(** ** Environment is unchanged by substitution from a fresh name *)

Lemma fnotin_fv_tt_open : forall Y X T,
  X \notin ffv_tt (T fopen_tt_var Y) ->
  X \notin ffv_tt T.
Proof.
  apply* dnotin_fv_tt_open.
Qed.

Lemma fnotin_fv_tt_open_inv : forall X T U k,
  X \notin ffv_tt T ->
  X \notin ffv_tt U ->
  X \notin ffv_tt (fopen_tt_rec k U T).
Proof.
  introv notint notinu. gen k.
  inductions T; introv; simpls~.
  case_nat*.
Qed.

Hint Resolve fnotin_fv_tt_open_inv.
Lemma fnotin_fv_te_open_inv : forall X T U k,
  X \notin ffv_te T ->
  X \notin ffv_tt U ->
  X \notin ffv_te (fopen_te_rec k U T).
Proof.
  introv notint notinu. gen k.
  inductions T; introv; simpls~.
Qed.

Lemma fnotin_fv_ee_open_inv: forall V z U k,
    z \notin ffv_ee V ->
    z \notin ffv_ee (fopen_te_rec k U V).
Proof.
  induction V; introv notinv; simpls~.
Qed.

Hint Resolve fnotin_fv_tt_open_inv
     fnotin_fv_ee_open_inv
     fnotin_fv_te_open_inv.

Lemma fnotin_fv_tt_subst_inv: forall V z x U,
    z \notin ffv_tt U ->
    z \notin ffv_tt V ->
    z <> x ->
    z \notin ffv_tt (fsubst_tt x U V).
Proof.
  induction V; introv notinu notinv neq; simpls~.
  case_var~.
Qed.

Lemma fnotin_fv_ee_subst_inv: forall V z x U,
    z \notin ffv_ee V ->
    z \notin ffv_ee (fsubst_te x U V).
Proof.
  induction V; introv notinv; simpls~.
Qed.

Hint Resolve fnotin_fv_tt_subst_inv.
Lemma fnotin_fv_te_subst_inv: forall V z x U,
    z \notin ffv_te V ->
    z \notin ffv_tt U ->
    z <> x ->
    z \notin ffv_te (fsubst_te x U V).
Proof.
  induction V; introv notinv notinu neq; simpls~.
Qed.

Hint Resolve fnotin_fv_ee_subst_inv fnotin_fv_te_subst_inv.

Lemma fnotin_fv_te_subst_ee_inv: forall V z x U,
    z \notin ffv_te V ->
    z \notin ffv_te U ->
    z \notin ffv_te (fsubst_ee x U V).
Proof.
  induction V; introv notinv; simpls~.
  case_var~.
Qed.

(* ********************************************************************** *)
(** ** Regularity of relations *)

(** The typing relation is restricted to well-formed objects. *)

Lemma ftyping_regular : forall E e T,
  ftyping E e T -> fokt E /\ fterm e /\ ftype T.
Proof.
  induction 1.
  splits*.
  splits~.
  splits~.
   pick_fresh y. specializes H0 y. destructs~ H0.
   apply* dokt_concat_left_inv.
   apply_fresh fterm_absann as y.
     pick_fresh y. specializes H0 y. destructs~ H0.
      forwards*: fokt_push_typ_inv.
     specializes H0 y. destructs~ H0.
   pick_fresh y. specializes H0 y. destructs~ H0.
    apply~ ftype_arrow.
      forwards*: fokt_push_typ_inv.

  splits~.
   pick_fresh y. specializes H0 y. destructs~ H0.
   apply_fresh* fterm_tabs as y.
     forwards~ K: (H0 y). destructs K. auto.
   apply_fresh ftype_all as Y.
     forwards~ K: (H0 Y). destructs K. auto.

  destruct IHftyping1 as [? [? ?]].
  destruct IHftyping2 as [? [? ?]]. inversion~ H6; subst.

  destruct IHftyping as [? [? ?]].
  splits~.
  apply* dtype_open.

  destruct IHftyping1 as [? [? ?]].
  destruct IHftyping2 as [? [? ?]]. splits~.

  destruct IHftyping as [? [? ?]]. inversions~ H2.

  destruct IHftyping as [? [? ?]]. inversions~ H2.
Qed.

Lemma ftyping_subst: forall E s t x y,
    ftyping E s t ->
    ftyping (map (fsubst_tt x (dtyp_fvar y)) E)
            (fsubst_te x (dtyp_fvar y) s)
            (fsubst_tt x (dtyp_fvar y) t).
Proof.
  induction 1; introv; simpls; auto.
  apply ftyping_var.
  unfold fsubst_tt.
  rewrite <- concat_empty_l.
  apply* dokt_subst_tt.
  rewrite~ concat_empty_l.
  apply* binds_map.

  apply ftyping_nat.
  rewrite <- concat_empty_l.
  apply* dokt_subst_tt.
  rewrite~ concat_empty_l.

  apply_fresh ftyping_absann as z.
  simpls.
  specializes H0 z.
  forwards ~ : H0. clear H0.
  rewrite map_push in H1.
  rewrite fsubst_te_open_ee_var.
  assumption.

  apply_fresh ftyping_tabs as z.
  rewrite~ fsubst_te_open_te_var.
  rewrite~ fsubst_tt_open_tt_var.

  apply ftyping_app with (fsubst_tt x (dtyp_fvar y) T1); auto.

  unfold fopen_tt.
  rewrite~ fsubst_tt_open_tt_rec.
  apply~ ftyping_tapp.

  apply* ftyping_fst.

  apply* ftyping_snd.
Qed.
(** Automation *)

Hint Extern 1 (fokt ?E) =>
  match goal with
  | H: ftyping _ _ _ |- _ => apply (proj31 (ftyping_regular H))
  end.

Hint Extern 1 (ftype ?E ?T) =>
  match goal with
  | H: ftyping E _ T |- _ => apply (proj33 (ftyping_regular H))
  end.

Hint Extern 1 (fterm ?e) =>
  match goal with
  | H: ftyping _ ?e _ |- _ => apply (proj32 (ftyping_regular H))
  end.

Lemma ftyping_weaken : forall E F G f t,
    ftyping (E & F) f t ->
    fokt (E & G & F) ->
    ftyping (E & G & F) f t.
Proof.
  introv ty okt.
  gen_eq M: (E & F). gen F.
  induction ty; introv okt minfo; subst~.
  apply* ftyping_var. apply* binds_weaken.

  apply_fresh ftyping_absann as y.
  specializes H0 y (F & y ~ V).
  specializes H y.
  repeat rewrite concat_assoc in H0. apply* H0.
  apply~ fokt_typ.
  forwards * : H.

  apply_fresh ftyping_tabs as y.
  specializes H0 y F.

  apply ftyping_app with T1; auto.

  apply* ftyping_fst.

  apply* ftyping_snd.
Qed.


(* ********************************************************************** *)
(** * Properties of Closeness *)

(** Abstracting a term name out of a term *)

Fixpoint fclose_ee_rec (k : nat) (z : var) (t : ftrm) {struct t} : ftrm :=
  match t with
  | ftrm_bvar i        => ftrm_bvar i
  | ftrm_fvar x        => If x = z then (ftrm_bvar k) else (ftrm_fvar x)
  | ftrm_nat  i        => ftrm_nat i
  | ftrm_absann t1 t2  => ftrm_absann t1 (fclose_ee_rec (S k) z t2)
  | ftrm_app t1 t2     => ftrm_app (fclose_ee_rec k z t1) (fclose_ee_rec k z t2)
  | ftrm_tabs t1       => ftrm_tabs (fclose_ee_rec k z t1)
  | ftrm_tapp t1 t2    => ftrm_tapp (fclose_ee_rec k z t1) t2
  | ftrm_pair t1 t2    => ftrm_pair (fclose_ee_rec k z t1) (fclose_ee_rec k z t2)
  | ftrm_fst t1        => ftrm_fst (fclose_ee_rec k z t1)
  | ftrm_snd t1        => ftrm_snd (fclose_ee_rec k z t1)
  end.

Definition fclose_ee z t := fclose_ee_rec 0 z t.

(** Close var commutes with open with some freshness conditions,
  this is used in the proofs of [close_ee_open] *)

Lemma fclose_ee_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (ffv_ee t1) ->
    (fopen_ee_rec i (ftrm_fvar y) (fopen_ee_rec j (ftrm_fvar z) (fclose_ee_rec j x t1)))
  = (fopen_ee_rec j (ftrm_fvar z) (fclose_ee_rec j x (fopen_ee_rec i  (ftrm_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ].
  case_var*. simpl. case_nat*.
Qed.

Lemma fclose_te_ee_rec_open : forall x y z t1 i j,
   y \notin (ffv_te t1) ->
    (fopen_te_rec i (ftyp_fvar y) (fopen_ee_rec j (ftrm_fvar z) (fclose_ee_rec j x t1)))
  = (fopen_ee_rec j (ftrm_fvar z) (fclose_ee_rec j x (fopen_te_rec i  (ftyp_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 1 (case_nat; simpl); try solve [ case_var* | case_nat* ].
  auto.
  auto.
  case_var*. simpl. case_nat*.
Qed.

(** Close var removes fresh var *)

Lemma fclose_ee_fresh : forall x t,
  x \notin ffv_ee (fclose_ee x t).
Proof.
  introv. unfold fclose_ee. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

(** Close var is the right inverse of open_var *)

Lemma fclose_ee_open : forall x t,
  fterm t -> t = (fclose_ee x t) fopen_ee_var x.
Proof.
  introv W. unfold fclose_ee, fopen_ee. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u ffv_ee t)) as [y Fr] end.
  apply* (@fopen_ee_rec_inj y).
  unfolds fopen_ee. rewrite* fclose_ee_rec_open.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u ffv_te t)) as [y Fr] end.
  apply* (@fopen_te_rec_inj y).
  unfolds fopen_te. rewrite* fclose_te_ee_rec_open.
Qed.

(** Abstracting a type name out of a type *)

Fixpoint fclose_tt_rec (k : nat) (z : var) (t : ftyp) {struct t} : ftyp :=
  match t with
  | dtyp_bvar i        => ftyp_bvar i
  | dtyp_fvar x        => If x = z then (dtyp_bvar k) else (dtyp_fvar x)
  | dtyp_nat           => ftyp_nat
  | dtyp_arrow t1 t2   => dtyp_arrow (fclose_tt_rec k z t1) (fclose_tt_rec k z t2)
  | dtyp_all t1        => dtyp_all (fclose_tt_rec (S k) z t1)
  | dtyp_pair t1 t2    => dtyp_pair (fclose_tt_rec k z t1) (fclose_tt_rec k z t2)
  end.

(** Abstracting a type name out of a type *)

Fixpoint fclose_te_rec (k : nat) (z : var) (t : ftrm) {struct t} : ftrm :=
  match t with
  | ftrm_bvar i        => ftrm_bvar i
  | ftrm_fvar x        => ftrm_fvar x
  | ftrm_nat  i        => ftrm_nat i
  | ftrm_absann t1 t2  => ftrm_absann (fclose_tt_rec k z t1) (fclose_te_rec k z t2)
  | ftrm_app t1 t2     => ftrm_app (fclose_te_rec k z t1) (fclose_te_rec k z t2)
  | ftrm_tabs t1       => ftrm_tabs (fclose_te_rec (S k) z t1)
  | ftrm_tapp t1 t2    => ftrm_tapp (fclose_te_rec k z t1) (fclose_tt_rec k z t2)
  | ftrm_pair t1 t2    => ftrm_pair (fclose_te_rec k z t1) (fclose_te_rec k z t2)
  | ftrm_fst t1        => ftrm_fst (fclose_te_rec k z t1)
  | ftrm_snd t1        => ftrm_snd (fclose_te_rec k z t1)
  end.

Definition fclose_tt z t := fclose_tt_rec 0 z t.
Definition fclose_te z t := fclose_te_rec 0 z t.

(** Close var commutes with open with some freshness conditions *)

Lemma fclose_tt_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (ffv_tt t1) ->
    (fopen_tt_rec i (ftyp_fvar y) (fopen_tt_rec j (ftyp_fvar z) (fclose_tt_rec j x t1)))
  = (fopen_tt_rec j (ftyp_fvar z) (fclose_tt_rec j x (fopen_tt_rec i  (ftyp_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ].
  case_var*. simpl. case_nat*.
Qed.

(** Close var removes fresh var *)

Lemma fclose_tt_fresh : forall x t k,
  x \notin ffv_tt (fclose_tt_rec k x t).
Proof.
  introv. unfold fclose_tt. gen k.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Hint Resolve fclose_tt_fresh.
Lemma fclose_te_fresh : forall x t,
  x \notin ffv_te (fclose_te x t).
Proof.
  introv. unfold fclose_te. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
Qed.

(** Close var is the right inverse of open_var *)

Lemma fclose_tt_open : forall x t k,
  ftype t -> t = fopen_tt_rec k (ftyp_fvar x) (fclose_tt_rec k x t).
Proof.
  introv W. unfold fclose_tt, fopen_tt. gen k.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u ffv_tt t)) as [y Fr] end.
  apply* (@fopen_tt_rec_inj y).
  unfolds fopen_tt. rewrite* fclose_tt_rec_open.
Qed.

Hint Resolve fclose_tt_rec_open.
Lemma fclose_te_rec_open : forall x y z t1 i j,
   i <> j -> y <> x -> y \notin (ffv_te t1) ->
   y \notin (ffv_te t1) ->
    (fopen_te_rec i (ftyp_fvar y) (fopen_te_rec j (ftyp_fvar z) (fclose_te_rec j x t1)))
  = (fopen_te_rec j (ftyp_fvar z) (fclose_te_rec j x (fopen_te_rec i  (ftyp_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
Qed.

Lemma fclose_ee_te_rec_open : forall x y z t1 i j,
   y \notin (ffv_te t1) ->
    (fopen_ee_rec i (ftrm_fvar y) (fopen_te_rec j (ftyp_fvar z) (fclose_te_rec j x t1)))
  = (fopen_te_rec j (ftyp_fvar z) (fclose_te_rec j x (fopen_ee_rec i (ftrm_fvar y) t1) )).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  case_nat*.
Qed.

Hint Resolve fclose_tt_open.
Lemma fclose_te_open : forall x t k,
  fterm t -> t = fopen_te_rec k (ftyp_fvar x) (fclose_te_rec k x t).
Proof.
  introv W. unfold fclose_te, fopen_te. gen k.
  induction W; intros k; simpls; f_equal*.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u ffv_ee t)) as [y Fr] end.
  apply* (@fopen_ee_rec_inj y).
  unfolds fopen_te. rewrite* fclose_ee_te_rec_open.
  let L := gather_vars in match goal with |- _ = ?t =>
    destruct (var_fresh (L \u ffv_te t)) as [y Fr] end.
  apply* (@fopen_te_rec_inj y).
  unfolds fopen_te. rewrite* fclose_te_rec_open.
Qed.

Lemma fclose_te_open_var : forall x t,
  fterm t -> t = (fclose_te x t) fopen_te_var x.
Proof.
  introv W. unfold fclose_te, fopen_te.
  apply* fclose_te_open.
Qed.

(* close does not introduce new free vairables *)

Lemma fnotin_fv_tt_close_inv: forall S x y k,
    x \notin ffv_tt S ->
    x \notin ffv_tt (fclose_tt_rec k y S).
Proof.
  induction S; introv notin; simpls~.
  case_var~. simpls~.
Qed.

Lemma fnotin_fv_ee_close_te_inv: forall S x y k,
    x \notin ffv_ee S ->
    x \notin ffv_ee (fclose_te_rec k y S).
Proof.
  induction S; introv notin; simpls~.
Qed.

Lemma fnotin_fv_te_close_te_inv: forall S x y k,
    x \notin ffv_te S ->
    x \notin ffv_te (fclose_te_rec k y S).
Proof.
  induction S; introv notin; simpls~.
  apply notin_union. split~.
  apply* fnotin_fv_tt_close_inv.
  apply notin_union. split~.
  apply* fnotin_fv_tt_close_inv.
Qed.

Hint Resolve fnotin_fv_tt_close_inv
     fnotin_fv_ee_close_te_inv
     fnotin_fv_te_close_te_inv.

(** open a closed one is substitution *)

Lemma fclose_tt_open_subst: forall S k x y,
    ftype S ->
    fopen_tt_rec k (ftyp_fvar y) (fclose_tt_rec k x S) = fsubst_tt x (ftyp_fvar y) S.
Proof.
  introv ty. gen k. induction ty; introv; simpls; auto.
  case_var~. simpls. case_nat~.

  rewrite* IHty1. rewrite* IHty2.

  pick_fresh z.
  specializes H0 z (S k).

  rewrite fsubst_tt_open_tt in H0; auto.
  simpls. case_var.

  unfolds dopen_tt.
  unfolds fopen_tt.
  rewrite <- fclose_tt_rec_open in H0; auto.
  apply fopen_tt_rec_inj in H0; auto.
  rewrite~ H0.
  apply fnotin_fv_tt_open_inv; simpls~.
  apply fnotin_fv_tt_subst_inv; simpls~.

  rewrite* IHty1. rewrite* IHty2.
Qed.

Hint Resolve fclose_tt_open_subst.
Lemma fclose_te_open_subst: forall S k x y,
    fterm S ->
    fopen_te_rec k (ftyp_fvar y) (fclose_te_rec k x S) = fsubst_te x (ftyp_fvar y) S.
Proof.
  introv te. gen k. induction te; introv; simpls; auto; f_equal~.

  pick_fresh z.
  specializes H1 z k.
  rewrite <- fsubst_te_open_ee_var in H1; simpls~.
  unfolds fopen_ee.
  rewrite <- fclose_ee_te_rec_open in H1; auto.
  apply fopen_ee_rec_inj in H1; auto.

  pick_fresh z.
  specializes H0 z (S k).
  rewrite <- fsubst_te_open_te_var in H0; simpls~.
  unfolds fopen_te.
  rewrite <- fclose_te_rec_open in H0; auto.
  apply fopen_te_rec_inj in H0; auto.
  apply~ fnotin_fv_te_open_inv. simpls~.
  apply~ fnotin_fv_te_subst_inv. simpls~.
Qed.

Require Import LibLN DeclDef.
Require Import DeclInfra.

(* ********************************************************************** *)
(** ** BE ABLT TO INDUCTION ON DERIVATION HEIGHT *)

Inductive dtyping_size : denv -> dstack -> dtrm -> dtyp -> nat -> Prop :=
  | dtyping_size_var : forall E x T T2 cs,
      dokt E ->
      binds x T E ->
      dsub (stack cs) T T2 ->
      dtyping_size E cs (dtrm_fvar x) T2 1
  | dtyping_size_nat : forall E i,
      dokt E ->
      dtyping_size E nil (dtrm_nat i) (dtyp_nat) 1
  | dtyping_size_absann : forall L E V e1 T1 n,
      dwft \{} V ->
      (forall x, x \notin L ->
            dtyping_size (E & x ~ V) nil (e1 dopen_ee_var x) T1 n) ->
      dtyping_size E nil (dtrm_absann V e1) (dtyp_arrow V T1) (1 + n)
  | dtyping_size_abs : forall L E t e1 T1 n,
      dtype t ->
      dmono t ->
      (forall x, x \notin L ->
            dtyping_size (E & x ~ t) nil (e1 dopen_ee_var x) T1 n) ->
      dtyping_size E nil (dtrm_abs e1) (dtyp_arrow t T1) (1 + n)

  | dtyping_size_absann_stack : forall L E V e1 T1 c cs n,
      dwft \{} V ->
      dsub plain c V ->
      (forall x, x \notin L ->
            dtyping_size (E & x ~ V) cs (e1 dopen_ee_var x) T1 n) ->
      dtyping_size E (cons c cs) (dtrm_absann V e1) (dtyp_arrow c T1) (1 + n)
  | dtyping_size_abs_stack : forall L E e1 T1 c cs n,
      (forall x, x \notin L ->
            dtyping_size (E & x ~ c) cs (e1 dopen_ee_var x) T1 n) ->
      dtyping_size E (cons c cs) (dtrm_abs e1) (dtyp_arrow c T1) (1 + n)
  | dtyping_size_app : forall T1 E e1 e2 T2 cs T3 n1 n2,
      dtyping_size E nil e2 T1 n1 ->
      dgen E T1 T3 ->
      dtyping_size E (cons T3 cs) e1 (dtyp_arrow T3 T2) n2 ->
      dtyping_size E cs (dtrm_app e1 e2) T2 (1 + n1 + n2)
  | dtyping_size_pair : forall E e1 e2 T1 T2 n1 n2,
      dtyping_size E nil e1 T1 n1 ->
      dtyping_size E nil e2 T2 n2 ->
      dtyping_size E nil (dtrm_pair e1 e2) (dtyp_pair T1 T2) (1 + n1 + n2)
  | dtyping_size_fst : forall E T cs,
      dokt E ->
      dsub (stack cs) fst_typ T ->
      dtyping_size E cs dtrm_fst T 1
  | dtyping_size_snd : forall E T cs,
      dokt E ->
      dsub (stack cs) snd_typ T ->
      dtyping_size E cs dtrm_snd T 1
.

Hint Constructors dtyping_size dtyping.
Lemma dtyping_size_dtyping: forall E Q e T n,
    dtyping_size E Q e T n ->
    dtyping E Q e T.
Proof.
  introv ty. induction ty; autos*.
Qed.

Lemma dtyping_size_regular : forall E e T Q n,
  dtyping_size E Q e T n -> dokt E /\ dterm e /\ dtype T /\ dstack_type Q.
Proof.
  introv ty.
  apply dtyping_size_dtyping in ty.
  apply* dtyping_regular.
Qed.

Lemma dgen_subst: forall E F z u U T1 T2,
    dgen (E & z ~ U & F) T1 T2 ->
    dokt (E & u ~ U & F) ->
    dgen (E & u ~ U & F) T1 T2.
Proof.
  introv gen. inductions gen; introv oke; simpls~.
  apply~ dgen_empty.
  rewrite dfv_tt_env_middle_inv in H1.
  rewrite~ dfv_tt_env_middle_inv.

  specializes IHgen E F z U.
  forwards ~  : IHgen.
  apply~ dgen_step.
  rewrite in_remove in H.
  destruct H as [int notin e].
  rewrite in_remove. split~.
  rewrite dfv_tt_env_middle_inv in notin.
  rewrite~ dfv_tt_env_middle_inv.
Qed.


Lemma dtyping_size_subst : forall E F Q t T z u U n,
    dtyping_size (E & z ~ U & F) Q t T n ->
    dokt (E & u ~ U & F) ->
    dtyping_size (E & u ~ U & F) Q (dsubst_ee z (dtrm_fvar u) t) T n.
Proof.
  introv typt. inductions typt; introv okt; simpl; f_equal~.
  case_var*.
    binds_mid~.
    apply dtyping_size_var with U; autos*.
    apply~ binds_middle_eq.
    lets: dok_from_okt okt.
    lets ~ : ok_middle_inv_r H0.

    apply* dtyping_size_var.
    forwards ~ : binds_remove H0.
    apply~ binds_weaken.

  apply_fresh dtyping_size_absann as x; auto.
  specializes H1 x E (F & x ~ V) z U.
  forwards * : H1.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H0 x. forwards ~ : H0.
  lets [? _]: dtyping_size_regular H2. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H2.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_size_abs as x; auto.
  specializes H2 x E (F & x ~ t) z U.
  forwards ~ : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_size_absann_stack as x; auto.
  specializes H2 x E (F & x ~ V) z U.
  forwards * : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H1 x. forwards ~ : H1.
  lets [? _]: dtyping_size_regular H3. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_size_abs_stack as x.
  specializes H0 x E (F & x ~ c) z U.
  forwards ~ : H0.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H x. forwards ~ : H.
  lets [? _]: dtyping_size_regular H1. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H1.
  rewrite* dsubst_ee_open_ee_var.

  apply dtyping_size_app with (T2:=T2) (T1:=T1) (T3:=T3).
  specializes IHtypt1 E F z U.
  apply* dgen_subst.
  specializes IHtypt2 E F z U.

  apply dtyping_size_pair with (T2:=T2) (T1:=T1).
  specializes IHtypt1 E F z U.
  specializes IHtypt2 E F z U.
Qed.

Lemma dtyping_size_rename : forall x Q y E t S V n,
  dtyping_size (E & x ~ V) Q (S dopen_ee_var x) t n ->
  x \notin dom E \u dfv_ee S ->
  y \notin dom E \u dfv_ee S ->
  dtyping_size (E & y ~ V) Q (S dopen_ee_var y) t n.
Proof.
  introv Typx Frx Fry.
  tests: (x = y). subst*.
  rewrite~ (@dsubst_ee_intro x).
  assert  (dtyping_size (E & y ~ V & empty) Q (dsubst_ee x (dtrm_fvar y) (S dopen_ee_var x)) t n).
  apply dtyping_size_subst.
  rewrite~ concat_empty_r.
  rewrite~ concat_empty_r.
  apply~ dokt_typ.
  lets [? ?]: dtyping_size_regular Typx.
  lets~ : dokt_concat_left_inv H.
  lets [? ?]: dtyping_size_regular Typx.
  apply* dtype_from_okt_typ.

  rewrite~ concat_empty_r in H.
Qed.

Lemma dtyping_dtyping_size: forall E Q e T,
    dtyping E Q e T ->
    exists n, dtyping_size E Q e T n.
Proof.
  introv ty. induction ty; autos*.
  pick_fresh y.
  forwards~ (n & ?) : H1 y.
  exists~ (1 + n). apply_fresh dtyping_size_absann as z; auto.
  apply* dtyping_size_rename.

  pick_fresh y.
  forwards~ (n & ?) : H2 y.
  exists~ (1 + n). apply_fresh dtyping_size_abs as z; auto.
  apply* dtyping_size_rename.

  pick_fresh y.
  forwards~ (n & ?) : H2 y.
  exists~ (1 + n). apply_fresh dtyping_size_absann_stack as z; auto.
  apply* dtyping_size_rename.

  pick_fresh y.
  forwards~ (n & ?) : H0 y.
  exists~ (1 + n). apply_fresh dtyping_size_abs_stack as z; auto.
  apply* dtyping_size_rename.

  inversion IHty1 as (n1 & ?).
  inversion IHty2 as (n2 & ?).
  exists (1 + n1 + n2).
  apply* dtyping_size_app.

  inversion IHty1 as (n1 & ?).
  inversion IHty2 as (n2 & ?).
  exists (1 + n1 + n2).
  apply* dtyping_size_pair.
Qed.

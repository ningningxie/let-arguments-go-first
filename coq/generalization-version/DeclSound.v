Require Import LibList LibLN DeclDef.
Require Import DeclInfra.
Require Import DTypingSize.
Set Implicit Arguments.

(* ********************************************************************** *)
(** ** Stack Type*)

Hint Constructors dstack_type.
Fixpoint stack_type (Q:dstack) (d:dtyp): dtyp :=
  match Q with
  | nil => d
  | cons c cs => dtyp_arrow c (stack_type cs d)
  end.

(* ********************************************************************** *)
(** ** Sub Relation of Stack*)

Inductive stack_sub : dstack -> dstack -> Prop :=
| stack_sub_relf: stack_sub nil nil
| stack_sub_add : forall x y cs1 cs2,
    stack_sub cs1 cs2 ->
    dsub plain x y ->
    stack_sub (cons x cs1) (cons y cs2).
Hint Constructors stack_sub.

(* ********************************************************************** *)
(** ** SUBTYPING PROPERTY *)

Hint Constructors stack_sub.
Hint Constructors dstack_type.

Lemma dsub_refl: forall t,
    dtype t ->
    dsub plain t t.
Proof.
  introv ty.
  induction ty; simpls~; try constructor~.
  apply_fresh dsub_allr as x.
  apply dsub_alll1 with (dtyp_fvar x); auto.
Qed.
Hint Resolve dsub_refl.

Lemma dsub_refl_stack: forall Q f,
   dtype f ->
   dstack_type Q ->
   dsub (stack Q) (stack_type Q f) (stack_type Q f).
Proof.
  induction Q; introv ty dq; simpls~.
  apply~ dsub_empty.
  inversions~ dq.
  apply~ dsub_arrow_stack.
Qed.

Lemma dsub_stack_typ: forall cs T T2,
   dsub (stack cs) T T2 ->
   exists f, T2 = stack_type cs f.
Proof.
  introv sub. inductions sub.
  exists~ A.
  apply* IHsub.
  simpls. inversion IHsub2.
  exists~ x. rewrite <- H. auto.
Qed.

Hint Resolve dsub_stack_typ.

Lemma dsub_subsumption: forall Q1 Q2 f f2 T,
  dsub (stack Q1) T (stack_type Q1 f) ->
  dsub (stack Q2) f f2 ->
  dsub (stack (Q1 ++ Q2)) T (stack_type Q1 f2).
Proof.
  introv sub1 sub2.
  inductions sub1. simpls.
  rewrite~ app_nil_l.

  rewrite app_cons.
  apply dsub_alll2 with U; auto.
  rewrite <- app_cons.
  apply* IHsub1.

  simpls.
  apply* dsub_arrow_stack.
Qed.

Hint Resolve dsub_subsumption.

Lemma dsub_stack_to_plain: forall t1 t2 cs,
    dsub (stack cs) t1 t2 ->
    dsub plain t1 t2.
Proof.
  introv sub. inductions sub.
  apply~ dsub_refl.
  apply dsub_alll1 with U; auto.
  apply~ IHsub.
  apply dsub_arrow; auto.
Qed.

Hint Constructors dsub.
Lemma dsub_plain_to_stack: forall t1 t2 cs,
    dsub plain t1 (stack_type cs t2) ->
    exists t3, dsub (stack cs) t1 (stack_type cs t3) /\ dsub plain t3 t2.
Proof.
  introv sub. inductions sub.
  destruct cs; simpls~; subst~.
    exists dtyp_nat. split~.
    false x.
  destruct cs; simpls~; subst~.
    exists (dtyp_fvar X). split~.
    false x.
  destruct cs; simpls~; subst~.
    exists (dtyp_arrow S1 S2). split~. apply~ dsub_empty.
    apply~ dtype_arrow.
    apply (proj22 (dsub_regular sub1)).
    apply (proj21 (dsub_regular sub2)).

    inversion x; subst~.
    forwards ~ : IHsub2.
    destruct H as (t3 & [stack_t3 sub_t3]).
    exists~ t3.

  destruct cs; simpls~; subst~.
    exists~ (dtyp_all S2). split~. apply~ dsub_empty.
    lets [? ?]: dsub_regular sub.
    apply_fresh dtype_all as x.
    apply dtype_open_inv with U; auto.
    apply dsub_alll1 with U; auto.

    destruct IHsub as (t3 & [stack_t3 sub_t3]).
    exists~ t3. split~.
    apply dsub_alll2 with U; auto.

  destruct cs; simpls~; subst~.
    exists S2. split~. apply~ dsub_empty.
    pick_fresh y. forwards ~ : H y.
    apply (proj21 (dsub_regular H1)).
    apply* dsub_allr.

    false x.

  destruct cs; simpls~; subst~.
    exists (dtyp_pair T1 T2). split~. apply~ dsub_empty.
    apply~ dtype_pair.
    apply (proj21 (dsub_regular sub1)).
    apply (proj21 (dsub_regular sub2)).

    inversion x; subst~.
Qed.


(* ********************************************************************** *)
(** ** SUBTYPING TRANSITIVITY *)

Inductive dsub_size : aenv -> dtyp -> dtyp -> nat -> Prop :=
  |dsub_size_nat :
     dsub_size plain dtyp_nat dtyp_nat 1
  |dsub_size_tvar : forall X,
     dsub_size plain (dtyp_fvar X) (dtyp_fvar X) 1
  |dsub_size_arrow : forall S1 S2 T1 T2 n1 n2,
     dsub_size plain T1 S1 n1 ->
     dsub_size plain S2 T2 n2 ->
     dsub_size plain (dtyp_arrow S1 S2) (dtyp_arrow T1 T2) (1 + n1 + n2)
  |dsub_size_alll1 : forall S2 T2 U n1,
     dtype U ->
     dmono U ->
     dsub_size plain (dopen_tt S2 U) T2 n1 ->
     dsub_size plain (dtyp_all S2) T2 (1 + n1)
  |dsub_size_allr : forall L S2 T2 n1,
     (forall X, X \notin L ->
           dsub_size plain S2 (T2 dopen_tt_var X) n1) ->
     dsub_size plain S2 (dtyp_all T2) (1 + n1)

  |dsub_size_empty : forall A,
      dtype A ->
      dsub_size (stack nil) A A 1
  |dsub_size_alll2 : forall S2 T2 U c cs n1,
     dtype U ->
     dmono U ->
     dsub_size (stack (cons c cs)) (dopen_tt S2 U) T2 n1 ->
     dsub_size (stack (cons c cs)) (dtyp_all S2) T2 (1 + n1)
  |dsub_size_arrow_stack : forall S1 S2 T2 c cs n1 n2,
     dsub_size plain c S1 n1 ->
     dsub_size (stack cs) S2 T2 n2 ->
     dsub_size (stack (cons c cs)) (dtyp_arrow S1 S2) (dtyp_arrow c T2) (1 + n1 + n2)
  |dsub_size_pair : forall T1 T2 T3 T4 n1 n2,
      dsub_size plain T1 T3 n1 ->
      dsub_size plain T2 T4 n2 ->
      dsub_size plain (dtyp_pair T1 T2) (dtyp_pair T3 T4) (1 + n1 + n2).


Hint Constructors dsub_size.

Lemma dsub_size_dsub: forall Q t1 t2 n,
    dsub_size Q t1 t2 n ->
    dsub Q t1 t2.
Proof.
  introv sub. induction sub; autos~.
  apply dsub_alll1 with U; auto.
  apply_fresh dsub_allr as x; auto.
  apply dsub_alll2 with U; auto.
Qed.

Lemma dsub_size_subst_plain_var : forall t T z u n,
    dsub_size plain T t n ->
    dtype (dtyp_fvar u) ->
    dsub_size plain (dsubst_tt z (dtyp_fvar u) T) (dsubst_tt z (dtyp_fvar u) t) n.
Proof.
  introv Typt Typu. inductions Typt; introv; simpl; autos~.
  case_var*.

  forwards ~: IHTypt1.
  forwards ~: IHTypt2.
  apply~ dsub_size_arrow.

  forwards ~ : IHTypt.
  apply dsub_size_alll1 with (dsubst_tt z (dtyp_fvar u) U); auto.
  apply* dsubst_mono.
  rewrite* <- dsubst_tt_open_tt.

  apply_fresh* dsub_size_allr as x.
  specializes H0 x.
  forwards * : H0.
  rewrite* dsubst_tt_open_tt_var.

  forwards ~: IHTypt1.
  forwards ~: IHTypt2.
  apply~ dsub_size_pair.
Qed.

Lemma dsub_size_rename : forall x y t S n,
  dsub_size plain S (t dopen_tt_var x) n ->
  x \notin dfv_tt t \u dfv_tt S ->
  y \notin dfv_tt t \u dfv_tt S ->
  dsub_size plain S (t dopen_tt_var y) n.
Proof.
  introv Typx Frx Fry.
  tests: (x = y). subst*.
  rewrite~ (@dsubst_tt_intro x).
  assert  (dsub_size plain (dsubst_tt x (dtyp_fvar y) S) (dsubst_tt x (dtyp_fvar y) (t dopen_tt_var x)) n).
  apply* dsub_size_subst_plain_var.
  assert (dsubst_tt x (dtyp_fvar y) S = S).
  apply* dsubst_tt_fresh.
  rewrite H0 in H. auto.
Qed.

Lemma dsub_dsub_size: forall Q t1 t2,
    dsub Q t1 t2 ->
    exists n, dsub_size Q t1 t2 n.
Proof.
  introv sub. induction sub; autos~; try(solve[exists~ 1]).
  destruct IHsub1 as (n1 & ?).
  destruct IHsub2 as (n2 & ?).
  exists~ (1 + n1 + n2).

  destruct IHsub as (n1 & ?).
  exists~ (1 + n1).
  apply dsub_size_alll1 with U; auto.

  pick_fresh y. forwards ~ : H0 y.
  destruct H1 as (n1 & ?).
  exists~ (1 + n1).
  apply_fresh dsub_size_allr as z; auto.
  apply* dsub_size_rename.

  destruct IHsub as (n1 & ?).
  exists~ (1 + n1).
  apply dsub_size_alll2 with U; auto.

  destruct IHsub1 as (n1 & ?).
  destruct IHsub2 as (n2 & ?).
  exists~ (1 + n1 + n2).

  destruct IHsub1 as (n1 & ?).
  destruct IHsub2 as (n2 & ?).
  exists~ (1 + n1 + n2).
Qed.

Lemma dsub_size_regular: forall Q t1 t2 n,
    dsub_size Q t1 t2 n ->
    dtype t1 /\ dtype t2.
Proof.
  introv sb.
  lets: dsub_size_dsub sb.
  lets~ [? [? ?]]: dsub_regular H.
Qed.

Fixpoint num_of_all (t: dtyp) : nat :=
  match t with
      | dtyp_nat => 0
      | dtyp_fvar x => 0
      | dtyp_bvar x => 0
      | dtyp_arrow n1 n2 => (num_of_all n1) + (num_of_all n2)
      | dtyp_all n => 1 + (num_of_all n)
      | dtyp_pair n1 n2 => (num_of_all n1) + (num_of_all n2)
  end.

Hint Resolve dsub_size_dsub.

Lemma dsub_subst_plain_var : forall t T z U,
    dsub plain T t ->
    dtype U ->
    dmono U ->
    dsub plain (dsubst_tt z U T) (dsubst_tt z U t).
Proof.
  introv Typt Typu mo. inductions Typt; introv; simpl; autos~.
  case_var*.

  rewrite dsubst_tt_open_tt in IHTypt; auto.
  apply dsub_alll1 with (dsubst_tt z U U0); auto.
  apply* dsubst_mono.

  apply_fresh* dsub_allr as x.
  specializes H0 x.
  forwards * : H0.
  rewrite* dsubst_tt_open_tt_var.
Qed.

Lemma dsub_subst_stack_var : forall t T z U Q,
    dsub (stack Q) T t ->
    dtype U ->
    dmono U ->
    dsub (stack (dsubst_stack z U Q)) (dsubst_tt z U T) (dsubst_tt z U t).
Proof.
  introv Typt Typu mo. inductions Typt; introv; simpl; autos~.
  forwards ~ : IHTypt.
  simpls.
  rewrite dsubst_tt_open_tt in H1; auto.
  apply dsub_alll2 with (dsubst_tt z U U0); auto.
  apply* dsubst_mono.

  apply dsub_arrow_stack; auto.
  apply* dsub_subst_plain_var.
Qed.

Lemma num_of_all_mono: forall U,
    dmono U ->
    num_of_all U = 0.
Proof.
  induction U; introv mn; simpls~.
  inversion~ mn; subst~. rewrite~ IHU1.
  inversion mn.
  inversion~ mn; subst~. rewrite~ IHU1.
Qed.

Lemma num_of_all_open_rec_mono: forall S U k,
    dmono U ->
    num_of_all (dopen_tt_rec k U S) = num_of_all S.
Proof.
  induction S; introv mn; simpls~.
  case_nat~. apply~ num_of_all_mono.
Qed.

Lemma num_of_all_open_mono: forall S U,
    dmono U ->
    num_of_all (dopen_tt S U) = num_of_all S.
Proof.
  intros. apply* num_of_all_open_rec_mono.
Qed.

Lemma sub_trans_help : forall m n s1 s2 s3 n1 n2,
    num_of_all s2 < m ->
    dsub_size plain s2 s1 n1 ->
    dsub_size plain s3 s2 n2 ->
    n1 + n2 < n ->
    dsub plain s3 s1.
Proof.
  intro m. induction m; introv lem.
  inversion lem.

  gen s1 s2 s3 n1 n2.
  induction n; introv lenm sub21 sub32 len.
  inversion len.

  inductions sub21; subst~.

  (* nat *)
  inductions sub32; subst~.
  apply dsub_alll1 with U; auto. apply* dsub_size_dsub.

  (* var *)
  inductions sub32; subst~.
  apply dsub_alll1 with U; auto. apply* dsub_size_dsub.

  (* arrow *)
  clear IHsub21_1 IHsub21_2.
  inductions sub32; intros; subst~.
      (* arrow x arrow *)
      simpls. apply~ dsub_arrow.
      forwards ~ : IHn sub32_1 sub21_1. Omega.omega. Omega.omega.
      forwards ~ : IHn sub21_2 sub32_2. Omega.omega. Omega.omega.
      (* all x arrow *)
      apply dsub_alll1 with U; auto.
      forwards ~ : IHsub32 sub21_1 sub21_2.
      Omega.omega.

  (* alll *)
  clear IHsub21.
  inductions sub32; intros; subst~.
      (* alll x alll *)
      apply dsub_alll1 with U0; auto.
      forwards ~ : IHsub32 IHn.
      Omega.omega.
      (* allr x alll *)
      clear H2.
      pick_fresh y. forwards ~ : H1 y. clear H1.
      lets: dsub_size_dsub H2.
      forwards ~ : dsub_subst_plain_var y U H1.
      rewrite dsubst_tt_fresh in H3; auto.
      rewrite dsubst_tt_open_tt in H3; auto. simpls~. case_var~.
      rewrite dsubst_tt_fresh in H3; auto.
      lets (ni1 & I1) : dsub_dsub_size H3.
      forwards ~ : IHm sub21 I1.
      rewrite~ num_of_all_open_mono. Omega.omega.

  (* allr *)
  apply_fresh dsub_allr as x; auto.
  apply~ H0. Omega.omega.

  (* pair *)
  clear IHsub21_1 IHsub21_2.
  inductions sub32; intros; subst~.
      (* all x pair *)
      apply dsub_alll1 with U; auto.
      forwards ~ : IHsub32 sub21_1 sub21_2.
      Omega.omega.
      (* pair x pair *)
      simpls. apply~ dsub_pair.
      forwards ~ : IHn sub21_1 sub32_1. Omega.omega. Omega.omega.
      forwards ~ : IHn sub21_2 sub32_2. Omega.omega. Omega.omega.

Qed.

Lemma sub_trans : forall s1 s2 s3,
    dsub plain s2 s1 ->
    dsub plain s3 s2 ->
    dsub plain s3 s1.
Proof.
  introv sub21 sub32.
  lets (n1 & I1): dsub_dsub_size sub21.
  lets (n2 & I2): dsub_dsub_size sub32.
  apply* sub_trans_help.
Qed.

(* depending on transitivity *)

Lemma dsub_weaker_stack: forall Q Q2 t f,
  dsub (stack Q) t (stack_type Q f) ->
  stack_sub Q2 Q ->
  exists f2, dsub (stack Q2) t (stack_type Q2 f2) /\ dsub plain f2 f.
Proof.
  introv sub stk. gen Q2.
  inductions sub; introv stk; simpls~.

  inversions~ stk.
  exists f. splits~.

  forwards ~ : IHsub (c::cs) Q2. simpls~.
  destruct H1 as (f2 & [? ?]).
  exists f2. splits~.
  inversions stk.
  apply dsub_alll2 with U; auto.

  inversions stk.
  forwards ~ : IHsub2 H2.
  destruct H as (f2 & [? ?]).
  exists f2. split~.
  simpls~. apply~ dsub_arrow_stack.
  lets~ : sub_trans sub1 H3.
Qed.

(* ********************************************************************** *)
(** ** Wellformedness of Stack *)

Lemma dtype_stack_type : forall Q f,
    dtype (stack_type Q f) -> dstack_type Q /\ dtype f.
Proof.
  inductions Q; introv ty; simpls~.
  inversions~ ty.
  splits~. apply~ dstack_type_cons. apply* IHQ.
  apply* IHQ.
Qed.

Lemma dtype_from_dstack: forall s t,
    dstack_type s ->
    dtype t ->
    dtype (stack_type s t).
Proof.
  introv ds dt.
  induction ds; simpls~.
Qed.

Lemma stack_sub_regular: forall N M,
    stack_sub N M ->
    dstack_type N /\ dstack_type M.
Proof.
  introv ds. induction ds; simpls~; split~.
  apply~ dstack_type_cons.
  apply (proj21 (dsub_regular H)).
  destruct~ IHds.
  apply~ dstack_type_cons.
  apply (proj22 (dsub_regular H)).
  destruct~ IHds.
Qed.

Lemma stack_sub_contra: forall N M f,
    stack_sub N M ->
    dtype f ->
    dsub plain (stack_type M f) (stack_type N f).
Proof.
  introv ds ty.
  induction ds; simpls~.
Qed.

Lemma stack_sub_co: forall N t1 t2,
    dstack_type N ->
    dsub plain t1 t2 ->
    dsub plain (stack_type N t1) (stack_type N t2).
Proof.
  introv ds ty.
  induction ds; simpls~.
Qed.

Lemma stack_type_app: forall N M f,
    stack_type N (stack_type M f) = stack_type (N ++ M) f.
Proof.
  inductions N; introv; autos~.
  rewrite app_cons. simpls~.
  f_equal~.
Qed.

(* ********************************************************************** *)
(** * Properties of Generalization *)

Lemma dgen_nat: forall E,
    dokt E ->
    dgen E dtyp_nat dtyp_nat.
Proof.
  intros. apply~ dgen_empty. simpls.
  assert (\{} \c (\{} \- dfv_tt_env E )).
  apply subset_empty_l.
  assert ((\{} \- dfv_tt_env E ) \c  \{}).
  unfold subset. intros.
  rewrite in_remove in H1. destruct H1.
  rewrite in_empty in H1. false.
  forwards ~ : fset_extens H0 H1.
Qed.

Lemma dgen_nat_inv: forall E T,
    dgen E dtyp_nat T ->
    T = dtyp_nat.
Proof.
  intros.  inversions~ H.
  rewrite H0 in H1.
  rewrite in_remove in H1. simpls.
  destruct H1. rewrite* in_empty in H.
Qed.

Lemma dgen_sub: forall E t s,
    dgen E t s ->
    dsub plain s t.
Proof.
  introv gen. induction gen; simpls~.
  clear H gen.
  assert (dsub plain (dtyp_all t) (t dopen_tt_var x)).
  apply dsub_alll1 with (dtyp_fvar x); auto.
  apply~ dsub_refl.
  lets [? [? ?]] : dsub_regular IHgen. inversion H1.
  pick_fresh y. forwards ~ : H4 y. apply* dtype_open_inv.

  lets ~ : sub_trans H IHgen.
Qed.

Lemma dgen_sub_preserve: forall t1 t2 t3 t4 E,
    dsub plain t1 t2 ->
    dgen E t1 t3 ->
    dgen E t2 t4 ->
    dsub plain t3 t4.
Proof.
  introv sub gen1 gen2. gen t1.
  induction gen2; introv sub gen1.
  lets: dgen_sub gen1.
  lets~ : sub_trans sub H2.

  inductions gen1; subst~.
  apply~ IHgen2.
  apply_fresh dsub_allr as x.
  forwards ~ : dsub_subst_plain_var x (dtyp_fvar y) sub.
  rewrite dsubst_tt_fresh in H4; auto.
  rewrite <- dsubst_tt_intro in H4; auto.
  exact H4.
  rewrite dsubst_tt_open_tt in H4; auto.
  introv int0.
  rewrite in_remove in H. destruct H.
  assert (x \in dfv_tt t0 \- dfv_tt_env E). rewrite in_remove. split~.
  rewrite H3 in H6. rewrite in_empty in H6. false H6.

  (* *)
  apply~ dgen_empty.
  apply~ IHgen1.
  apply dsub_alll1 with (dtyp_fvar x0); auto.
Qed.

Lemma din_subst_tt_open_tt: forall x y u t,
    x \notin dfv_tt t ->
    x \in dfv_tt (t dopen_tt_var x) ->
    x \in dfv_tt (dsubst_tt y u t dopen_tt_var x).
Proof.
  unfolds dopen_tt. generalize 0. introv xin neq.
  gen n. inductions t; introv neq; simpls~.
  case_var~.
  rewrite in_union in neq. destruct neq.
  forwards * : IHt1. rewrite in_union. left~.
  forwards * : IHt2. rewrite in_union. right~.

  rewrite in_union in neq. destruct neq.
  forwards * : IHt1. rewrite in_union. left~.
  forwards * : IHt2. rewrite in_union. right~.
Qed.

Lemma dgen_subst_tvar: forall E t1 t2 x y,
    dgen E t1 t2 ->
    y \notin dfv_tt_env E \u dfv_tt t1 ->
    dgen (map (dsubst_tt x (dtyp_fvar y)) E)
         (dsubst_tt x (dtyp_fvar y) t1)
         (dsubst_tt x (dtyp_fvar y) t2).
Proof.
  introv gen not.
  induction gen.
  apply~ dgen_empty.
  apply~ dokt_subst_tt_empty.

  assert (\{} \c
   dfv_tt (dsubst_tt x (dtyp_fvar y) t) \-
   dfv_tt_env (map (dsubst_tt x (dtyp_fvar y)) E)).
  apply subset_empty_l.
  assert (dfv_tt (dsubst_tt x (dtyp_fvar y) t) \-
                dfv_tt_env (map (dsubst_tt x (dtyp_fvar y)) E) \c \{}).
  unfolds subset.
  intros.
  rewrite in_remove in H3. destruct H3.
  apply din_subst_tt_inv in H3.
  apply dnotin_dfv_tt_env_inv in H4.

  destruct H3 as [[? ?] | [? ?]].

  destruct H4 as [[? [? | ?]] | [? [? ?]]].
    assert (x0 \in (dfv_tt t \- dfv_tt_env E)).
    rewrite in_remove. split~.
    rewrite H1 in H7. auto.

    assert (x0 \in (dfv_tt t \- dfv_tt_env E)).
    rewrite in_remove. split~.
    rewrite H1 in H7. auto.

    subst~. false~ H5.

  simpls. rewrite in_singleton in H3. subst~.
  destruct H4 as [[? [? | ?]] | [? [? ?]]].
    false H4. apply in_singleton_self.

    assert (x \in (dfv_tt t \- dfv_tt_env E)).
    rewrite in_remove. split~.
    rewrite H1 in H6.
    rewrite in_empty in H6. false H6.

    false H6. apply in_singleton_self.

  forwards ~ : fset_extens H2 H3.

  (* step *)
  simpls.
  assert (y \notin dfv_tt t). apply* dnotin_fv_tt_dopen.
  forwards ~ : IHgen.

  rewrite~ dsubst_tt_open_tt.
  rewrite in_remove in H. destruct H.

  simpls~. case_var~.
  apply~ dgen_step.
  rewrite~ dsubst_tt_fresh.
  rewrite~ dsubst_tt_env_fresh.
  apply din_open_tt_inv in H.
  destruct H.
  apply H0 in H. false~.
  rewrite in_remove. split~.

  rewrite~ dsubst_tt_fresh.

  forwards ~ : IHgen. clear IHgen.
  tests : (x0 = y).
  rewrite notin_union in not. destruct not.
  apply H6 in H. false~.

  apply~ dgen_step.
  rewrite in_remove. splits~.

  apply* din_subst_tt_open_tt.
  apply~ dnotin_fv_subst_tt_env_inv. simpls~.

  unfold notin. introv xin0.
  apply din_subst_tt_inv in xin0.
  destruct xin0 as [[? ?] |[? ?]].
  apply H0 in H5. false~.
  simpls. rewrite in_singleton in H5. apply C0 in H5. false~.
Qed.

(* ********************************************************************** *)
(** ** TYPING PROPERTY *)


Lemma dtyping_stack_typ: forall E u U Q,
    dtyping E Q u U ->
    exists f, U = stack_type Q f.
Proof.
  introv ty.
  inductions ty; simpls~; autos*.

  simpls. pick_fresh y.
  forwards * : H2 y. inversion H3. subst~.
  exists~ x.

  simpls. pick_fresh y.
  forwards * : H0 y. inversion H1. subst~.
  exists~ x.

  simpls. inversion IHty2. inversion H0.
  exists~ x.
Qed.

Lemma dtyping_more_stack : forall E u Q f Q2,
    dtyping E Q u (stack_type Q (stack_type Q2 f)) ->
    dtyping E (Q ++ Q2) u (stack_type Q (stack_type Q2 f)).
Proof.
  introv ty.
  inductions ty; simpls~.

  (* var *)
  apply* dtyping_var.
  apply* dsub_subsumption.
  apply~ dsub_refl_stack.
  lets [_ [? _]]: dsub_regular H1.
  lets [? ?]: dtype_stack_type H2.
  lets~ [? ?] : dtype_stack_type H4.
  lets [_ [? _]]: dsub_regular H1.
  lets [? ?]: dtype_stack_type H2.
  lets~ [? ?] : dtype_stack_type H4.

  (* nat *)
  destruct Q2; simpls.
  subst. apply* dtyping_nat.
  inversion x.

  (* absann *)
  destruct Q2; simpls.
  subst. rewrite app_nil_r.
  apply_fresh dtyping_absann as x; auto.
  inversions x.
  rewrite app_nil_l in *.
  apply_fresh dtyping_absann_stack as x; auto.
  apply~ dsub_refl.
    pick_fresh y. forwards ~ : H1 y.
    lets [? [? ?]]: dtyping_regular H2.
    apply* dtype_from_okt_typ.
  forwards ~ : H1 x.

  (* abs *)
  destruct Q2; simpls.
  subst. rewrite app_nil_r.
  apply_fresh dtyping_abs as x; auto.
  inversions x.
  rewrite app_nil_l in *.
  apply_fresh dtyping_abs_stack as y; auto.
  forwards ~ : H2 y.

  (* absann *)
  rewrite app_cons.
  apply_fresh dtyping_absann_stack as x; auto.

  (* abs *)
  rewrite app_cons.
  apply_fresh dtyping_abs_stack as x; auto.

  (* app *)
  apply* dtyping_app.

  (* pair *)
  destruct Q2; simpls.
  subst. rewrite app_nil_r.
  apply* dtyping_pair.

  inversions x.

  (* fst *)
  apply* dtyping_fst.
  apply* dsub_subsumption.
  apply~ dsub_refl_stack.
  lets [_ [? _]]: dsub_regular H0.
    lets [? ?]: dtype_stack_type H1.
    lets~ [? ?] : dtype_stack_type H3.
  lets [_ [? _]]: dsub_regular H0.
    lets [? ?]: dtype_stack_type H1.
    lets~ [? ?] : dtype_stack_type H3.

  (* snd *)
  apply* dtyping_snd.
  apply* dsub_subsumption.
  apply~ dsub_refl_stack.
  lets [_ [? _]]: dsub_regular H0.
    lets [? ?]: dtype_stack_type H1.
    lets~ [? ?] : dtype_stack_type H3.
  lets [_ [? _]]: dsub_regular H0.
    lets [? ?]: dtype_stack_type H1.
    lets~ [? ?] : dtype_stack_type H3.
Qed.

Lemma dtyping_inf_to_chk : forall E u Q f,
    dtyping E nil u (stack_type Q f) ->
    dtyping E Q u (stack_type Q f).
Proof.
  intros.
  assert (stack_type Q f = stack_type nil (stack_type Q f)). simpls~.
  rewrite H0 in H.
  lets: dtyping_more_stack H.
  rewrite app_nil_l in H1.
  simpls~.
Qed.

Lemma dtyping_subst : forall E F Q t T z u U,
    dtyping (E & z ~ U & F) Q t T ->
    dokt (E & u ~ U & F) ->
    dtyping (E & u ~ U & F) Q (dsubst_ee z (dtrm_fvar u) t) T.
Proof.
  introv typt. inductions typt; introv okt; simpl; f_equal~.
  case_var*.
    binds_mid~.
    apply dtyping_var with U; autos*.
    apply~ binds_middle_eq.
    lets: dok_from_okt okt.
    lets ~ : ok_middle_inv_r H0.

    apply* dtyping_var.
    forwards ~ : binds_remove H0.
    apply~ binds_weaken.

  apply_fresh dtyping_absann as x; auto.
  specializes H1 x E (F & x ~ V) z U.
  forwards * : H1.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H0 x. forwards ~ : H0.
  lets [? _]: dtyping_regular H2. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H2.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_abs as x; auto.
  specializes H2 x E (F & x ~ t) z U.
  forwards ~ : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_absann_stack as x; auto.
  specializes H2 x E (F & x ~ V) z U.
  forwards * : H2.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H1 x. forwards ~ : H1.
  lets [? _]: dtyping_regular H3. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H3.
  rewrite* dsubst_ee_open_ee_var.

  apply_fresh dtyping_abs_stack as x.
  specializes H0 x E (F & x ~ c) z U.
  forwards ~ : H0.
  rewrite* concat_assoc.
  rewrite~ concat_assoc.
  apply~ dokt_typ.
  specializes H x. forwards ~ : H.
  lets [? _]: dtyping_regular H1. apply* dtype_from_okt_typ.
  rewrite concat_assoc in H1.
  rewrite* dsubst_ee_open_ee_var.

  apply dtyping_app with (T2:=T2) (T1:=T1) (T3:=T3); auto.
  specializes IHtypt1 E F z U.
  apply* dgen_subst.
Qed.

Lemma dtyping_rename : forall x Q y E t S V,
  dtyping (E & x ~ V) Q (S dopen_ee_var x) t ->
  x \notin dom E \u dfv_ee S ->
  y \notin dom E \u dfv_ee S ->
  dtyping (E & y ~ V) Q (S dopen_ee_var y) t.
Proof.
  introv Typx Frx Fry.
  tests: (x = y). subst*.
  rewrite~ (@dsubst_ee_intro x).
  assert  (dtyping (E & y ~ V & empty) Q (dsubst_ee x (dtrm_fvar y) (S dopen_ee_var x)) t).
  apply dtyping_subst.
  rewrite~ concat_empty_r.
  rewrite~ concat_empty_r.
  apply~ dokt_typ.
  lets [? ?]: dtyping_regular Typx.
  lets~ : dokt_concat_left_inv H.
  lets [? ?]: dtyping_regular Typx.
  apply* dtype_from_okt_typ.

  rewrite~ concat_empty_r in H.
Qed.

Lemma dsubst_tt_fst_typ : forall Z U,
   dsubst_tt Z U fst_typ = fst_typ.
Proof.
  introv.
  apply~ dsubst_tt_fresh.
  simpls~.
Qed.

Lemma dsubst_tt_snd_typ : forall Z U,
   dsubst_tt Z U snd_typ = snd_typ.
Proof.
  introv.
  apply~ dsubst_tt_fresh.
  simpls~.
Qed.

Lemma dtyping_size_subst_tvar_helper : forall m n E Q e T x,
    dtyping_size E Q e T n ->
    n < m ->
    exists L, (forall y,
             y \notin L ->
             dtyping_size (map (dsubst_tt x (dtyp_fvar y)) E)
                          (dsubst_stack x (dtyp_fvar y) Q)
                          e
                          (dsubst_tt x (dtyp_fvar y) T)
                          n).
Proof.
  intro m. induction m; introv typt len.
  inversion len.

  inductions typt; simpls~; f_equal~.

  (* var *)
  exists (\{}: vars). intro y; intros.
  apply dtyping_size_var with (dsubst_tt x (dtyp_fvar y) T); autos*.
  apply~ dokt_subst_tt_empty.
  apply~ dsub_subst_stack_var.

  (* nat *)
  exists (\{}: vars). intro y; intros.
  apply~ dtyping_size_nat.
  apply~ dokt_subst_tt_empty.

  (* absann *)
  pick_fresh z.
  forwards ~ (L1 & ?): H1 z. Omega.omega. clear H1.
  exists L1. intros.
  rewrite~ dwft_subst; auto.
  apply_fresh dtyping_size_absann as u; auto.
  forwards ~ : H2 y.
  rewrite map_push in H3.
  rewrite dwft_subst in H3; auto.
  apply* dtyping_size_rename.

  (* abs *)
  pick_fresh z.
  forwards ~ (L1 & ?): H2 z. Omega.omega. clear H2.
  exists L1. intros.
  apply_fresh dtyping_size_abs as u; auto.
  apply* dsubst_mono.
  forwards ~ : H3 y.
  rewrite map_push in H4.
  apply* dtyping_size_rename.

  (* absann stack *)
  pick_fresh z.
  forwards ~ (L1 & ?): H2 z. Omega.omega. clear H2.
  exists L1. intros.
  apply_fresh dtyping_size_absann_stack as u; auto.
  assert (dsubst_tt x (dtyp_fvar y) V = V). apply* dsubst_tt_fresh.
    apply* dwft_no_ftv.
  rewrite <- H4. apply* dsub_subst_plain_var.
  forwards ~ : H3 y.
  rewrite map_push in H4.
  rewrite dwft_subst in H4; auto.
  apply* dtyping_size_rename.

  (* abs stack *)
  pick_fresh z.
  forwards ~ (L1 & ?): H0 z. Omega.omega. clear H0.
  exists L1. intros.
  apply_fresh dtyping_size_abs_stack as u; auto.
  forwards ~ : H1 y.
  rewrite map_push in H2.
  apply* dtyping_size_rename.

  (* app *)
  clear IHtypt1 IHtypt2.
  forwards ~ : IHm x typt1. Omega.omega.
  destruct H0 as (L1 & ?).
  forwards ~ : IHm x typt2. Omega.omega.
  destruct H1 as (L2 & ?).
  exists (L1 \u L2 \u dfv_tt_env E \u dfv_tt T1).
  intros.
  forwards~ : H0 y. clear H0.
  forwards~ : H1 y. clear H1.
  apply* dtyping_size_app.
  apply * dgen_subst_tvar.

  (* pair *)
  clear IHtypt1 IHtypt2.
  forwards ~ : IHm x typt1. Omega.omega.
  destruct H as (L1 & ?).
  forwards ~ : IHm x typt2. Omega.omega.
  destruct H0 as (L2 & ?).
  exists (L1 \u L2 \u dfv_tt_env E \u dfv_tt T1).
  intros.
  forwards~ : H y. clear H.
  forwards~ : H0 y. clear H0.
  apply* dtyping_size_pair.

  (* fst *)
  exists (\{}: vars). intro y; intros.
  apply dtyping_size_fst; autos*.
  apply~ dokt_subst_tt_empty.
  assert (dsubst_tt x (dtyp_fvar y) fst_typ = fst_typ).
    apply~ dsubst_tt_fst_typ.
  rewrite <- H2.
  apply~ dsub_subst_stack_var.

  (* snd *)
  exists (\{}: vars). intro y; intros.
  apply dtyping_size_snd; autos*.
  apply~ dokt_subst_tt_empty.
  assert (dsubst_tt x (dtyp_fvar y) snd_typ = snd_typ).
    apply~ dsubst_tt_snd_typ.
  rewrite <- H2.
  apply~ dsub_subst_stack_var.
Qed.

Lemma dtyping_size_subst_tvar : forall n E Q e T x,
    dtyping_size E Q e T n ->
    exists L, (forall y,
             y \notin L ->
             dtyping_size (map (dsubst_tt x (dtyp_fvar y)) E)
                          (dsubst_stack x (dtyp_fvar y) Q)
                          e
                          (dsubst_tt x (dtyp_fvar y) T)
                          n).
Proof.
  intros. apply* dtyping_size_subst_tvar_helper.
Qed.

Lemma dtyping_subst_tvar : forall E Q e T x,
    dtyping E Q e T ->
    exists L, (forall y,
             y \notin L ->
             dtyping (map (dsubst_tt x (dtyp_fvar y)) E)
                     (dsubst_stack x (dtyp_fvar y) Q)
                     e
                     (dsubst_tt x (dtyp_fvar y) T)
         ).
Proof.
  intros.
  forwards~ (n & ?) : dtyping_dtyping_size H.
  forwards~ (L & ?) : dtyping_size_subst_tvar x H0.
  exists L. intros.
  forwards ~ : H1 y.
  apply* dtyping_size_dtyping.
Qed.
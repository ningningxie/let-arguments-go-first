Require Import LibList LibFset LibLN DeclDef.
Require Import HM HMInfra.
Require Import DeclInfra.
Require Import DeclSound.
Require Import DEnvSub DTypingSize.

(* ********************************************************************** *)
(** * Desugar *)

Inductive h2d_desugar : htrm -> dtrm -> Prop :=
  | h2d_desugar_var : forall x,
      h2d_desugar (htrm_fvar x) (dtrm_fvar x)
  | h2d_desugar_nat : forall i,
      h2d_desugar (htrm_nat i) (dtrm_nat i)
  | h2d_desugar_abs : forall L e1 s1,
      (forall x, x \notin L ->
            h2d_desugar (e1 hopen_ee_var x) (s1 dopen_ee_var x)) ->
      h2d_desugar (htrm_abs e1) (dtrm_abs s1)
  | h2d_desugar_app : forall s1 s2 e1 e2,
      h2d_desugar e2 s2 ->
      h2d_desugar e1 s1 ->
      h2d_desugar (htrm_app e2 e1) (dtrm_app s2 s1)
  | h2d_desugar_let : forall L e1 e2 s1 s2,
      h2d_desugar e1 s1 ->
      (forall x, x \notin L ->
            h2d_desugar (e2 hopen_ee_var x) (s2 dopen_ee_var x)) ->
      h2d_desugar (htrm_let e1 e2) (dtrm_app (dtrm_abs s2) s1).

Hint Constructors htyping h2d_desugar.

Lemma dgen_exists: forall E t,
    dokt E ->
    dtype t ->
    exists s, dgen E t s.
Proof.
Admitted.

Lemma sub_empty: forall (E:vars),
    E \c \{} -> E = \{}.
Proof.
  intros.
  assert (\{} \c E). apply subset_empty_l.
  lets~ : fset_extens H H0.
Qed.

Lemma empty_remove: forall (E: vars),
      \{} \- E = \{}.
Proof.
  intros.
  apply~ sub_empty.
  unfold subset. intros.
  rewrite in_remove in H. destruct H.
  rewrite in_empty in H. false H.
Qed.
Hint Resolve empty_remove.

Lemma dgen_empty: forall E t t1,
    dgen E t t1 ->
    dfv_tt t1 \- dfv_tt_env E = \{}.
Proof.
  intros.
  inductions H; simpls~.
Qed.

Lemma empty_fewer_l: forall (U t1 t2:vars),
    (t1 \u t2) \- U = \{} ->
    t1 \- U = \{}.
Proof.
  intros.
  apply~ sub_empty. unfolds subset.
  intros.
  rewrite in_remove in H0. destruct H0.
  assert (x \in (t1 \u t2) \- U). rewrite in_remove. splits~.
  rewrite in_union. left~.
  rewrite H in H2. rewrite in_empty in H2. false H2.
Qed.

Lemma empty_fewer_r: forall (U t1 t2:vars),
    (t1 \u t2) \- U = \{} ->
    t2 \- U = \{}.
Proof.
  intros.
  apply~ sub_empty. unfolds subset.
  intros.
  rewrite in_remove in H0. destruct H0.
  assert (x \in (t1 \u t2) \- U). rewrite in_remove. splits~.
  rewrite in_union. right~.
  rewrite H in H2. rewrite in_empty in H2. false H2.
Qed.

Lemma empty_union: forall (U t1 t2:vars),
    t1 \- U = \{} ->
    t2 \- U = \{} ->
    (t1 \u t2) \- U = \{}.
Proof.
  intros.
  apply~ sub_empty. unfolds subset.
  intros.
  rewrite in_remove in H1. destruct H1.
  rewrite in_union in H1. destruct H1.
  assert (x \in t1 \- U). rewrite in_remove. splits~.
  rewrite H in H3. rewrite in_empty in H3. false H3.
  assert (x \in t2 \- U). rewrite in_remove. splits~.
  rewrite H0 in H3. rewrite in_empty in H3. false H3.
Qed.

Lemma empty_more_r: forall (U t1 t2:vars),
    t1 \- U = \{} ->
    t1 \- (t2 \u U) = \{}.
Proof.
  intros.
  apply~ sub_empty. unfolds subset.
  intros.
  rewrite in_remove in H0. destruct H0.
  assert (x \in t1 \- U). rewrite in_remove. splits~.
  rewrite H in H2. rewrite in_empty in H2. false H2.
Qed.

Lemma empty_more_l: forall (U t1 t2:vars),
    t1 \- U = \{} ->
    t1 \- (U \u t2) = \{}.
Proof.
  intros.
  apply~ sub_empty. unfolds subset.
  intros.
  rewrite in_remove in H0. destruct H0.
  assert (x \in t1 \- U). rewrite in_remove. splits~.
  rewrite H in H2. rewrite in_empty in H2. false H2.
Qed.

Hint Resolve empty_remove dgen_empty empty_fewer_l empty_more_r
     empty_fewer_r empty_more_r.

Lemma dtyping_size_weaken_helper : forall m F E e M N f T1 n,
  dtyping_size E M e (stack_type M f) n ->
  denv_sub F E ->
  stack_sub N M ->
  dgen E f T1 ->
  dfv_stack M \- dfv_tt_env E = \{} ->
  dfv_stack N \- dfv_tt_env F = \{} ->
  n < m ->
  exists f2 T2 , dtyping_size F N e (stack_type N f2) n /\
            dgen F f2 T2 /\
            dsub plain T2 T1.
Proof.
Admitted.

Lemma dtyping_size_weaken : forall F E e M N f T1 n,
  dtyping_size E M e (stack_type M f) n ->
  denv_sub F E ->
  stack_sub N M ->
  dgen E f T1 ->
  dfv_stack M \- dfv_tt_env E = \{} ->
  dfv_stack N \- dfv_tt_env F = \{} ->
  exists f2 T2 , dtyping_size F N e (stack_type N f2) n /\
            dgen F f2 T2 /\
            dsub plain T2 T1.
Proof.
  intros.
  apply* dtyping_size_weaken_helper.
Qed.

Lemma dtyping_weaken : forall  F E e M N f T1,
  dtyping E M e (stack_type M f) ->
  denv_sub F E ->
  stack_sub N M ->
  dgen E f T1 ->
  dfv_stack M \- dfv_tt_env E = \{} ->
  dfv_stack N \- dfv_tt_env F = \{} ->
  exists f2 T2 , dtyping F N e (stack_type N f2) /\
            dgen F f2 T2 /\
            dsub plain T2 T1.
Proof.
  intros.
  apply dtyping_dtyping_size in H. destruct H.
  forwards * : dtyping_size_weaken H.
  destruct H5 as (X1 & X2 & [? [? ?]]).
  exists X1 X2.
  splits~. apply* dtyping_size_dtyping.
Qed.

Hint Constructors stack_sub.
Hint Constructors dstack_type.

Lemma dtyping_weaken_more_stack : forall F E e M N f f' Q,
  dtyping E M e (stack_type M f) ->
  denv_sub F E ->
  stack_sub N M ->
  dsub plain f (stack_type Q f') ->
  dfv_stack M \- dfv_tt_env E = \{} ->
  dfv_stack (N ++ Q) \- dfv_tt_env F = \{} ->
  exists f2, dtyping F (N ++ Q) e (stack_type (N ++ Q) f2) /\
        dsub plain f2 f'.
Proof.
  introv ty sub_ef sub_mn sub_ff fvm fvn. gen F N Q f'.
  inductions ty; introv sub_ef sub_mn fvn sub_ff; simpls~.

  (* *)
  lets: denv_sub_binds H0 sub_ef.
  destruct H2 as (XT & [bd2 sub2]).
  lets: dsub_stack_to_plain H1.
  forwards ~ : stack_sub_contra f sub_mn.
  apply(proj21 (dsub_regular sub_ff)).
  lets ~ : stack_sub_co N sub_ff.
  apply (proj21 (stack_sub_regular sub_mn)).
  rewrite stack_type_app in H4.
  lets: sub_trans H2 sub2.
  lets: sub_trans H3 H5.
  lets: sub_trans H4 H6.
  lets: dsub_plain_to_stack H7.
  destruct H8 as (t3 & [sub_t3 sub_plain_t3]).
  exists~ t3. split~.
  apply* dtyping_var.
  apply (proj21 (denv_sub_regular sub_ef)).

  (* *)
  subst~.
  inversion sub_mn; subst~.
  destruct Q; simpls; try(solve[inversion sub_ff]).
  rewrite app_nil_r.
  exists~ (dtyp_nat). simpls~. split~. apply* dtyping_nat.
  lets ~ [? ?]: denv_sub_regular sub_ef.

  (* *)
  subst.
  inversion sub_mn; subst.
  rewrite app_nil_l.
  pick_fresh x.
  destruct Q.
    simpls.
    assert (dsub plain T1 (stack_type nil T1)). simpls~. apply~ dsub_refl.
      lets [? _]: dsub_regular sub_ff.
      inversion~ H2.
    forwards ~ : H1 x (F & x ~ V) (nil: dstack) H2. apply~ denv_sub_add.
      apply~ dsub_refl. forwards ~ : H0 x.
      lets [? _]: dtyping_regular H3.
      apply* dtype_from_okt_typ.
    rewrite app_nil_r in H3.
    destruct H3 as (f2 & [typing_f2 sub_f2]).
    exists~ (dtyp_arrow V f2). simpls~. split~.
    apply_fresh dtyping_absann as y; auto.
    apply* dtyping_rename.
    assert (dsub plain (dtyp_arrow V f2) (dtyp_arrow V T1)). apply~ dsub_arrow. apply~ dsub_refl.
      lets [? _]: dsub_regular sub_ff.
      inversion~ H3.
      apply* sub_trans.

    simpls.
    inversion sub_ff; subst.
    forwards ~ : H1 x (F & x ~ V) (nil: dstack) H7. apply~ denv_sub_add.
      apply~ dsub_refl. forwards ~ : H0 x.
      lets [? _]: dtyping_regular H2.
      apply* dtype_from_okt_typ.
      rewrite app_nil_l. rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvn. auto.
    rewrite app_nil_l in H2.
    destruct H2 as (f2 & [typing_f2 sub_f2]).
    exists~ f2. simpls~. split~.
    apply_fresh dtyping_absann_stack as y; auto.
    apply* dtyping_rename.

  (* *)
  subst.
  inversion sub_mn; subst.
  rewrite app_nil_l.
  pick_fresh x.
  destruct Q.
    simpls.
    assert (dsub plain T1 (stack_type nil T1)). simpls~. apply~ dsub_refl.
      lets [? _]: dsub_regular sub_ff.
      inversion~ H3.
    forwards ~ : H2 x (F & x ~ t) (nil: dstack) H3.
    rewrite app_nil_r in H4.
    destruct H4 as (f2 & [typing_f2 sub_f2]).
    exists~ (dtyp_arrow t f2). simpls~. split~.
    apply_fresh dtyping_abs as y; auto.
    apply* dtyping_rename.
    assert (dsub plain (dtyp_arrow t f2) (dtyp_arrow t T1)). apply~ dsub_arrow.
    apply* sub_trans.

    simpls.
    inversion sub_ff; subst.
    forwards ~ : H2 x (F & x ~ d) (nil: dstack) H8.
      rewrite app_nil_l. rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvn. auto.
    rewrite app_nil_l in H3.
    destruct H3 as (f2 & [typing_f2 sub_f2]).
    exists~ f2. simpls~. split~.
    apply_fresh dtyping_abs_stack as y; auto.
    apply* dtyping_rename.

  (* *)
  inversion sub_mn; subst.
  rewrite app_cons. rewrite app_cons in fvn. simpls.
  pick_fresh y.
  forwards ~ : H2 y (F & y ~ V) cs1 sub_ff.
      rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvm. auto.
    apply ~ denv_sub_add.
    apply~ dsub_refl.
    apply~ (proj22 (dsub_regular H0)).
      rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvn. auto.
  destruct H3 as (f2 & [typing_f2 sub_f2]).
  exists~ f2. split~.
  apply_fresh dtyping_absann_stack as z; auto.
    apply* sub_trans.
  apply* dtyping_rename.

  (* *)
  inversion sub_mn; subst.
  rewrite app_cons in *. simpls.
  pick_fresh y.
  forwards ~ : H0 y (F & y ~ x) cs1 sub_ff.
      rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvm. auto.
      rewrite dfv_tt_env_push_inv.
      apply empty_more_r.
      apply empty_fewer_r in fvn. auto.
  destruct H1 as (f2 & [typing_f2 sub_f2]).
  exists~ f2. split~.
  apply_fresh dtyping_abs_stack as z; auto.
  apply* dtyping_rename.

  (* app *)
  forwards ~ : IHty1 sub_ef (nil:dstack) (nil:dstack) T1.
    simpls~. apply~ dsub_refl.
    apply (proj33 (dtyping_regular ty1)).
  destruct H0 as (f2 & [typing_e2 sub_f2]). clear IHty1.
  rewrite app_nil_r in typing_e2. simpls~.
  assert (T1 = stack_type nil T1) by simpls~.
  rewrite H0 in ty1.
  assert (stack_sub nil nil). apply~ stack_sub_relf.
  lets: dtyping_weaken ty1 sub_ef H1 H.
  destruct H2 as (f22 & f2' & [ty [gen sub_gen]]). unfolds~ dfv_stack. simpls~.
  assert (I1: stack_sub (f2' :: N) (T3 :: cs)).
    apply~ stack_sub_add.
  forwards ~ : IHty2 sub_ef I1 sub_ff.
    apply~ empty_union. apply* dgen_empty.
    apply~ empty_union. apply* dgen_empty.
  destruct H2 as (f22' &  [tyf22 subf22]).
  rewrite app_cons in tyf22. simpls~.
  exists f22'. split~.
  apply* dtyping_app.

  (* pair *)
  inversions~ sub_mn.
  destruct Q; simpls; try(solve[inversion sub_ff]).
  forwards ~ (f01 & [? ?]) : IHty1 sub_ef (nil:dstack) (nil:dstack) T1.
    simpls~. apply dsub_refl.
    lets~ [_ [_ [? _]]] : dtyping_regular ty1.
  simpls~.
  forwards ~ (f02 & [? ?]) : IHty2 sub_ef (nil:dstack) (nil:dstack) T2.
    simpls~. apply dsub_refl.
    lets~ [_ [_ [? _]]] : dtyping_regular ty2.
  simpls~.
  exists~ (dtyp_pair f01 f02).
  rewrite app_nil_r in *.
  splits~.
  apply sub_trans with (dtyp_pair T1 T2); auto.

  (* fst *)
  lets: dsub_stack_to_plain H0.
  forwards ~ : stack_sub_contra f sub_mn.
    apply(proj21 (dsub_regular sub_ff)).
  lets ~ : stack_sub_co N sub_ff.
    apply (proj21 (stack_sub_regular sub_mn)).
  rewrite stack_type_app in H3.
  lets: sub_trans H2 H1.
  lets: sub_trans H3 H4.
  lets (t3 & [? ?]): dsub_plain_to_stack H5.
  exists~ t3. splits~.
  apply* dtyping_fst.
    apply (proj21 (denv_sub_regular sub_ef)).

  (* snd *)
  lets: dsub_stack_to_plain H0.
  forwards ~ : stack_sub_contra f sub_mn.
    apply(proj21 (dsub_regular sub_ff)).
  lets ~ : stack_sub_co N sub_ff.
    apply (proj21 (stack_sub_regular sub_mn)).
  rewrite stack_type_app in H3.
  lets: sub_trans H2 H1.
  lets: sub_trans H3 H4.
  lets (t3 & [? ?]): dsub_plain_to_stack H5.
  exists~ t3. splits~.
  apply* dtyping_snd.
    apply (proj21 (denv_sub_regular sub_ef)).
Qed.

Lemma dtyping_inf_to_weaker_chk : forall E e f f' Q,
  dtyping E nil e f ->
  dsub plain f (stack_type Q f') ->
  dfv_stack  Q \- dfv_tt_env E = \{} ->
  exists f2, dtyping E Q e (stack_type Q f2) /\
        dsub plain f2 f'.
Proof.
  introv ty dsub fvq.
  assert (I1: dtyping E nil e (stack_type nil f)). simpls~.
  assert (I2: denv_sub E E). apply* denv_sub_self.
    apply (proj31 (dtyping_regular ty)).
  assert (I3: stack_sub nil nil) by auto.
  forwards~ : dtyping_weaken_more_stack I1 I2 I3 dsub.
Qed.

Lemma hinst_sub: forall t1 t2,
    hinst t1 t2 ->
    dsub plain t1 t2.
Proof.
  introv ins. inductions ins; simpls~.
  apply dsub_alll1 with (dtyp_fvar x); auto.
Qed.

Lemma hm_preserve: forall E e t s,
    htyping E e t ->
    h2d_desugar e s ->
    exists T, dtyping E nil s T /\ dsub plain T t.
Proof.
  introv ty. gen s.
  induction ty; introv de; simpls~.

  inversion de; subst.
  exists T. split~. apply* dtyping_var. apply* hinst_sub.

  inversion de; subst.
  exists~ htyp_nat.

  inversion de; subst.
  pick_fresh y.
  forwards ~ : H4 y. clear H4.
  forwards ~ : H1 y. clear H1.
  forwards ~ : H2 y H3.
  destruct H1 as (T & [ty sub]).
  exists (dtyp_arrow t T).
  split~.
  apply_fresh dtyping_abs as x; auto.
  forwards ~ : dtyping_rename y x ty.

  inversion de; subst.
  forwards~ (t1 & hty1): IHty1 H3. clear IHty1.
  forwards~ (t2 & hty2) : IHty2 H1. clear IHty2.
  clear de ty1 H3.
  destruct hty1. destruct hty2.
  lets (t1' & t1gen): dgen_exists E t1; auto.
    apply (proj31 (dtyping_regular H)).
    apply (proj33 (dtyping_regular H)).
  lets: dgen_sub t1gen.
  lets: sub_trans H0 H4.
  assert (dsub plain (dtyp_arrow T1 T2) (dtyp_arrow t1' T2)).
    apply~ dsub_arrow. apply~ dsub_refl.
    lets [_ ?]: dsub_regular H3. inversion~ H6. inversions~ H7.
  assert (dsub plain t2 (dtyp_arrow t1' T2)).
    apply* sub_trans.
  assert (dtyp_arrow t1' T2 = stack_type (t1'::nil) T2). simpls~.
  rewrite H8 in H7.
  lets: dtyping_inf_to_weaker_chk H2 H7.
  destruct H9 as (f2 & [tyf2 subf2]). simpls.
    rewrite union_empty_r. apply* dgen_empty.
  simpls.
  exists f2. split~.
  apply* dtyping_app.

  (* *)
  inversion de; subst.
  forwards~ (t1 & hty1): IHty H4. clear IHty.
  pick_fresh x.
  forwards~ (t2 & hty2) : H1 x (H6 x). clear H1.
  clear de H4 H0 H6.
  destruct hty1. destruct hty2.
  lets (t1' & t1gen): dgen_exists E t1; auto.
    apply (proj31 (dtyping_regular H0)).
    apply (proj33 (dtyping_regular H0)).
  lets: dgen_sub_preserve H1 t1gen H.

  assert (I1: dtyping (E & x ~ T3) nil (s2 dopen_ee_var x) (stack_type nil t2)) by simpls~.
  assert (I2: denv_sub (E & x ~ t1') (E & x ~ T3)).
    apply~ denv_sub_add. apply~ denv_sub_self.
  assert (I3: stack_sub nil nil) by auto.
  assert (I4: dsub plain t2 (stack_type nil t2)).
    simpls~. apply~ dsub_refl.
    apply (proj21 (dsub_regular H3)).
  lets: dtyping_weaken_more_stack I1 I2 I3 I4.
  destruct H5 as (f2 & [tyf2 subf2]).
  unfolds dfv_stack. simpls~.
  simpls~.
  exists f2.
  split~.
  apply* dtyping_app.
  apply_fresh dtyping_abs_stack as z; auto.
  rewrite app_nil_r in tyf2.
  apply* dtyping_rename.
  apply* sub_trans.
Qed.
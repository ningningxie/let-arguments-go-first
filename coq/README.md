This repository is supposed to be built with Coq 8.5pl3.

It depends on the [TLC](http://www.chargueraud.org/softs/tlc/) Locally Nameless
Coq Library, which provides tactics for formalizing a type system. All files for
TLC is included in [tlc/](./tlc)

- [generalization-version](./generalization-version): First application.
- [system-f](./system-f): Second application
- [tlc](./tlc): locally nameless library



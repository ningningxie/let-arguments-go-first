Set Implicit Arguments.
Require Import LibList LibLN DeclDef.

(* ********************************************************************** *)
(** * Additional Definitions Used in the Proofs *)

(** Computing free type variables in a type *)

Fixpoint fv_tt (T : typ) {struct T} : vars :=
  match T with
  | typ_nat         => \{}
  | typ_bvar J      => \{}
  | typ_fvar X      => \{X}
  | typ_arrow T1 T2 => (fv_tt T1) \u (fv_tt T2)
  | typ_all T2      => (fv_tt T2)
  end.

(** Computing free type variables in a term *)

Fixpoint fv_te (e : trm) {struct e} : vars :=
  match e with
  | trm_nat i        => \{}
  | trm_bvar i       => \{}
  | trm_fvar x       => \{}
  | trm_abs e1       => (fv_te e1)
  | trm_absann V e1  => (fv_tt V) \u (fv_te e1)
  | trm_app e1 e2    => (fv_te e1) \u (fv_te e2)
  | trm_tabs e1      => (fv_te e1)
  | trm_tapp e1 V    => (fv_tt V) \u (fv_te e1)
  end.

(** Computing free term variables in a term *)

Fixpoint fv_ee (e : trm) {struct e} : vars :=
  match e with
  | trm_nat i        => \{}
  | trm_bvar i       => \{}
  | trm_fvar x       => \{x}
  | trm_abs e1       => (fv_ee e1)
  | trm_absann V e1  => (fv_ee e1)
  | trm_app e1 e2    => (fv_ee e1) \u (fv_ee e2)
  | trm_tabs e1      => (fv_ee e1)
  | trm_tapp e1 V    => (fv_ee e1)
  end.

(** Substitution for free type variables in terms. *)

Fixpoint subst_te (Z : var) (U : typ) (e : trm) {struct e} : trm :=
  match e with
  | trm_nat i        => trm_nat i
  | trm_bvar i       => trm_bvar i
  | trm_fvar x       => trm_fvar x
  | trm_abs e1       => trm_abs  (subst_te Z U e1)
  | trm_absann V e1  => trm_absann  (subst_tt Z U V)  (subst_te Z U e1)
  | trm_app e1 e2    => trm_app  (subst_te Z U e1) (subst_te Z U e2)
  | trm_tabs e1      => trm_tabs (subst_te Z U e1)
  | trm_tapp e1 V    => trm_tapp (subst_te Z U e1) (subst_tt Z U V)
  end.

(** Substitution for free term variables in terms. *)

Fixpoint subst_ee (z : var) (u : trm) (e : trm) {struct e} : trm :=
  match e with
  | trm_nat i        => trm_nat i
  | trm_bvar i       => trm_bvar i
  | trm_fvar x       => If x = z then u else (trm_fvar x)
  | trm_abs e1       => trm_abs (subst_ee z u e1)
  | trm_absann V e1  => trm_absann V (subst_ee z u e1)
  | trm_app e1 e2    => trm_app (subst_ee z u e1) (subst_ee z u e2)
  | trm_tabs e1      => trm_tabs (subst_ee z u e1)
  | trm_tapp e1 V    => trm_tapp (subst_ee z u e1) V
  end.

(** Substitution for free type variables in environment. *)

Definition subst_tb (Z : var) (P : typ) (b : bind) : bind :=
  match b with
  | bind_tvar  => bind_tvar
  | bind_def T => bind_def (subst_tt Z P T)
  | bind_typ T => bind_typ (subst_tt Z P T)
  end.

(** Substitution for free type variables in environment. *)

Definition subst_ti (Z : var) (P : typ) (b : item) : item :=
  match b with
  | item_typ T => item_typ (subst_tt Z P T)
  | item_def T => item_def (subst_tt Z P T)
  end.

(* ********************************************************************** *)
(** * Tactics *)

(** Constructors as hints. *)

Hint Constructors type term wft wfa ok okt value red.

Hint Resolve
  sub_empty sub_tapp sub_app
  typing_var typing_app typing_tapp.

(** Gathering free names already used in the proofs *)

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : trm => fv_te x) in
  let D := gather_vars_with (fun x : trm => fv_ee x) in
  let E := gather_vars_with (fun x : typ => fv_tt x) in
  let F := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D \u E \u F).

(** "pick_fresh x" tactic create a fresh variable with name x *)

Ltac pick_fresh x :=
  let L := gather_vars in (pick_fresh_gen L x).

(** "apply_fresh T as x" is used to apply inductive rule which
   use an universal quantification over a cofinite set *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

(** These tactics help applying a lemma which conclusion mentions
  an environment (E & F) in the particular case when F is empty *)

Ltac get_env :=
  match goal with
  | |- wft ?E _ => E
  | |- wfa ?E _ => E
  | |- sub ?E _ _ _  => E
  | |- typing ?E _ _ _ => E
  | |- apply_env ?E _ => E
  end.

Tactic Notation "apply_empty_bis" tactic(get_env) constr(lemma) :=
  let E := get_env in rewrite <- (concat_empty_r E);
  eapply lemma; try rewrite concat_empty_r.

Tactic Notation "apply_empty" constr(F) :=
  apply_empty_bis (get_env) F.

Tactic Notation "apply_empty" "*" constr(F) :=
  apply_empty F; autos*.

(** Tactic to undo when Coq does too much simplification *)

Ltac unsimpl_map_bind :=
  match goal with |- context [ ?B (subst_tt ?Z ?P ?U) ] =>
    unsimpl ((subst_tb Z P) (B U)) end.

Tactic Notation "unsimpl_map_bind" "*" :=
  unsimpl_map_bind; autos*.

(* ********************************************************************** *)
(** * Properties of Substitutions *)

(* ********************************************************************** *)
(** ** Properties of type substitution in type *)

(** Substitution on indices is identity on well-formed terms. *)

Lemma open_tt_rec_type_core : forall T j V U i, i <> j ->
  (open_tt_rec j V T) = open_tt_rec i U (open_tt_rec j V T) ->
  T = open_tt_rec i U T.
Proof.
  induction T; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma open_tt_var_fresh : forall A B x,
    x \notin fv_tt A \u fv_tt B ->
    A open_tt_var x = B open_tt_var x ->
    A = B.
Proof.
   intro A; unfolds open_tt; generalize 0;
     inductions A;
     introv H H1; unfolds open_tt; destruct B; simpls*;
       try solve [inversion H1 | case_nat*].
  case_nat*. case_nat*.
  case_nat*. case_nat*. inversions H1. false* notin_same.
  case_nat*. inversions H1. false* notin_same.
  inversions H1. f_equal*.
  inversions H1. f_equal*.
Qed.

Lemma open_tt_rec_type : forall T U,
  type T -> forall k, T = open_tt_rec k U T.
Proof.
  induction 1; intros; simpl; f_equal*. unfolds open_tt.
  pick_fresh X. apply* (@open_tt_rec_type_core T 0 (typ_fvar X)).
Qed.

Lemma open_tt_rec_type_commu : forall e j v u i, i <> j ->
  type v -> type u ->
  open_tt_rec j v (open_tt_rec i u e) = open_tt_rec i u (open_tt_rec j v e).
Proof.
  induction e; introv Neq tv tu; simpl in *; auto.
  case_nat*. case_nat*. simpls. case_nat*.
  symmetry. apply~ open_tt_rec_type.
  case_nat*.
  simpls. case_nat*.
  apply~ open_tt_rec_type.
  simpls. case_nat*. case_nat*.
  f_equal*. f_equal*.
Qed.

(** Substitution for a fresh name is identity. *)

Lemma subst_tt_fresh : forall Z U T,
  Z \notin fv_tt T -> subst_tt Z U T = T.
Proof.
  induction T; simpl; intros; f_equal*.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma subst_tt_open_tt_rec : forall T1 T2 X P n, type P ->
  subst_tt X P (open_tt_rec n T2 T1) =
  open_tt_rec n (subst_tt X P T2) (subst_tt X P T1).
Proof.
  introv WP. generalize n.
  induction T1; intros k; simpls; f_equal*.
  case_nat*.
  case_var*. rewrite* <- open_tt_rec_type.
Qed.

Lemma subst_tt_open_tt : forall T1 T2 X P, type P ->
  subst_tt X P (open_tt T1 T2) =
  open_tt (subst_tt X P T1) (subst_tt X P T2).
Proof.
  unfold open_tt. autos* subst_tt_open_tt_rec.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma subst_tt_open_tt_var : forall X Y U T, Y <> X -> type U ->
  (subst_tt X U T) open_tt_var Y = subst_tt X U (T open_tt_var Y).
Proof.
  introv Neq Wu. rewrite* subst_tt_open_tt.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma subst_tt_intro : forall X T2 U,
  X \notin fv_tt T2 -> type U ->
  open_tt T2 U = subst_tt X U (T2 open_tt_var X).
Proof.
  introv Fr Wu. rewrite* subst_tt_open_tt.
  rewrite* subst_tt_fresh. simpl. case_var*.
Qed.

Lemma notin_subst_tt: forall t A x y,
    x \notin fv_tt t ->
    x \notin fv_tt A ->
    x \notin fv_tt (subst_tt y t A).
Proof.
  inductions A; intros; simpls*.
  case_var*.
Qed.

Lemma notin_subst_tt_self : forall t A x,
    x \notin fv_tt t ->
    x \notin fv_tt (subst_tt x t A).
Proof.
  inductions A; intros; simpls*.
  case_var*. simpls*.
Qed.

Lemma notin_subst_tt_inv: forall x y U t,
    x \notin fv_tt (subst_tt y U t) ->
    (x \notin fv_tt t /\ (x \notin fv_tt U \/ y \notin fv_tt t)) \/
    (x \in fv_tt t /\ x = y /\ x \notin fv_tt U).
Proof.
  introv xin. induction t; simpls~.
  case_var~.
  tests: (x = y).
  right. splits~. apply in_singleton_self.
  left. splits~.

  rewrite notin_union in xin.
  destruct xin.
  forwards ~ : IHt1. clear IHt1.
  forwards ~ : IHt2. clear IHt2.
  destruct H1 as [ [? [? | ?]] | [? [? ?]]].
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  left. splits~.
  left. splits~.
  subst~.
  right. splits~. rewrite in_union. right~.
  subst~.
  destruct H2 as [ [? [? | ?]] | [? [? ?]]].
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.
  right~. splits~. rewrite in_union. left~.
Qed.

Lemma subst_tt_commu: forall  x u x0 d B,
    x0 <> x ->
    x0 \notin fv_tt u ->
    (subst_tt x u (subst_tt x0 d B)) =
    (subst_tt x0 (subst_tt x u d) (subst_tt x u B)).
Proof.
  induction B; introv neq notin; simpls~; try(solve[f_equal~]).
  case_var~.
  case_var~. simpls~. case_var~.
  case_var~. simpls~. case_var~.
  rewrite~ subst_tt_fresh.
  simpls~. case_var~. case_var~.
Qed.

(* ********************************************************************** *)
(** ** Properties of type substitution in terms *)

Lemma open_te_rec_term_core : forall e j u i P ,
  open_ee_rec j u e = open_te_rec i P (open_ee_rec j u e) ->
  e = open_te_rec i P e.
Proof.
  induction e; intros; simpl in *; inversion H; f_equal*; f_equal*.
Qed.

Lemma open_te_rec_type_core : forall e j Q i P, i <> j ->
  open_te_rec j Q e = open_te_rec i P (open_te_rec j Q e) ->
   e = open_te_rec i P e.
Proof.
  induction e; intros; simpl in *; inversion H0; f_equal*;
  match goal with H: ?i <> ?j |- ?t = open_tt_rec ?i _ ?t =>
   apply* (@open_tt_rec_type_core t j) end.
Qed.

Lemma open_te_rec_term : forall e U,
  term e -> forall k, e = open_te_rec k U e.
Proof.
  intros e U WF. induction WF; intros; simpl;
    f_equal*; try solve [ apply* open_tt_rec_type ].
  unfolds open_ee. pick_fresh x.
   apply* (@open_te_rec_term_core e1 0 (trm_fvar x)).
  unfolds open_te. pick_fresh X.
   apply* (@open_te_rec_term_core e1 0 (trm_fvar X)).
  unfolds open_te. pick_fresh X.
   apply* (@open_te_rec_type_core e1 0 (typ_fvar X)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma subst_te_fresh : forall X U e,
  X \notin fv_te e -> subst_te X U e = e.
Proof.
  induction e; simpl; intros; f_equal*; autos* subst_tt_fresh.
Qed.

(** Substitution distributes on the open operation. *)

Lemma subst_te_open_te : forall e T X U, type U ->
  subst_te X U (open_te e T) =
  open_te (subst_te X U e) (subst_tt X U T).
Proof.
  intros. unfold open_te. generalize 0.
  induction e; intros; simpls; f_equal*;
  autos* subst_tt_open_tt_rec.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma subst_te_open_te_var : forall X Y U e, Y <> X -> type U ->
  (subst_te X U e) open_te_var Y = subst_te X U (e open_te_var Y).
Proof.
  introv Neq Wu. rewrite* subst_te_open_te.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma subst_te_intro : forall X U e,
  X \notin fv_te e -> type U ->
  open_te e U = subst_te X U (e open_te_var X).
Proof.
  introv Fr Wu. rewrite* subst_te_open_te.
  rewrite* subst_te_fresh. simpl. case_var*.
Qed.


(* ********************************************************************** *)
(** ** Properties of term substitution in terms *)

Lemma open_ee_rec_term_core : forall e j v u i, i <> j ->
  open_ee_rec j v e = open_ee_rec i u (open_ee_rec j v e) ->
  e = open_ee_rec i u e.
Proof.
  induction e; introv Neq H; simpl in *; inversion H; f_equal*.
  case_nat*. case_nat*.
Qed.

Lemma open_ee_rec_type_core : forall e j V u i,
  open_te_rec j V e = open_ee_rec i u (open_te_rec j V e) ->
  e = open_ee_rec i u e.
Proof.
  induction e; introv H; simpls; inversion H; f_equal*.
Qed.

Lemma open_ee_rec_term : forall u e,
  term e -> forall k, e = open_ee_rec k u e.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open_ee. pick_fresh x.
   apply* (@open_ee_rec_term_core e1 0 (trm_fvar x)).
  unfolds open_ee. pick_fresh x.
   apply* (@open_ee_rec_term_core e1 0 (trm_fvar x)).
  unfolds open_te. pick_fresh X.
   apply* (@open_ee_rec_type_core e1 0 (typ_fvar X)).
Qed.

(** Substitution for a fresh name is identity. *)

Lemma subst_ee_fresh : forall x u e,
  x \notin fv_ee e -> subst_ee x u e = e.
Proof.
  induction e; simpl; intros; f_equal*.
  case_var*.
Qed.

(** Substitution distributes on the open operation. *)

Lemma subst_ee_open_ee : forall t1 t2 u x, term u ->
  subst_ee x u (open_ee t1 t2) =
  open_ee (subst_ee x u t1) (subst_ee x u t2).
Proof.
  intros. unfold open_ee. generalize 0.
  induction t1; intros; simpls; f_equal*.
  case_nat*.
  case_var*. rewrite* <- open_ee_rec_term.
Qed.

(** Substitution and open_var for distinct names commute. *)

Lemma subst_ee_open_ee_var : forall x y u e, y <> x -> term u ->
  (subst_ee x u e) open_ee_var y = subst_ee x u (e open_ee_var y).
Proof.
  introv Neq Wu. rewrite* subst_ee_open_ee.
  simpl. case_var*.
Qed.

(** Opening up a body t with a type u is the same as opening
  up the abstraction with a fresh name x and then substituting u for x. *)

Lemma subst_ee_intro : forall x u e,
  x \notin fv_ee e -> term u ->
  open_ee e u = subst_ee x u (e open_ee_var x).
Proof.
  introv Fr Wu. rewrite* subst_ee_open_ee.
  rewrite* subst_ee_fresh. simpl. case_var*.
Qed.

(** Interactions between type substitutions in terms and opening
  with term variables in terms. *)

Lemma subst_te_open_ee_var : forall Z P x e,
  (subst_te Z P e) open_ee_var x = subst_te Z P (e open_ee_var x).
Proof.
  introv. unfold open_ee. generalize 0.
  induction e; intros; simpl; f_equal*. case_nat*.
Qed.

(** Interactions between term substitutions in terms and opening
  with type variables in terms. *)

Lemma subst_ee_open_te_var : forall z u e X, term u ->
  (subst_ee z u e) open_te_var X = subst_ee z u (e open_te_var X).
Proof.
  introv. unfold open_te. generalize 0.
  induction e; intros; simpl; f_equal*.
  case_var*. symmetry. autos* open_te_rec_term.
Qed.

(** Substitutions preserve local closure. *)

Lemma subst_tt_type : forall T Z P,
  type T -> type P -> type (subst_tt Z P T).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* type_all as X. rewrite* subst_tt_open_tt_var.
Qed.

Lemma subst_tt_type_inv: forall T Z P,
  type P -> type (subst_tt Z P T) ->
  type T.
Proof.
  introv H dm. inductions dm; simpls~.

  destruct T; simpls; try(solve[inversions x]).
  auto. case_var~.

  destruct T; simpls; try(solve[inversions x]).
  case_var~.

  destruct T; simpls; try(solve[inversions x]).
  case_var~. inversions* x.

  destruct T; simpls; try(solve[inversions x]).
  case_var~.
  inversions x.
  apply_fresh type_all as y.
  apply H0 with y P Z; autos*.
  rewrite* subst_tt_open_tt_var.
Qed.

Lemma subst_te_term : forall e Z P,
  term e -> type P -> term (subst_te Z P e).
Proof.
  lets: subst_tt_type. induction 1; intros; simpl; auto.
  apply_fresh* term_abs as x. rewrite* subst_te_open_ee_var.
  apply_fresh* term_absann as x. rewrite* subst_te_open_ee_var.
  apply_fresh* term_tabs as x. rewrite* subst_te_open_te_var.
Qed.

Lemma subst_ee_term : forall e1 Z e2,
  term e1 -> term e2 -> term (subst_ee Z e2 e1).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* term_abs as y. rewrite* subst_ee_open_ee_var.
  apply_fresh* term_absann as y. rewrite* subst_ee_open_ee_var.
  apply_fresh* term_tabs as Y. rewrite* subst_ee_open_te_var.
Qed.

Hint Resolve subst_tt_type subst_te_term subst_ee_term.


(* ********************************************************************** *)
(** * Properties of well-formedness of a type in an environment *)

(** If a type is well-formed in an environment then it is locally closed. *)

Lemma wft_type : forall E T,
  wft E T -> type T.
Proof.
  induction 1; eauto.
Qed.

(** Through weakening *)

Lemma wft_weaken : forall G T E F,
  wft (E & G) T ->
  ok (E & F & G) ->
  wft (E & F & G) T.
Proof.
  intros. gen_eq K: (E & G). gen E F G.
  induction H; intros; subst; eauto.
  (* case: var *)
  apply (@wft_tvar). apply* binds_weaken.
  (* case: all *)
  apply_fresh* wft_all as Y. apply_ih_bind* H0.
Qed.

(** Through strengthening *)

Lemma wft_strengthen : forall E F x U T,
 wft (E & x ~: U & F) T -> wft (E & F) T.
Proof.
  intros. gen_eq G: (E & x ~: U & F). gen F.
  induction H; intros F EQ; subst; auto.
  apply* (@wft_tvar).
  destruct (binds_concat_inv H) as [?|[? ?]].
    apply~ binds_concat_right.
    destruct (binds_push_inv H1) as [[? ?]|[? ?]].
      subst. false.
      apply~ binds_concat_left.
  (* todo: binds_cases tactic *)
  apply_fresh* wft_all as Y. apply_ih_bind* H0.
Qed.

Lemma wft_strengthen_def : forall E F x U T,
 wft (E & x ~= U & F) T -> wft (E & F) T.
Proof.
  intros. gen_eq G: (E & x ~= U & F). gen F.
  induction H; intros F EQ; subst; auto.
  apply* (@wft_tvar).
  destruct (binds_concat_inv H) as [?|[? ?]].
    apply~ binds_concat_right.
    destruct (binds_push_inv H1) as [[? ?]|[? ?]].
      subst. false.
      apply~ binds_concat_left.
  (* todo: binds_cases tactic *)
  apply_fresh* wft_all as Y. apply_ih_bind* H0.
Qed.

(** Through type substitution *)

Lemma wft_subst_tb : forall F E Z P T,
  wft (E & Z ~tvar & F) T ->
  wft E P ->
  ok (E & map (subst_tb Z P) F) ->
  wft (E & map (subst_tb Z P) F) (subst_tt Z P T).
Proof.
  introv WT WP. gen_eq G: (E & Z ~tvar & F). gen F.
  induction WT; intros F EQ Ok; subst; simpl subst_tt; auto.
  case_var*.
    apply_empty* wft_weaken.
    destruct (binds_concat_inv H) as [?|[? ?]].
      apply (@wft_tvar ).
       apply~ binds_concat_right.
       unsimpl (subst_tb Z P bind_tvar).
       apply~ binds_map.
      destruct (binds_push_inv H1) as [[? ?]|[? ?]].
        subst. false~.
        applys wft_tvar. apply* binds_concat_left.
  apply_fresh* wft_all as Y.
   unsimpl ((subst_tb Z P) bind_tvar).
   lets: wft_type.
   rewrite* subst_tt_open_tt_var.
   apply_ih_map_bind* H0.
Qed.

(** Through type reduction *)

Lemma wft_open : forall E U T2,
  ok E ->
  wft E (typ_all T2) ->
  wft E U ->
  wft E (open_tt T2 U).
Proof.
  introv Ok WA WU. inversions WA. pick_fresh X.
  autos* wft_type. rewrite* (@subst_tt_intro X).
  lets K: (@wft_subst_tb empty).
  specializes_vars K. clean_empty K. apply* K.
Qed.

Lemma wft_open_subst: forall E T U P,
   ok E ->
   wft E U ->
   wft E P ->
   wft E (open_tt T U) ->
   wft E (open_tt T P).
Proof.
  unfolds open_tt. generalize 0.
  introv oke wtu wtp wtsu.
  gen_eq M:(open_tt_rec n U T) .
  gen U P n T.
  induction wtsu; introv wtu wtp minfo.

  destruct T; simpls~; try(solve[inversion minfo]). case_nat*.
  destruct T; simpls~; try(solve[inversion minfo]). case_nat*.
    inversions minfo. apply* wft_tvar.
  destruct T; simpls~; try(solve[inversion minfo]). case_nat*.
    inversion minfo; subst. clear minfo. constructor.
    apply IHwtsu1 with U; auto. apply IHwtsu2 with U; auto.
  destruct T0; simpls~; try(solve[inversion minfo]). case_nat*.
    inversion minfo; subst. clear minfo.
    apply_fresh wft_all as x.
    forwards ~ : H0 x U P.
       apply_empty* wft_weaken. apply_empty* wft_weaken.
       rewrite* open_tt_rec_type_commu.
       apply* wft_type.
       rewrite open_tt_rec_type_commu in H1; auto.
       apply* wft_type.
Qed.

(* ********************************************************************** *)
(** * Relations between well-formed environment and types well-formed
  in environments *)

(** If an environment is well-formed, then it does not contain duplicated keys. *)

Lemma ok_from_okt : forall E,
  okt E -> ok E.
Proof.
  induction 1; auto.
Qed.

Lemma ok_map_inv: forall (E F: env) f,
  ok (E & map f F) ->
  ok (E & F).
Proof.
  introv okt. induction F using env_ind.
  rewrite map_empty in okt; auto.
  rewrite map_push in okt.
  rewrite concat_assoc in *. constructor~.
  lets* [? ?]  : ok_push_inv okt.
Qed.

Hint Extern 1 (ok _) => apply ok_from_okt.

(** Extraction from a subtyping assumption in a well-formed environments *)

Lemma wft_from_env_has_def : forall x U E,
  okt E -> binds x (bind_def U) E -> wft E U.
Proof.
  induction E using env_ind; intros Ok B.
  false* binds_empty_inv.
  inversions Ok.
    false (empty_push_inv H0).
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H2.
       apply_empty* wft_weaken.
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H3.
       apply_empty* wft_weaken.
       apply_empty* wft_weaken.
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H3.
       apply_empty* wft_weaken.
Qed.

(** Extraction from a typing assumption in a well-formed environments *)

Lemma wft_from_env_has_typ : forall x U E,
  okt E -> binds x (bind_typ U) E -> wft E U.
Proof.
  induction E using env_ind; intros Ok B.
  false* binds_empty_inv.
  inversions Ok.
    false (empty_push_inv H0).
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H2.
       apply_empty* wft_weaken.
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H3. apply_empty* wft_weaken.
    destruct (eq_push_inv H) as [? [? ?]]. subst. clear H.
     destruct (binds_push_inv B) as [[? ?]|[? ?]]. subst.
       inversions H3. apply_empty* wft_weaken.
       apply_empty* wft_weaken.
Qed.

(** Extraction from a well-formed environment *)

Lemma wft_from_okt_typ : forall x T E,
  okt (E & x ~: T) -> wft E T.
Proof.
  intros. inversions* H.
  false (empty_push_inv H1).
  destruct (eq_push_inv H0) as [? [? ?]]. false.
  destruct (eq_push_inv H0) as [? [? ?]]. inversions~ H4.
  destruct (eq_push_inv H0) as [? [? ?]]. inversions~ H4.
Qed.

Lemma wft_from_okt_def : forall x T E,
  okt (E & x ~= T) -> wft E T.
Proof.
  intros. inversions* H.
  false (empty_push_inv H1).
  destruct (eq_push_inv H0) as [? [? ?]]. inversions~ H3.
  destruct (eq_push_inv H0) as [? [? ?]]. inversions~ H4.
  destruct (eq_push_inv H0) as [? [? ?]]. inversions~ H4.
Qed.

(** Automation *)

Lemma wft_weaken_right : forall T E F,
  wft E T ->
  ok (E & F) ->
  wft (E & F) T.
Proof.
  intros. apply_empty* wft_weaken.
Qed.

Hint Resolve wft_weaken_right.
Hint Resolve wft_from_okt_typ wft_from_okt_def.
Hint Immediate wft_from_env_has_def wft_from_env_has_typ.
Hint Resolve wft_subst_tb.


(* ********************************************************************** *)
(** ** Properties of well-formedness of an environment *)

(** Inversion lemma *)

Lemma okt_push_inv : forall E X B,
  okt (E & X ~ B) -> B = bind_tvar \/ exists T, B = bind_def T \/ B = bind_typ T.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&?&?): (eq_push_inv H). subst*.
    lets (?&?&?): (eq_push_inv H). subst*.
    lets (?&?&?): (eq_push_inv H). subst*.
Qed.

Lemma okt_push_tvar_inv : forall E X,
  okt (E & X ~tvar) -> okt E /\ X # E.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&M&?): (eq_push_inv H). subst. inverts~ M.
    lets (?&?&?): (eq_push_inv H). false.
    lets (?&?&?): (eq_push_inv H). false.
Qed.

Lemma okt_push_def_inv : forall E X T,
  okt (E & X ~= T) -> okt E /\ wft E T /\ X # E.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&M&?): (eq_push_inv H). subst. inverts~ M.
    lets (?&?&?): (eq_push_inv H). inversions~ H4.
    lets (?&?&?): (eq_push_inv H). false.
Qed.

Lemma okt_push_def_type : forall E X T,
  okt (E & X ~= T) -> type T.
Proof. intros. applys wft_type. forwards*: okt_push_def_inv. Qed.

Lemma okt_push_typ_inv : forall E x T,
  okt (E & x ~: T) -> okt E /\ wft E T /\ x # E.
Proof.
  introv O. inverts O.
    false* empty_push_inv.
    lets (?&?&?): (eq_push_inv H). false.
    lets (?&M&?): (eq_push_inv H). subst. inverts~ M.
    lets (?&M&?): (eq_push_inv H). subst. inverts~ M.
Qed.

Lemma okt_push_typ_type : forall E X T,
  okt (E & X ~: T) -> type T.
Proof. intros. applys wft_type. forwards*: okt_push_typ_inv. Qed.

Hint Immediate okt_push_def_type okt_push_typ_type.

(** Through strengthening *)

Lemma okt_strengthen : forall x T (E F:env),
  okt (E & x ~: T & F) ->
  okt (E & F).
Proof.
 introv O. induction F using env_ind.
  rewrite concat_empty_r in *. lets*: (okt_push_typ_inv O).
  rewrite concat_assoc in *.
   lets [? | (U&[?|?])]: okt_push_inv O; subst.
     lets (?&?): (okt_push_tvar_inv) O.
      applys~ okt_tvar.
     lets (?&?&?): (okt_push_def_inv O).
      applys~ okt_def. applys* wft_strengthen.
     lets (?&?&?): (okt_push_typ_inv O).
      applys~ okt_typ. applys* wft_strengthen.
Qed.

Lemma okt_strengthen_def : forall x T (E F:env),
  okt (E & x ~= T & F) ->
  okt (E & F).
Proof.
 introv O. induction F using env_ind.
  rewrite concat_empty_r in *. lets*: (okt_push_def_inv O).
  rewrite concat_assoc in *.
   lets [? | (U&[?|?])]: okt_push_inv O; subst.
     lets (?&?): (okt_push_tvar_inv) O.
      applys~ okt_tvar.
     lets (?&?&?): (okt_push_def_inv O).
      applys~ okt_def. applys* wft_strengthen_def.
     lets (?&?&?): (okt_push_typ_inv O).
      applys~ okt_typ. applys* wft_strengthen_def.
Qed.

(** Through type substitution *)

Lemma okt_subst_tb : forall Z P (E F:env),
  okt (E & Z ~tvar & F) ->
  wft E P ->
  okt (E & map (subst_tb Z P) F).
Proof.
 introv O W. induction F using env_ind.
  rewrite map_empty. rewrite concat_empty_r in *.
   lets*: (okt_push_tvar_inv O).
  rewrite map_push. rewrite concat_assoc in *.
   lets [?|(U&[?|?])]: okt_push_inv O; subst.
     lets (?&?): (okt_push_tvar_inv O).
      applys~ okt_tvar.
     lets (?&?&?): (okt_push_def_inv O).
      applys~ okt_def.
     lets (?&?&?): (okt_push_typ_inv O).
      applys~ okt_typ.
Qed.

(** Automation *)

Hint Resolve okt_subst_tb wft_weaken.
Hint Immediate okt_strengthen.

(** Concatenate *)

Lemma okt_concat_left_inv : forall E F,
    okt (E & F) ->
    okt E.
Proof.
  induction F using env_ind; intros.
  clean_empty H. auto.
  rewrite concat_assoc in H.
  destruct v. apply* IHF. apply* okt_push_tvar_inv.
    apply* IHF. apply* okt_push_def_inv.
    apply* IHF. apply* okt_push_typ_inv.
Qed.

(* ********************************************************************** *)
(** ** Environment is unchanged by substitution from a fresh name *)

Lemma notin_fv_tt_open : forall Y X T,
  X \notin fv_tt (T open_tt_var Y) ->
  X \notin fv_tt T.
Proof.
 introv. unfold open_tt. generalize 0.
 induction T; simpl; intros k Fr; auto.
 specializes IHT1 k. specializes IHT2 k. auto.
 specializes IHT (S k). auto.
Qed.

Lemma notin_fv_wf : forall E X T,
  wft E T -> X # E -> X \notin fv_tt T.
Proof.
  induction 1; intros Fr; simpl.
  eauto.
  rewrite notin_singleton. intro. subst. applys binds_fresh_inv H Fr.
  notin_simpl; auto.
  notin_simpl; auto. pick_fresh Y. apply* (@notin_fv_tt_open Y).
Qed.

Lemma map_subst_tb_id : forall G Z P,
  okt G -> Z # G -> G = map (subst_tb Z P) G.
Proof.
  induction 1; intros Fr; autorewrite with rew_env_map; simpl.
  auto.
  rewrite* <- IHokt. rewrite* subst_tt_fresh. rewrite~ <- IHokt. apply* notin_fv_wf.
  rewrite* <- IHokt. rewrite* subst_tt_fresh. apply* notin_fv_wf.
Qed.

Lemma map_env_twice: forall x A F,
    x \notin fv_tt A ->
    (map (subst_tb x A) (map (subst_tb x A) F)) = (map (subst_tb x A) F).
Proof.
  induction F using env_ind; introv neq.
  repeat rewrite* map_empty.
  destruct v; repeat rewrite map_push; simpls.
    rewrite* IHF. rewrite* IHF. rewrite~ subst_tt_fresh.
      apply* notin_subst_tt_self.
    rewrite* IHF. rewrite~ subst_tt_fresh.
      apply* notin_subst_tt_self.
Qed.

(* ********************************************************************** *)
(** ** Well-formedness of Application Context *)

(** Inversion lemma *)

Lemma wfa_push_inv : forall E V S,
    wfa E (cons V S) ->
    exists T, V = item_def T \/ V = item_typ T.
Proof.
  introv O. inverts O;
    autos*.
Qed.

Lemma wfa_push_def_inv : forall E V S,
    wfa E (cons (item_def V) S) ->
    wft E V /\ wfa E S.
Proof.
  introv O. inverts O;
    autos*.
Qed.

Lemma wfa_push_typ_inv : forall E V S,
    wfa E (cons (item_typ V) S) ->
    wft E V /\ wfa E S.
Proof.
  introv O. inverts O;
    autos*.
Qed.

Lemma wfa_push_def_type : forall E V S,
    wfa E (cons (item_def V) S) ->
    type V.
Proof.
  introv O. inverts O; autos*.
  apply* wft_type.
Qed.

Lemma wfa_push_typ_type : forall E V S,
    wfa E (cons (item_typ V) S) ->
    type V.
Proof.
  introv O. inverts O; autos*.
  apply* wft_type.
Qed.

Hint Immediate wfa_push_def_type wfa_push_typ_type.

(** Through strengthening *)

Lemma wfa_strengthen : forall E F x U T,
 wfa (E & x ~: U & F) T -> wfa (E & F) T.
Proof.
  intros. gen_eq G: (E & x ~: U & F). gen F.
  induction H; intros F EQ; subst; auto.
  constructor*.
  apply* wft_strengthen.
  constructor*.
  apply* wft_strengthen.
Qed.

Lemma wfa_strengthen_def : forall E F x U T,
 wfa (E & x ~= U & F) T -> wfa (E & F) T.
Proof.
  intros. gen_eq G: (E & x ~= U & F). gen F.
  induction H; intros F EQ; subst; auto.
  constructor*.
  apply* wft_strengthen_def.
  constructor*.
  apply* wft_strengthen_def.
Qed.

(** Through weakening *)

Lemma wfa_weaken : forall E F G T,
   wfa (E & G) T ->
   ok (E & F & G) ->
   wfa (E & F & G) T.
Proof.
  introv Typ. gen F. inductions Typ; introv oke; simpls*.
Qed.

(** Concatenate *)

Lemma wfa_concat: forall E S1 S2,
    wfa E S1 ->
    wfa E S2 ->
    wfa E (S1 ++ S2).
Proof.
  inductions S1; intros.
  rewrite* app_nil_l. destruct a.
  rewrite app_cons. inversions H. constructor*.
  rewrite app_cons. inversions H. constructor*.
Qed.

(* ********************************************************************** *)
(** ** Apply Environment *)

Lemma apply_env_empty : forall A,
   apply_env empty A = A.
Proof.
  rewrite* empty_def.
Qed.

Lemma apply_env_push_tvar : forall E A x,
   apply_env (E & x ~tvar) A = apply_env E A.
Proof.
  introv.
  rewrite single_def. rewrite concat_def.
  simpls*.
Qed.

Lemma apply_env_push_typ : forall E A x T,
   apply_env (E & x ~: T) A = apply_env E A.
Proof.
  introv.
  rewrite single_def. rewrite concat_def.
  simpls*.
Qed.

Lemma apply_env_push_def : forall E A x T,
   apply_env (E & x ~= T) A = apply_env E (subst_tt x T A).
Proof.
  introv.
  rewrite single_def. rewrite concat_def.
  simpls*.
Qed.

Lemma apply_env_concat : forall E F A,
   apply_env (E & F) A = apply_env E (apply_env F A).
Proof.
  induction F using env_ind; intros; simpls.
  rewrite apply_env_empty.
  rewrite* concat_empty_r.
  rewrite concat_assoc.
  destruct v.
    repeat rewrite* apply_env_push_tvar.
    repeat rewrite* apply_env_push_def.
    repeat rewrite* apply_env_push_typ.
Qed.

(** Through strengthening *)

Lemma apply_env_strengthen_typ : forall E F x T A,
    apply_env (E & x ~: T & F) A =
    apply_env (E & F) A.
Proof.
  intros. rewrite apply_env_concat.
  rewrite apply_env_push_typ.
  rewrite~ apply_env_concat.
Qed.

Lemma apply_env_strengthen_tvar : forall E F x A,
    apply_env (E & x ~tvar & F) A =
    apply_env (E & F) A.
Proof.
  intros. rewrite apply_env_concat.
  rewrite apply_env_push_tvar.
  rewrite~ apply_env_concat.
Qed.

(** Structural apply *)

Lemma apply_env_nat : forall E,
    apply_env E typ_nat = typ_nat.
Proof.
  inductions E; autos*.
  destruct a. destruct b; simpls*.
Qed.

Lemma apply_env_bvar : forall E X,
    apply_env E (typ_bvar X) = typ_bvar X.
Proof.
  induction E using env_ind; intros; autos*.
  rewrite* apply_env_empty.
  destruct v.
  rewrite* apply_env_push_tvar.
  rewrite* apply_env_push_def.
  rewrite* apply_env_push_typ.
Qed.

Lemma apply_env_fresh : forall E X,
    X # E ->
    apply_env E (typ_fvar X) = typ_fvar X.
Proof.
  induction E using env_ind; intros; autos*.
  rewrite* apply_env_empty.
  destruct v.
  rewrite* apply_env_push_tvar.
  rewrite* apply_env_push_def. simpls*. simpl_dom. case_var*.
  rewrite* apply_env_push_typ.
Qed.

Lemma apply_env_binds_typ : forall E X,
    ok E ->
    binds X bind_tvar E ->
    apply_env E (typ_fvar X) = typ_fvar X.
Proof.
  induction E using env_ind; intros; autos*.
  false* binds_empty_inv.
  binds_push * H0.
  rewrite* apply_env_push_tvar.
  apply apply_env_fresh. apply* ok_push_inv.
  destruct v.
  rewrite* apply_env_push_tvar.
  rewrite* apply_env_push_def. simpls*. case_var*.
  rewrite* apply_env_push_typ.
Qed.

Lemma apply_env_arrow : forall E A B,
    apply_env E (typ_arrow A B) =
    typ_arrow (apply_env E A) (apply_env E B).
Proof.
  induction E using env_ind; intros; autos*.
  repeat rewrite* apply_env_empty.
  destruct v.
  repeat rewrite* apply_env_push_tvar.
  repeat rewrite* apply_env_push_def.
  repeat rewrite* apply_env_push_typ.
Qed.

Lemma apply_env_all : forall E A,
    apply_env E (typ_all A) =
    typ_all (apply_env E A).
Proof.
  induction E using env_ind; intros; autos*.
  repeat rewrite* apply_env_empty.
  destruct v.
    repeat rewrite* apply_env_push_tvar.
    repeat rewrite* apply_env_push_def.
    repeat rewrite* apply_env_push_typ.
Qed.

(** Properties preserved during application *)

Lemma apply_env_open_tt : forall E A B,
    okt E ->
    apply_env E (open_tt A B) =
    open_tt (apply_env E A) (apply_env E B).
Proof.
  intros. gen A B; induction H; intros; autos*.
  repeat rewrite* apply_env_empty.
  repeat rewrite* apply_env_push_tvar.
  repeat rewrite* apply_env_push_def.
    rewrite* subst_tt_open_tt.
    apply* wft_type.
  repeat rewrite* apply_env_push_typ.
Qed.

Lemma apply_env_open_tt_right : forall F E A B,
    okt (F & E) ->
    apply_env E (open_tt A B) =
    open_tt (apply_env E A) (apply_env E B).
Proof.
  intros. gen A B; induction E using env_ind; intros; autos*.
  repeat rewrite* apply_env_empty.
  destruct v; rewrite concat_assoc in *.
    repeat rewrite* apply_env_push_tvar. apply* IHE.
      apply* okt_concat_left_inv.
    repeat rewrite* apply_env_push_def.
      rewrite* subst_tt_open_tt. apply* IHE.
      apply* okt_concat_left_inv.
    repeat rewrite* apply_env_push_typ. apply* IHE.
      apply* okt_concat_left_inv.
Qed.

Lemma apply_env_type_inv : forall E A,
    okt E ->
    type (apply_env E A) ->
    type A.
Proof.
  intro E. induction E using env_ind;
  introv okt dm.
  rewrite apply_env_empty in dm. simpls~.

  destruct v.
  rewrite apply_env_push_tvar in *; autos*.
  apply* IHE. apply* okt_push_tvar_inv.
  rewrite apply_env_push_def in *.
  apply IHE in dm; auto.
  apply subst_tt_type_inv with x t; auto.
  apply* okt_push_def_type.
  apply* okt_push_def_inv.
  apply* IHE. apply* okt_push_typ_inv.
  rewrite apply_env_push_typ in dm; auto.
Qed.

Lemma notin_apply_env : forall E A x,
    okt E ->
    x # E -> x \notin fv_tt A ->
    x \notin fv_tt (apply_env E A).
Proof.
  introv oke neq1 neq2. gen A. induction E using env_ind; intros.
  rewrite* apply_env_empty.
  destruct v.
    rewrite* apply_env_push_tvar. apply* IHE.
      apply* okt_concat_left_inv.
    rewrite* apply_env_push_def. apply* IHE.
      apply* okt_concat_left_inv. apply* notin_subst_tt.
      apply* notin_fv_wf.
    rewrite* apply_env_push_typ. apply* IHE.
      apply* okt_concat_left_inv.
Qed.

Lemma notin_apply_env_right : forall E F A x,
    okt (F & E) ->
    x # (F & E) -> x \notin fv_tt A ->
    x \notin fv_tt (apply_env E A).
Proof.
  introv oke neq1 neq2. gen A. induction E using env_ind; intros.
  rewrite* apply_env_empty.
  destruct v; rewrite concat_assoc in *.
    rewrite* apply_env_push_tvar. apply* IHE.
      apply* okt_concat_left_inv.
    rewrite* apply_env_push_def. apply* IHE.
      apply* okt_concat_left_inv. apply* notin_subst_tt.
      apply* notin_fv_wf.
    rewrite* apply_env_push_typ. apply* IHE.
      apply* okt_concat_left_inv.
Qed.

Lemma notin_apply_env_inv : forall A E x,
    x # E ->
    x \notin fv_tt (apply_env E A) ->
    x \notin fv_tt A.
Proof.
  introv notin1 notin2. gen A.
  induction E using env_ind; intros; simpls~.
  rewrite apply_env_empty in notin2; auto.
  destruct v.
  rewrite apply_env_push_tvar in notin2; auto.
  rewrite apply_env_push_def in notin2; auto.
  forwards ~ : IHE notin2.
  forwards ~ [[? ?] | [? [? ?]]]: notin_subst_tt_inv H.
  subst~. simpl_dom. false* notin_same.
  rewrite apply_env_push_typ in notin2; auto.
Qed.

(** Apply on well formed type *)

Lemma apply_env_wft: forall E A,
    okt E ->
    wft E A ->
    apply_env E A = A.
Proof.
  introv oke wf. inductions wf; simpls*.
  rewrite* apply_env_nat.
  rewrite* apply_env_binds_typ.
  rewrite* apply_env_arrow. f_equal*.
  rewrite* apply_env_all. f_equal*.
    pick_fresh x. forwards ~ : H0 x.
    rewrite apply_env_push_tvar in H1.
    rewrite apply_env_open_tt in H1; auto.
    rewrite apply_env_fresh in H1; auto.
    apply open_tt_var_fresh in H1; auto.
    assert (x \notin fv_tt (apply_env E T)). apply* notin_apply_env. auto.
Qed.

Lemma apply_env_wft_right : forall F E A,
    okt (F & E) ->
    wft (F & E) A ->
    apply_env E A = A.
Proof.
  introv oke wf. inductions wf; simpls*.
  rewrite* apply_env_nat.
  binds_cases H. apply* apply_env_fresh.
  rewrite* apply_env_binds_typ.
  apply ok_from_okt in oke. apply* ok_concat_inv.
  rewrite* apply_env_arrow. f_equal*.
  rewrite* apply_env_all. f_equal*.
    pick_fresh x. forwards ~ : H0 x (E & x ~tvar) F;
    try(rewrite~ concat_assoc).
    rewrite apply_env_push_tvar in H1.
    rewrite apply_env_open_tt_right with (F:=F) in H1; auto.
    rewrite apply_env_fresh in H1; auto.
    apply open_tt_var_fresh in H1; auto.
    assert (x \notin fv_tt (apply_env E T)).
    apply* notin_apply_env_right. auto.
Qed.

(** Apply twice *)

Lemma apply_env_twice: forall E V,
    okt E ->
    apply_env E (apply_env E V) = apply_env E V.
Proof.
  introv ok. gen V. inductions ok; intros; simpls~.
  repeat rewrite~ apply_env_empty.
  repeat rewrite~ apply_env_push_tvar.
  repeat rewrite~ apply_env_push_def.
    rewrite~ subst_tt_fresh.
    apply* notin_apply_env.
    apply* notin_subst_tt_self.
    apply* notin_fv_wf.
  repeat rewrite~ apply_env_push_typ.
Qed.

(** Subst_tt on applying env *)

Lemma subst_tt_apply_env : forall E x A V F,
    x # E ->
    okt (F & x ~= A) ->
    ok (F & x ~= A & E) ->
    subst_tt x A (apply_env E V) =
    apply_env (map (subst_tb x A) E) (subst_tt x A V).
Proof.
  introv neq okf oke. gen V. induction E using env_ind; intros.
  rewrite map_empty. do 2 rewrite* apply_env_empty.
  destruct v; rewrite concat_assoc in *.
    rewrite map_push. do 2 rewrite apply_env_push_tvar. apply* IHE.
    rewrite map_push. simpls. do 2 rewrite apply_env_push_def.
      rewrite* IHE. rewrite* <- subst_tt_commu. simpl_dom. auto.
      lets [_ ?] : ok_push_inv oke.
      lets [? [? ?]] : okt_push_def_inv okf.
      apply* notin_fv_wf.
    rewrite map_push. simpls. do 2 rewrite apply_env_push_typ. apply* IHE.
Qed.

(** Change Definition *)

Lemma apply_env_push_change: forall E x U V,
    okt (E & x ~= apply_env E U) ->
    apply_env (E & x ~= apply_env E U) V =
    apply_env (E & x ~= U) V.
Proof.
  introv oke.
  induction V.
  do 2 rewrite apply_env_nat. simpls~.
  do 2 rewrite apply_env_bvar. simpls~.
    rewrite apply_env_push_def. simpls. case_var*.
    rewrite apply_env_push_def. simpls. case_var*.
    apply* apply_env_twice. apply* okt_concat_left_inv.
  rewrite apply_env_push_def. simpls. case_var*.
  do 2 rewrite apply_env_arrow. simpls. f_equal*.
  do 2 rewrite apply_env_all. simpls. f_equal*.
Qed.

Lemma apply_env_middle_change: forall E x U V F,
    okt (E & x ~= apply_env E U) ->
    apply_env (E & x ~= apply_env E U & F) V =
    apply_env (E & x ~= U & F) V.
Proof.
  introv oke. rewrite apply_env_concat.
  rewrite* apply_env_push_change.
  rewrite* <- apply_env_concat.
Qed.

(** Exchange contexts *)

Lemma apply_env_push_def_subst_tt : forall E x A V,
    okt (E & x ~= A) ->
    apply_env (E & x ~= A) V = subst_tt x A (apply_env E V).
Proof.
  introv oke. induction V; simpls*.
  do 2 rewrite apply_env_nat. simpls~.
  do 2 rewrite apply_env_bvar. simpls~.
  rewrite apply_env_push_def. simpls. case_var*.
    rewrite~ apply_env_fresh. simpls*. case_var*.
    apply* apply_env_wft. apply* okt_concat_left_inv.
    apply* okt_push_def_inv.
    rewrite* subst_tt_fresh. apply notin_apply_env.
      apply* okt_concat_left_inv. apply* okt_push_def_inv. simpls*.
  do 2 rewrite apply_env_arrow. simpls. f_equal*.
  do 2 rewrite apply_env_all. simpls. f_equal*.
Qed.

Lemma apply_env_push_def_subst_tt_right : forall E x A V F,
    okt (F & E & x ~= A) ->
    apply_env (E & x ~= A) V = subst_tt x A (apply_env E V).
Proof.
  introv oke. induction V; simpls*.
  do 2 rewrite apply_env_nat. simpls~.
  do 2 rewrite apply_env_bvar. simpls~.
  rewrite apply_env_push_def. simpls. case_var*.
    rewrite~ apply_env_fresh. simpls*. case_var*.
    apply* apply_env_wft_right. apply* okt_concat_left_inv.
    apply okt_push_def_inv in oke. destructs~ oke.
    rewrite* subst_tt_fresh.
    apply notin_apply_env_right with (F:=F); auto.
      apply* okt_concat_left_inv. apply* okt_push_def_inv. simpls*.
  do 2 rewrite apply_env_arrow. simpls. f_equal*.
  do 2 rewrite apply_env_all. simpls. f_equal*.
Qed.

Lemma apply_env_concat_inv : forall E F A,
    okt (E & F) ->
    apply_env (E & F) A  = apply_env (F & E) A.
Proof.
  induction F using env_ind; introv oke.
  rewrite concat_empty_r. rewrite concat_empty_l. auto.
  destruct v; rewrite concat_assoc in *.
    rewrite apply_env_push_tvar. do 2 rewrite apply_env_concat.
      rewrite~ apply_env_push_tvar. do 2 rewrite <- apply_env_concat.
      apply* IHF. apply* okt_concat_left_inv.
    rewrite* apply_env_push_def_subst_tt.
      do 2 rewrite apply_env_concat.
      rewrite apply_env_push_def_subst_tt_right with (F:=E); auto.
      do 2 rewrite <- apply_env_concat. rewrite* IHF.
      apply* okt_concat_left_inv.
    rewrite apply_env_push_typ. do 2 rewrite apply_env_concat.
      rewrite~ apply_env_push_typ. do 2 rewrite <- apply_env_concat.
      apply* IHF. apply* okt_concat_left_inv.
Qed.

Lemma apply_env_strengthen_def : forall E F x A V,
    okt (E & x ~= A & map (subst_tb x A) F) ->
    apply_env (E & x ~= A & map (subst_tb x A) F) V
    = subst_tt x A (apply_env (E & F) V).
Proof.
  introv oke. repeat rewrite apply_env_concat.
  rewrite <- apply_env_push_def_subst_tt.
  repeat rewrite apply_env_concat.
  repeat rewrite single_def; simpls.
  rewrite subst_tt_apply_env with (F:=E); auto.
  rewrite subst_tt_apply_env with (F:=E); auto.
  rewrite* map_env_twice.
  apply okt_concat_left_inv in oke.
    lets [? [? ?]]: okt_push_def_inv oke. apply* notin_fv_wf.
    apply ok_from_okt in oke. lets~ [? ?] : ok_middle_inv oke.
    apply* okt_concat_left_inv.
    apply ok_from_okt in oke.
    apply* ok_map_inv.
    apply ok_from_okt in oke. lets~ [? ?] : ok_middle_inv oke.
    apply* okt_concat_left_inv. apply* okt_concat_left_inv.
Qed.

Lemma apply_env_middle_def_subst_tt : forall E F x A V,
    okt (E & x ~= apply_env E A & F) ->
      apply_env (E & x ~= apply_env E A & F) V
    = apply_env (E & F) (subst_tt x A V).
Proof.
  introv oke.
  rewrite apply_env_concat. rewrite apply_env_push_def.
  assert (I: okt (E & F & x ~= apply_env E A)).
    constructor~.
    apply* okt_strengthen_def.
    apply_empty* wft_weaken.
      apply okt_concat_left_inv in oke.
      lets~ [? [? ?]] : okt_push_def_inv oke.
      apply ok_from_okt in oke.
      apply* ok_remove.
      apply ok_from_okt in oke. apply ok_middle_inv in oke. destructs~ oke.
  rewrite <- apply_env_push_def_subst_tt_right with (F:=E); auto.
  rewrite <- apply_env_concat. rewrite <- apply_env_push_def.
  rewrite concat_assoc.
  assert (apply_env (E & F) A = apply_env E A).
    rewrite* apply_env_concat_inv. rewrite apply_env_concat.
    rewrite apply_env_wft_right with (F:=E); auto.
    apply* okt_concat_left_inv. apply* okt_push_def_inv.
    apply* okt_concat_left_inv.
  rewrite <- H.
  rewrite* apply_env_push_change. rewrite~ H.
Qed.

(* ********************************************************************** *)
(** ** Regularity of relations *)

(** The subtyping relation is restricted to well-formed objects. *)

Lemma sub_regular : forall E Q S T,
    sub Q S T ->
    wfa E Q -> okt E -> wft E S ->
    wft E T.
Proof.
  induction 1; intros. autos*.
  inversions H0.
    apply~ IHsub.
    apply~ wft_open.
  inversions H0.
    apply~ IHsub.
    inversions~ H2.
Qed.

(** The typing relation is restricted to well-formed objects. *)

Lemma typing_regular : forall E Q e T,
  typing E Q e T -> okt E /\ term e /\ wft E T /\ wfa E Q.
Proof.
  induction 1.
  splits*. apply sub_regular with S A; auto.
    apply* wft_from_env_has_typ.
  splits*.
  splits.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: okt_push_typ_inv.
   apply_fresh* term_abs as y.
     specializes H0 y. destructs~ H0.
   pick_fresh y. specializes H0 y. destructs~ H0.
     apply_empty* wft_strengthen.
   constructor~.
     pick_fresh y. specializes H0 y. destructs~ H0.
       apply_empty* wfa_strengthen.
     pick_fresh y. specializes H0 y. destructs~ H0.
       apply_empty* wft_strengthen.

  splits.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: okt_push_typ_inv.
   apply_fresh* term_absann as y.
     pick_fresh y. specializes H0 y. destructs~ H0.
      apply apply_env_type_inv with E; auto.
      forwards*: okt_push_typ_inv.
      forwards*: okt_push_typ_inv.
     specializes H0 y. destructs~ H0.
   pick_fresh y. specializes H0 y. destructs~ H0.
     apply* wft_arrow.
     apply_empty* wft_strengthen.
   constructor~.

  splits*.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: okt_push_typ_inv.
   apply_fresh* term_absann as y.
     pick_fresh y. specializes H0 y. destructs~ H0.
      apply apply_env_type_inv with E; auto.
      forwards*: okt_push_typ_inv.
      forwards*: okt_push_typ_inv.
     specializes H0 y. destructs~ H0.
   pick_fresh y. specializes H0 y. destructs~ H0.
     apply_empty* wft_strengthen.
   constructor~.
   pick_fresh y. specializes H0 y. destructs~ H0.
     apply_empty* wfa_strengthen.
   pick_fresh y. specializes H0 y. destructs~ H0.
     forwards*: okt_push_typ_inv.

  splits*. destructs IHtyping2. inversion* H4.

  splits*.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: okt_push_tvar_inv.
   apply_fresh* term_tabs as y.
     forwards~ K: (H0 y). destructs K. auto.
   apply_fresh* wft_all as Y.
     pick_fresh y. forwards~ K: (H0 y). destructs K.
      forwards*: okt_push_tvar_inv.
     forwards~ K: (H0 Y). destructs K.
      forwards*: okt_push_tvar_inv.

  splits*.
   pick_fresh y. specializes H0 y. destructs~ H0.
    forwards*: okt_push_def_inv.
   apply_fresh* term_tabs as y.
     forwards~ K: (H0 y). destructs K. auto.
   pick_fresh y. forwards~ K: (H0 y). destructs K.
     apply_empty* wft_strengthen_def.
   constructor*.
     pick_fresh y. forwards~ K: (H0 y). destructs K.
       apply_empty* wfa_strengthen_def.
     pick_fresh y. forwards~ K: (H0 y). destructs K.
       apply* okt_push_def_inv.

  splits*.
    apply* term_tapp.
      apply apply_env_type_inv with E; autos*.
    apply* wfa_push_def_inv.
Qed.

(** The value relation is restricted to well-formed objects. *)

Lemma value_regular : forall t,
  value t -> term t.
Proof.
  induction 1; autos*.
Qed.

(** The reduction relation is restricted to well-formed objects. *)

Lemma red_regular : forall t t',
  red t t' -> term t /\ term t'.
Proof.
  induction 1; split; autos* value_regular.
  inversions H. pick_fresh y. rewrite* (@subst_ee_intro y).
  inversions H. pick_fresh y. rewrite* (@subst_ee_intro y).
  inversions H. pick_fresh Y. rewrite* (@subst_te_intro Y).
Qed.

(** Automation *)

Hint Extern 1 (okt ?E) =>
  match goal with
  | H: typing _ _ _ _ |- _ => apply (proj41 (typing_regular H))
  end.

Hint Extern 1 (wft ?E ?T) =>
  match goal with
  | H: typing E _ _ T |- _ => apply (proj43 (typing_regular H))
  | H: sub E _ _ T |- _ => apply (sub_regular H)
  end.

Hint Extern 1 (type ?T) =>
  let go E := apply (@wft_type E); auto in
  match goal with
  | H: typing ?E _ _ T |- _ => go E
  | H: sub ?E _ _ T |- _ => go E
  end.

Hint Extern 1 (term ?e) =>
  match goal with
  | H: typing _ _ ?e _ |- _ => apply (proj42 (typing_regular H))
  | H: red ?e _ |- _ => apply (proj1 (red_regular H))
  | H: red _ ?e |- _ => apply (proj2 (red_regular H))
  end.

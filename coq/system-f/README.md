# Proofs

This project contains all the Coq proof code for the second application in the paper *let arguments go first*.

All the proofs have been tested under Coq 8.5pl1, which is compiled with OCaml 4.03.0.

## Files

- `DeclDef.v`: the definition of declarative system.
- `DeclInfra.v`: the infrastructure of declarative system.
- `DeclSound.v`: proofs including progress and preservation.

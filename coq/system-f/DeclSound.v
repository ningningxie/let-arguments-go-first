Set Implicit Arguments.
Require Import LibReflect.
Require Import LibList LibLN DeclDef DeclInfra.

(* ********************************************************************** *)
(** * Properties of Subtyping *)

(* ********************************************************************** *)

Lemma sub_trans: forall Q1 Q2 S T A,
   sub Q1 S T ->
   sub Q2 T A ->
   sub (Q1 ++ Q2) S A.
Proof.
  introv sub1 sub2. gen Q2 A S.
  induction Q1; intros; simpls*.
  inversions sub1. rewrite* app_nil_l.
  rewrite app_cons.
  inversions sub1;
    constructor*.
Qed.

(* ********************************************************************** *)
(** * Properties of Typing *)

(* ********************************************************************** *)

(** Weakening *)

Lemma apply_env_weaken : forall G E F V,
    wft (E & G) (apply_env (E & G) V) ->
    ok (E & F & G) ->
    okt (E & G) ->
    (apply_env (E & G) V) = (apply_env (E & F & G) V).
Proof.
  induction F using env_ind; introv wf okf okg.
  rewrite~ concat_empty_r.
  rewrite concat_assoc in *. destruct v.
    do 2 rewrite apply_env_concat. rewrite apply_env_push_tvar.
      do 2 rewrite~ <- apply_env_concat.
    do 2 rewrite apply_env_concat. rewrite apply_env_push_def.
      rewrite~ subst_tt_fresh.
      do 2 rewrite~ <- apply_env_concat.
      apply notin_apply_env_right with (F:= E); auto.
      lets ~ [? ?] : ok_middle_inv okf.
      forwards : notin_fv_wf x wf. lets ~ [? ?] : ok_middle_inv okf.
      apply notin_apply_env_inv with (E & G); auto. lets ~ [? ?] : ok_middle_inv okf.
    do 2 rewrite apply_env_concat. rewrite apply_env_push_typ.
      do 2 rewrite~ <- apply_env_concat.
Qed.

Lemma typing_weaken : forall E F G e T Q,
   typing (E & G) Q e T ->
   okt (E & F & G) ->
   typing (E & F & G) Q e T.
Proof.
  introv Typ. gen F. lets ty : Typ. inductions Typ; introv Ok.
  apply* typing_var. apply* wfa_weaken. apply* binds_weaken.
  apply* typing_nat.
  apply_fresh* typing_abs as x. forwards~ K: (H x).
    apply_ih_bind (H0 x); eauto.
    assert (I : (apply_env (E & G) V) = (apply_env (E & F & G) V)).
      rewrite apply_env_weaken with (F:=F); auto.
      pick_fresh y. forwards ~ : H y. lets [? _] : typing_regular H1.
      lets~ [? [? ?]] : okt_push_typ_inv H2.
    rewrite I. apply_fresh typing_absann_a as x. apply_ih_bind * H0.
    rewrite~ I. rewrite~ <- I. constructor~.
    rewrite~ <- I. apply~ wft_weaken.
      forwards ~ : H x. lets [? _] : typing_regular H1.
      lets~ [? [? ?]] : okt_push_typ_inv H2.
  assert (I : (apply_env (E & G) V) = (apply_env (E & F & G) V)).
     rewrite apply_env_weaken with (F:=F); auto.
     pick_fresh y. forwards ~ : H y. lets [? _] : typing_regular H1.
     lets~ [? [? ?]] : okt_push_typ_inv H2.
   rewrite I. apply_fresh typing_absann_b as x. apply_ih_bind * H0.
   rewrite~ I. rewrite~ <- I. constructor~.
   rewrite~ <- I. apply~ wft_weaken.
     forwards ~ : H x. lets [? _] : typing_regular H1.
     lets~ [? [? ?]] : okt_push_typ_inv H2.
  apply* typing_app.
  apply_fresh* typing_tabs_a as X. forwards~ K: (H X).
    apply_ih_bind (H0 X); eauto.
  apply_fresh* typing_tabs_b as X. forwards~ K: (H X).
    apply_ih_bind (H0 X); eauto.
  apply* typing_tapp.
    assert (I : (apply_env (E & G) A) = (apply_env (E & F & G) A)).
      rewrite apply_env_weaken with (F:=F); auto.
      lets [_ [_ [_ ?]]] : typing_regular Typ. inversions~ H.
    rewrite <- I. apply* IHTyp.
Qed.

(* ********************************************************************** *)
(** * Subsitution *)

(* ********************************************************************** *)

Lemma ok_tvar_to_def : forall E F x A,
   ok (E & x ~tvar & F) ->
   ok (E & x ~= A & map (subst_tb x A) F).
Proof.
  intros. apply* ok_concat_map. apply* ok_middle_change.
Qed.

Lemma wft_tvar_to_def: forall E F x A t,
   ok (E & x ~tvar & F) ->
   wft (E & x ~tvar & F) t ->
   wft E A ->
   wft (E & x ~= A & map (subst_tb x A) F) (subst_tt x A t).
Proof.
  introv oke wf wfa. inductions wf; simpls*.
  binds_cases H; case_var*.
    rewrite <- concat_assoc. apply_empty* wft_weaken. rewrite* concat_assoc.
      apply* ok_tvar_to_def.
    rewrite <- concat_assoc. apply_empty* wft_weaken. rewrite* concat_assoc.
      apply* ok_tvar_to_def.
    rewrite <- concat_assoc. apply_empty* wft_weaken. rewrite* concat_assoc.
      apply* ok_tvar_to_def.
    apply wft_tvar. apply* binds_concat_right.
      unsimpl (subst_tb x A bind_tvar). apply* binds_map.
  apply_fresh wft_all as x.
  rewrite* subst_tt_open_tt_var.
  forwards * : H0 y x (F & y ~tvar).
    rewrite* concat_assoc. rewrite* concat_assoc.
    rewrite map_push in H1.
    rewrite concat_assoc in H1; auto.
    apply* wft_type.
Qed.

Lemma okt_tvar_to_def : forall E F x A,
    okt (E & x ~tvar & F) ->
    wft E A ->
    okt (E & x ~= A & map (subst_tb x A) F).
Proof.
  introv okt wf. induction F using env_ind; simpls*.
  rewrite map_empty. rewrite* concat_empty_r.
    constructor*. clean_empty okt. apply* okt_push_tvar_inv.
  destruct v; rewrite map_push; rewrite concat_assoc in * ; simpls*.
    constructor. apply* IHF. apply* okt_push_tvar_inv.
      simpl_dom. rewrite dom_map.
      forwards [_ ?] : okt_push_tvar_inv okt. simpl_dom. auto.
    constructor. apply* IHF. apply* okt_push_def_inv.
      apply* wft_tvar_to_def. apply ok_from_okt. apply* okt_push_def_inv.
      simpl_dom. rewrite dom_map.
      forwards [_ [_ ?]] : okt_push_def_inv okt. simpl_dom. auto.
    constructor. apply* IHF. apply* okt_push_typ_inv.
      apply* wft_tvar_to_def. apply ok_from_okt. apply* okt_push_typ_inv.
      simpl_dom. rewrite dom_map.
      forwards [_ [_ ?]] : okt_push_typ_inv okt. simpl_dom. auto.
Qed.

Lemma wfa_tvar_to_def : forall E F x S A,
    okt (E & x ~tvar & F) ->
    wfa (E & x ~tvar & F) S ->
    wft E A ->
    wfa (E & x ~= A & map (subst_tb x A) F) (List.map (subst_ti x A) S).
Proof.
  introv oke fa ft. inductions fa; simpls*.
  constructor*. apply* wft_tvar_to_def.
  constructor*. apply* wft_tvar_to_def.
Qed.

Lemma sub_tvar_to_def : forall S A B T x,
        sub S A B ->
        type T ->
        sub (List.map (subst_ti x T) S)
            (subst_tt x T A) (subst_tt x T B)
.
Proof.
  introv su. inductions su; simpls*.
  constructor~. rewrite* <- subst_tt_open_tt.
Qed.

Lemma typing_tvar_to_def : forall E S x A F e T,
    typing (E & x ~tvar & F) S e T ->
    wft E A ->
    typing (E & x ~= A & map (subst_tb x A) F)
           (List.map (subst_ti x A) S)
           e
           (subst_tt x A T).
Proof.
  introv ty wf. inductions ty; simpls*.

  (* var *)
  binds_cases H1.
    apply typing_var with A0; autos*.
      apply* okt_tvar_to_def. apply* wfa_tvar_to_def.
      assert (subst_tt x A A0 = A0).
        rewrite* subst_tt_fresh.
        apply notin_fv_wf with E.
          apply* wft_from_env_has_typ.
          do 2 apply okt_concat_left_inv in H. auto.
        apply okt_concat_left_inv in H. apply* okt_push_tvar_inv.
      rewrite <- H1. apply* sub_tvar_to_def. apply* wft_type.
    apply typing_var with (subst_tt x A A0); autos*.
      apply* okt_tvar_to_def. apply* wfa_tvar_to_def.
      apply* binds_concat_right. unsimpl_map_bind *.
      apply* sub_tvar_to_def. apply* wft_type.
  (* nat *)
  apply~ typing_nat.
  apply* okt_tvar_to_def.
  (* abs *)
  apply_fresh typing_abs as x.
  rewrite <- concat_assoc.
  assert ((map (subst_tb x A) F & y ~: subst_tt x A V) =
           map (subst_tb x A) (F & y ~: V)).
    rewrite* map_push.
  rewrite H1. apply* H0.
  rewrite* concat_assoc.
  (* absann *)
  assert ((subst_tt x A (apply_env (E & x ~tvar & F) V)) =
          apply_env (E & x ~= A & map (subst_tb x A) F) V).
    rewrite* apply_env_strengthen_def.
    rewrite* apply_env_strengthen_tvar.
    pick_fresh y. forwards ~ : H y. lets [? _] : typing_regular H1.
    apply okt_concat_left_inv in H2. apply* okt_tvar_to_def.
  rewrite H1.
  apply_fresh typing_absann_a as x.
  forwards * : H0 y E x (F & y ~: apply_env (E & x ~tvar & F) V).
    rewrite* concat_assoc.
  rewrite map_push in H2. simpls.
  rewrite concat_assoc in H2.
  rewrite H1 in H2; auto.
  (* absann *)
  assert ((subst_tt x A (apply_env (E & x ~tvar & F) V)) =
          apply_env (E & x ~= A & map (subst_tb x A) F) V).
    rewrite* apply_env_strengthen_def.
    rewrite* apply_env_strengthen_tvar.
    pick_fresh y. forwards ~ : H y. lets [? _] : typing_regular H1.
    apply okt_concat_left_inv in H2. apply* okt_tvar_to_def.
  rewrite H1.
  apply_fresh typing_absann_b as x.
  forwards * : H0 y E x (F & y ~: apply_env (E & x ~tvar & F) V).
    rewrite* concat_assoc.
  rewrite map_push in H2. simpls.
  rewrite concat_assoc in H2.
  rewrite H1 in H2; auto.
  (* tabs *)
  apply_fresh typing_tabs_a as x.
  rewrite* subst_tt_open_tt_var.
  rewrite <- concat_assoc.
  assert ((map (subst_tb x A) F & y ~tvar) =
          map (subst_tb x A) (F & y ~tvar)).
    rewrite* map_push.
  rewrite H1. apply* H0.
  rewrite* concat_assoc.
  apply* wft_type.
  (* tabs_b *)
  apply_fresh typing_tabs_b as x.
  rewrite <- concat_assoc.
  assert ((map (subst_tb x A) F & y ~= subst_tt x A A0) =
          map (subst_tb x A) (F & y ~= A0)).
    rewrite* map_push.
  rewrite H1. apply* H0.
  rewrite* concat_assoc.
  (* tapp *)
  apply* typing_tapp.
  assert ((subst_tt x A (apply_env (E & x ~tvar & F) A0)) =
          (apply_env (E & x ~= A & map (subst_tb x A) F) A0)).
    rewrite* apply_env_strengthen_def.
    rewrite* apply_env_strengthen_tvar.
    pick_fresh y. lets [? _] : typing_regular ty.
    apply okt_concat_left_inv in H. apply* okt_tvar_to_def.
  rewrite* <- H.
Qed.

Lemma typing_tvar_to_def_empty : forall E x A e T,
    typing (E & x ~tvar) nil e T ->
    wft E A ->
    typing (E & x ~= A) nil e (subst_tt x A T).
Proof.
  intros.
  assert (E & x ~tvar = E & x ~tvar & empty) by rewrite~ concat_empty_r.
  rewrite H1 in H.
  forwards ~ : typing_tvar_to_def A H. simpls.
  rewrite map_empty in H2. clean_empty H2. auto.
Qed.

Lemma subsumption : forall E S1 u U T v,
    typing E S1 u U ->
    wfa E (v :: nil) ->
    sub (v :: nil) U T ->
    typing E (S1 ++ v:: nil) u T.
Proof.
  introv ty wf sub. lets ty' : ty. gen v T. inductions ty; intros.
  apply* typing_var.
    apply* wfa_concat.
    apply* sub_trans.
  (* nat *)
  inversions sub.
  (* abs *)
  rewrite app_cons. apply_fresh typing_abs as x.
    apply* H0. apply_empty* wfa_weaken.
  (* absann *)
  rewrite app_nil_l.
      inversions sub. apply_fresh typing_absann_b as x; autos*.
        inversions* H6.
  (* absann *)
  rewrite app_cons.
    apply_fresh typing_absann_b as x; autos*.
    apply* H0. apply_empty* wfa_weaken.
  (* app *)
  apply* typing_app.
  (* tabs *)
  rewrite app_nil_l.
    inversions sub. inversions H5. apply_fresh typing_tabs_b as x; autos*.
    forwards~ : H x.
    forwards ~ : typing_tvar_to_def_empty A H1. inversions~ wf.
    rewrite <- subst_tt_intro in H2; auto. inversions~ wf. apply* wft_type.
  (* tabs *)
  rewrite app_cons.
      apply_fresh typing_tabs_b as x; autos*.
      forwards * : H0 x H v. apply_empty* wfa_weaken.
  (* tapp *)
  apply* typing_tapp.
Qed.

Lemma subsumption_general : forall E S1 u U T S2,
    typing E S1 u U ->
    wfa E S2 ->
    sub S2 U T ->
    typing E (S1 ++ S2) u T.
Proof.
  introv. gen S1 U T.
  inductions S2; introv ty wf su.
    inversions su. rewrite~ app_nil_r.
    inversions su.
      rewrite app_last. apply* IHS2.
        apply* subsumption. constructor*. inversions~ wf.
        inversions~ wf.
      rewrite app_last. apply* IHS2.
        apply* subsumption. constructor*. inversions~ wf.
        inversions~ wf.
Qed.

Lemma typing_through_subst_ee : forall U S E F x T e u,
  typing (E & x ~: U & F) S e T ->
  typing E nil u U ->
  typing (E & F) S (subst_ee x u e) T.
Proof.
  introv TypT TypU. inductions TypT; introv; simpl.
  case_var.
    binds_get H1.
      assert (typing (E & F) nil u U). apply_empty* typing_weaken.
      forwards ~ : subsumption_general H3 H2.
      apply* wfa_strengthen.
    binds_cases H1; apply* typing_var.
    apply* wfa_strengthen.
    apply* wfa_strengthen.
  (* nat *)
  apply* typing_nat.
  (* case : abs *)
  apply_fresh* typing_abs as y.
   rewrite* subst_ee_open_ee_var.
    apply_ih_bind* H0.
  (* case : absann *)
  rewrite apply_env_strengthen_typ.
  apply_fresh* typing_absann_a as y.
  forwards* : H0 y.
  rewrite* concat_assoc.
  rewrite apply_env_strengthen_typ in H1.
  rewrite concat_assoc in H1; auto.
  rewrite* subst_ee_open_ee_var.
  (* case: absann *)
  rewrite apply_env_strengthen_typ.
  apply_fresh* typing_absann_b as y.
    forwards* : H0 y.
    rewrite* concat_assoc.
    rewrite apply_env_strengthen_typ in H1.
    rewrite* subst_ee_open_ee_var.
    apply_ih_bind* H1.
  (* app *)
  apply* typing_app.
  (* tabs *)
  apply_fresh* typing_tabs_a as y.
    rewrite* subst_ee_open_te_var.
    apply_ih_bind* H0.
  (* tabs *)
  apply_fresh* typing_tabs_b as Y.
    rewrite* subst_ee_open_te_var.
    apply_ih_bind* H0.
  (* tapp *)
  apply* typing_tapp.
    forwards* : IHTypT.
    rewrite apply_env_strengthen_typ in H; auto.
Qed.

(* ********************************************************************** *)
(** * Subsitution *)

(* ********************************************************************** *)

Lemma typing_through_subst_te : forall U S E F x T e,
  typing (E & x ~= apply_env E U & F) S e T ->
  type U ->
  typing (E & F) S (subst_te x U e) T.
Proof.
  introv TypT TypU.
  inductions TypT; introv; simpls*.
  apply typing_var with A; auto. apply* okt_strengthen_def.
    apply* wfa_strengthen_def.
    tests : (x0 = x). binds_mid~. inversions H3.
    apply* binds_remove.
  apply* typing_nat. apply* okt_strengthen_def.
  apply_fresh typing_abs as x.
    rewrite subst_te_open_ee_var. apply_ih_bind * H0.
  rewrite* apply_env_middle_def_subst_tt.
    apply_fresh typing_absann_a as y. rewrite subst_te_open_ee_var.
    rewrite apply_env_middle_def_subst_tt in H0; auto.
    apply_ih_bind * H0.
    forwards ~ : H y. lets [? _] : typing_regular H1. apply* okt_concat_left_inv.
    pick_fresh y.
    forwards ~ : H y. lets [? _] : typing_regular H1. apply* okt_concat_left_inv.
  rewrite* apply_env_middle_def_subst_tt.
    apply_fresh typing_absann_b as y. rewrite subst_te_open_ee_var.
    rewrite apply_env_middle_def_subst_tt in H0; auto.
    apply_ih_bind * H0.
    forwards ~ : H y. lets [? _] : typing_regular H1. apply* okt_concat_left_inv.
    pick_fresh y.
    forwards ~ : H y. lets [? _] : typing_regular H1. apply* okt_concat_left_inv.
  apply_fresh typing_tabs_a as x.
    rewrite* subst_te_open_te_var. apply_ih_bind * H0.
  apply_fresh typing_tabs_b as x.
    rewrite* subst_te_open_te_var. apply_ih_bind * H0.
  apply typing_tapp.
    rewrite apply_env_middle_def_subst_tt in IHTypT; auto.
Qed.

(* ********************************************************************** *)
(** * Preservation *)

Lemma typing_preservation : forall E S e T e',
    typing E S e T ->
    red e e' ->
    typing E S e' T.
Proof.
  introv ty rd. gen E S T.
  inductions rd; introv ty.

  (* app *)
  inversions ty.
  apply* typing_app.

  (* tapp *)
  inversions ty.
  apply* typing_tapp.

  (* abs *)
  inversions ty.
  inversions H7.
  pick_fresh x. forwards ~ : H8 x.
  assert (E & x ~: T1 & empty = E & x ~: T1) by
      rewrite~ concat_empty_r.
  rewrite <- H2 in H1.
  forwards ~ : typing_through_subst_ee H1 H5.
  clean_empty H3.
  rewrite <- subst_ee_intro in H3; auto.

  (* absann *)
  inversions ty.
  inversions H7.
  pick_fresh x. forwards ~ : H9 x.
  assert (E & x ~: apply_env E V & empty = E & x ~: apply_env E V) by
      rewrite~ concat_empty_r.
  rewrite <- H2 in H1.
  forwards ~ : typing_through_subst_ee H1 H5.
  clean_empty H3.
  rewrite <- subst_ee_intro in H3; auto.

  (* tapp *)
  inversions ty.
  inversions H6.
  pick_fresh x. forwards ~ : H7 x.
  assert (E & x ~= apply_env E V2 = E & x ~= apply_env E V2 & empty) by
      rewrite~ concat_empty_r.
  rewrite H2 in H1.
  forwards ~ : typing_through_subst_te H1 H0.
  clean_empty H3.
  rewrite <- subst_te_intro in H3; auto.
Qed.

(* ********************************************************************** *)
(** * Progress *)

(* ********************************************************************** *)

(** Canonical Forms *)

Lemma canonical_form_abs : forall t U1 U2 S,
  value t -> typing empty (cons (item_typ U1) S) t U2 ->
  (exists V, exists e1, t = trm_absann V e1) \/
  exists e1, t = trm_abs e1.
Proof.
  introv Val Typ. gen_eq E: (@empty bind).
  gen_eq M: (cons (item_typ U1) S). gen U1 S.
  induction Typ; introv EQT EQE;
   try solve [ inversion Val | inversion EQT | eauto ].
Qed.

Lemma canonical_form_tabs : forall t U1 U2 S,
  value t -> typing empty (cons (item_def U1) S) t U2 ->
  exists e1, t = trm_tabs e1.
Proof.
  introv Val Typ. gen_eq E: (@empty bind).
  gen_eq M: (cons (item_def U1) S). gen U1 S.
  induction Typ; introv EQT EQE;
   try solve [ inversion Val | inversion EQT | eauto ].
Qed.

(* ********************************************************************** *)
(** Progress Result *)

Lemma typing_progress' : forall e T S,
    typing empty S e T ->
    value e \/ exists e', red e e'.
Proof.
  introv ty. lets Ty : ty.
  inductions ty; simpls~.
  (* case: var *)
  false* binds_empty_inv.
  (* case: app *)
  right. destruct* IHty2 as [Val2 | [e2' Rede2']].
      destruct (canonical_form_abs Val2 ty2) as [[V [e3 EQ]] | [e3 EQ] ]; subst.
        exists* (open_ee e3 e2).
        exists* (open_ee e3 e2).
  (* case: tapp *)
  right. destruct* IHty as [Val2 | [e2' Rede2']].
      rewrite apply_env_empty in ty.
      destruct (canonical_form_tabs Val2 ty) as [e3 EQ]; subst.
        exists* (open_te e3 A). constructor*.
        lets [_ [? _]] : typing_regular Ty.
        inversions~ H.
      exists* (trm_tapp e2' A). constructor*.
        lets [_ [? _]] : typing_regular Ty.
        inversions~ H.
Qed.

Lemma typing_progress : forall e T nil,
    typing empty nil e T ->
    value e \/ exists e', red e e'.
Proof.
  introv ty.
  apply* typing_progress'.
Qed.

(* ********************************************************************** *)
(** * Uniqueness *)

(* ********************************************************************** *)

Lemma sub_unique : forall S A B1 B2,
    sub S A B1 ->
    sub S A B2 ->
    B1 = B2.
Proof.
  introv sub1 sub2. gen B2. inductions sub1; introv sub2;
  inversions~ sub2.
Qed.

Lemma typing_unique : forall E S e A B,
    typing E S e A ->
    typing E S e B ->
    A = B.
Proof.
  introv ty1 ty2. gen B. inductions ty1; introv ty2; inversions~ ty2.
  (* var *)
  forwards* : binds_func H1 H6.
  inversions H3. apply sub_unique with S A0; auto.
  (* abs *)
  pick_fresh y. apply* H0.
  (* absann *)
  f_equal. pick_fresh y. apply* H0.
  (* absann *)
  pick_fresh y. apply* H0.
  (* app *)
  apply* IHty1_2. forwards * : IHty1_1 H3. substs*.
  (* tabs *)
  f_equal. pick_fresh y. forwards * : H0 y.
  apply* open_tt_var_fresh.
  (* tapp *)
  pick_fresh y. apply* H0.
Qed.
